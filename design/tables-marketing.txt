As of 01/20/15 Table Layouts

//**************************
//*
//*  marketing_channel
//*
//*  This is channel that ad is being show on
//*  such as Facebook, websites or twitter
//*  Used to track where the lead originated from
//*  i.e. where is the best place to advertise
//*
//* last verified 01/20/15
//**************************


// This denotes the name of a channel. Although each new Sponsor will recieve
// an auto generated list, it will be expand to allow the advertisers to add
// new channels on their own.

// Note: When a new sponsor is added, the basic channels should be added for them.

sponsor_id // owner of the record
title
description
archived
created_at
updated_at


//**************************
//*
//*  marketing_campaign
//*
//*  This is used to track a marketing campaign
//*  A campaign is comprised of the following elements
//*  Sponsor, Advertisement_advertisement and Marketing_channel
//*  Who, What and Where
//*
//* last verified 01/20/15
//**************************

sponsor_id // owner of the record
marketing_channel_id
advertisements_advertisement_id
title
description
start_date
end_date
sent int // if mailed or emailed, how many were sent out
responses int // how many people responded and visited the website
signups int // how many responders actually signed up
notes
archived
created_at
updated_at
