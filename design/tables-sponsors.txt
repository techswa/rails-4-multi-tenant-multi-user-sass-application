As of 01/20/15 Table Layouts


//**************************
//*
//*  sponsor
//*
//*  This is used to track a advertising sponsors
//*  A sponsor has the ability to create and manage advertising campaigns
//*  There is also a related Sponsor_details record that holds
//*  information used to personalize the advertisements to be displayed
//*
//* Note: This is also the devise (authenitiction record for a sponsor)
//*
//* last verified 01/20/15
//**************************
user_name
email
encrypted_password
reset_password_token
reset_password_sent_at
remember_created_at
sign_in_count
current_sign_in_at
last_sign_in_at
current_sign_in_ip
last_sign_in_ip
campaign_id  // marketing campaign that resulted in this sign_up
lookups_industry_id // used to classify sponsor
sponsor_type_id // used to track sponsor's audience type -> b2b, b2c
license_number // Govt issued license
license_email // email used govt contact
business_name
first_name
last_name
primary_phone
address_1 // billing address not broker's address
address_2
city
lookups_state_id
zipcode
website
notes
created_at
updated_at


//**************************
//*
//*  sponsor_details
//*
//*  This is used to manage advertisement content generation
//*  these fields are used as merge information when an advertisement
//*  is generated and displayed
//*
//* last verified 01/20/15
//**************************

sponsor_id
company_name
contact_last_name
contact_first_name
phone_1
phone_2
website
email
tag_line_1
tag_line_2
welcome_greeting
return_greeting
logo_filename
headshot_filename
created_at
updated_at


//**************************
//*
//*  sponsor_types
//*
//*  This is used to manage availability of information based upon role
//* certain ad templates are only shown to applicabile sponsor types
//*
//* last verified 01/20/15
//**************************

title
description
symbol_name //Used internally for branching
archived
created_at
updated_at
