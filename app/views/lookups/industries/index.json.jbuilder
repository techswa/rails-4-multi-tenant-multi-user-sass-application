json.array!(@lookups_industries) do |lookups_industry|
  json.extract! lookups_industry, :id, :naics_code, :title, :note
  json.url lookups_industry_url(lookups_industry, format: :json)
end
