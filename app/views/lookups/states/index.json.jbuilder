json.array!(@lookups_states) do |lookups_state|
  json.extract! lookups_state, :id, :fips_state, :full_name, :abbreviated_name
  json.url lookups_state_url(lookups_state, format: :json)
end
