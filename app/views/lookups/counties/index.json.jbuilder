json.array!(@lookups_counties) do |lookups_county|
  json.extract! lookups_county, :id, :full_name, :fips_county, :fips_state
  json.url lookups_county_url(lookups_county, format: :json)
end
