json.array!(@lookups_exchanges) do |lookups_exchange|
  json.extract! lookups_exchange, :id, :exchange_name, :exchange_abbreviation, :city, :lookups_state_id
  json.url lookups_exchange_url(lookups_exchange, format: :json)
end
