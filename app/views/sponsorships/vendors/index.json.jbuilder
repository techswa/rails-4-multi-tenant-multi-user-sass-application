json.array!(@sponsorships_vendors) do |sponsorships_vendor|
  json.extract! sponsorships_vendor, :id, :vendor_id, :sponsor_id, :marketing_campaign_id, :start_date, :end_date, :active
  json.url sponsorships_vendor_url(sponsorships_vendor, format: :json)
end
