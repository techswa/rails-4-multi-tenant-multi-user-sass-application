json.array!(@sponsorships_consumers) do |sponsorships_consumer|
  json.extract! sponsorships_consumer, :id, :sponsors_sponsor_id, :user_id, :marketing_campaign_id, :start_date, :end_date
  json.url sponsorships_consumer_url(sponsorships_consumer, format: :json)
end
