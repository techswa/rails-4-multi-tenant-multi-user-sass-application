json.array!(@sponsorships_businesses) do |sponsorships_business|
  json.extract! sponsorships_business, :id, :sponsors_sponsor_id, :sponsors_sponsored_id, :marketing_campaign_id, :start_date, :end_date
  json.url sponsorships_business_url(sponsorships_business, format: :json)
end
