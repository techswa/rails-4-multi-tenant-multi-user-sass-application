json.array!(@accounting_statuses) do |accounting_status|
  json.extract! accounting_status, :id, :sponsors_sponsor_id, :suspended, :grace
  json.url accounting_status_url(accounting_status, format: :json)
end
