json.array!(@accounting_payment_types) do |accounting_payment_type|
  json.extract! accounting_payment_type, :id, :name, :sort
  json.url accounting_payment_type_url(accounting_payment_type, format: :json)
end
