json.array!(@accounting_payments) do |accounting_payment|
  json.extract! accounting_payment, :id, :sponsor_id, :accounting_payment_type_id, :amount, :staff_id, :note
  json.url accounting_payment_url(accounting_payment, format: :json)
end
