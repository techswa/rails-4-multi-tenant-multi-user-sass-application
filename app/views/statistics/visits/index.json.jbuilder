json.array!(@statistics_visits) do |statistics_visit|
  json.extract! statistics_visit, :id, :user_id, :sponsors_sponsor_id, :marketing_campaign_id, :session_id, :statistics_device_id
  json.url statistics_visit_url(statistics_visit, format: :json)
end
