json.array!(@statistics_devices) do |statistics_device|
  json.extract! statistics_device, :id, :note
  json.url statistics_device_url(statistics_device, format: :json)
end
