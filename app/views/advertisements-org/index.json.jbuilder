json.array!(@advertisements) do |advertisement|
  json.extract! advertisement, :id, :sponsor_id, :title, :description, :ad_type_id, :ad_source_type_id, :system_ad_id, :sponsor_ad_id, :archived
  json.url advertisement_url(advertisement, format: :json)
end
