json.array!(@sponsor_details) do |sponsor_detail|
  json.extract! sponsor_detail, :id, :sponsor_id, :company_name, :contact_last_name, :contact_first_name, :phone_1, :phone_2, :website, :email, :tag_line_1, :tag_line_2, :welcome_greeting, :return_greeting, :logo_file, :headshot_file
  json.url sponsor_detail_url(sponsor_detail, format: :json)
end
