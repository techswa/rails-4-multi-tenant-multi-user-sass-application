json.extract! @sponsor, :id, :industry_id, :user_id, :business_name, :phone, :address_1, :address_2, :city, :state_id, :zipcode, :website, :notes, :created_at, :updated_at
