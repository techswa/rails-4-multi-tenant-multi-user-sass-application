json.array!(@sponsors) do |sponsor|
  json.extract! sponsor, :id, :industry_id, :user_id, :business_name, :phone, :address_1, :address_2, :city, :state_id, :zipcode, :website, :notes
  json.url sponsor_url(sponsor, format: :json)
end
