json.array!(@sponsors_portals) do |sponsors_portal|
  json.extract! sponsors_portal, :id, :sponsor_id, :portal_setting_id, :disabled
  json.url sponsors_portal_url(sponsors_portal, format: :json)
end
