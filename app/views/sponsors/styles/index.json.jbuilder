json.array!(@sponsors_styles) do |sponsors_style|
  json.extract! sponsors_style, :id, :title, :description, :archived
  json.url sponsors_style_url(sponsors_style, format: :json)
end
