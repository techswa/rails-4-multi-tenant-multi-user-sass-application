json.array!(@sponsors_levels) do |sponsors_level|
  json.extract! sponsors_level, :id, :name, :description, :archived
  json.url sponsors_level_url(sponsors_level, format: :json)
end
