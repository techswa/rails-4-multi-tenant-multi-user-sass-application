json.array!(@sponsors_franchises) do |sponsors_franchise|
  json.extract! sponsors_franchise, :id, :title, :description, :archived
  json.url sponsors_franchise_url(sponsors_franchise, format: :json)
end
