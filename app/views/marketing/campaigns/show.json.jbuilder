json.extract! @marketing_campaign, :id, :sponsor_id, :title, :description, :start_date, :end_date, :sent, :responses, :signups, :notes, :archived, :created_at, :updated_at
