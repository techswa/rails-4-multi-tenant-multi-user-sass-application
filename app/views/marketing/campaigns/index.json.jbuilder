json.array!(@marketing_campaigns) do |marketing_campaign|
  json.extract! marketing_campaign, :id, :sponsor_id, :title, :description, :start_date, :end_date, :sent, :responses, :signups, :notes, :archived
  json.url marketing_campaign_url(marketing_campaign, format: :json)
end
