json.array!(@staffs) do |staff|
  json.extract! staff, :id, :first_name, :last_name, :staff_role_id, :note, :disabled
  json.url staff_url(staff, format: :json)
end
