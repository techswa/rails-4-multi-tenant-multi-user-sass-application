json.array!(@advertisements_mechanicals) do |advertisements_mechanical|
  json.extract! advertisements_mechanical, :id, :title, :description, :sponsors_type_id, :display_width, :display_height, :symbol_name, :archived
  json.url advertisements_mechanical_url(advertisements_mechanical, format: :json)
end
