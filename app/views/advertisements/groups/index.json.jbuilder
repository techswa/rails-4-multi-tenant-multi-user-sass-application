json.array!(@advertisements_groups) do |advertisements_group|
  json.extract! advertisements_group, :id, :title, :description, :archived
  json.url advertisements_group_url(advertisements_group, format: :json)
end
