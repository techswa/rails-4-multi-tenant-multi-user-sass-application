json.array!(@advertisements_rotations) do |advertisements_rotation|
  json.extract! advertisements_rotation, :id, :sponsors_spid, :advertisements_advertisement_id, :running, :start_date, :stop_date, :notes, :archived
  json.url advertisements_rotation_url(advertisements_rotation, format: :json)
end
