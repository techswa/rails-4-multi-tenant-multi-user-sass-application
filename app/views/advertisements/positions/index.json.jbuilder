json.array!(@advertisements_positions) do |advertisements_position|
  json.extract! advertisements_position, :id, :title, :description, :sponsors_type_id, :display_width, :display_height, :symbol_name, :archived
  json.url advertisements_position_url(advertisements_position, format: :json)
end
