json.array!(@advertisements_advertisements) do |advertisements_advertisement|
  json.extract! advertisements_advertisement, :id, :sponsors_sponsor_id, :title, :description, :revision, :symbol_name, :advertisements_source_id, :advertisements_position_id, :advertisements_template_id, :images_imgfile_id, :archived
  json.url advertisements_advertisement_url(advertisements_advertisement, format: :json)
end
