json.array!(@advertisements_templates) do |advertisements_template|
  json.extract! advertisements_template, :id, :title, :description, :sponsor_id, :code, :archived
  json.url advertisements_template_url(advertisements_template, format: :json)
end
