module AdvertisementService

  # Displays correct sponsor advertisement
  # to display to the current_sponsor that is loggged in
  class AdManagerSponsor
    include Service

    # Manages advertisement services
    # Pass in current_sponsor record and ad_position_symbol
    # Return composed advertisement in html or image file path

    def self.call (sponsor, ad_mechanical)
      puts ''
      puts 'Entered AdManager::Service'
      puts "sponsor passed in: #{sponsor.inspect}"
      puts "ad_mechanical passed in: #{ad_mechanical.inspect}"
      puts ''
    # Who
       #Get the Business::Sponsorship record
       sponsorship_record = Sponsorships::Business.where(sponsor_id: sponsor.id).where(active: true).first
       puts ''
       puts 'Sponsorship record to be used :'
       puts "#{sponsorship_record.inspect}"

    # # Where - ad_position matches symbol and isn't archived
       puts ''
       puts 'Locating mechanical record for mechanical symbol.'
       advertisements_mechanical_record = Advertisements::Mechanical.where(archived: false).where(symbol_name: ad_mechanical).first
    #
       puts ''
       puts "Ad_mechanical found: #{advertisements_mechanical_record.inspect}"

    # # What - Which advertisment rotation to show - find ad that matches Who and Where from above that is also active
       puts ''
       rotation_record = Advertisements::Rotation.where(sponsor_id: sponsorship_record.vendor_id).where(advertisements_mechanical_id: advertisements_mechanical_record.id).first
       puts "Rotation details #{rotation_record.inspect}"


       puts ''
       puts 'Finding correct advertisement'
       advertisement = Advertisements::Advertisement.find(rotation_record.advertisements_advertisement_id)
       puts "Advertisements found: #{advertisement.inspect}"
      #
       puts ''
       template = Advertisements::Template.find(advertisement.advertisements_template_id)
       puts "Advertising template is : #{template.inspect}"
       template
      #
      # template_code = template.code

      # "<b>Hello from the ad system<b/><br/> #{advertisement.inspect}"



    end


    private




  end

end
