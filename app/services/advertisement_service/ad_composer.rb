module AdvertisementService
  #TODO Is this being used?
  # Creates ad from template and SponsorDetails
  class AdComposer
    include Service

    # Pass Sponsor and Advertisement
    # Return composed advertisement in html or image file path
    # Can display static image instead of the html

    def self.call (sponsor, campaign)
      #Who owns the ad
      puts "Entered AdvertisementService::AdComposer"
      puts "Sponor #{sponsor.inspect}"
      #Which ad to use
      puts "Marketing campaign #{campaign.inspect}"

      ad_rotation_id = campaign.advertisements_rotation_id

      ad_rotation = Advertisements::Rotation.find(ad_rotation_id)



      advertisement = Advertisements::Advertisement.find(ad_rotation.advertisements_advertisement_id)
      # variables needed to create ad
      puts "Returned advertisement #{advertisement.inspect}"

      # advertisement_position = advertisement.advertisement_position_id
      # ad_position_id = advertisement.advertisements_position_id
      #sponsor - already have it

      # Retrieve sponsor ad for the current user
      # advertisement_template = AdvertisementService::AdManager.call(sponsor, ad_position_id) #:sponsor_portal_main

      advertisement_template = Advertisements::Template.find(advertisement.advertisements_template_id)
      puts advertisement_template
      # Temp template
      # advertisement_template = "<div> <div><img src='/system/images/remax-136x132.png' height='80'></div> <div style='color:blue'> <b>Advertisement for {{sponsor.business_name}}</b> <br/> Hello {{sponsor.first_name}} {{sponsor.last_name}}, <br/> Body of the ad goes here.</div></div>"


      # Create sponsor drop
      sponsor_drop = SponsorDrop.new(sponsor)
      # puts '---------'
      # puts sponsor_drop.inspect
      # puts '---------'
      merge_ready_template = Liquid::Template.parse(advertisement_template.code)

      # puts merge_ready_template.inspect

      @merged_advertisement = merge_ready_template.render('sponsor' => sponsor_drop, )

      # #Advertisement rotations
      # @rotations = Advertisements::Rotation.all
      #
      # # Advertisements
      # @advertisements = Advertisements::Advertisement.where(sponsor_id: current_sponsor.id)
      #
    end


    private



    def self.portal_show sponsor_details
      "Hey there from the banner ad for portal show.\n #{sponsor_details.inspect}"

    end


  end

end
