module AccountingService

  # Retrieve campaign details by the campaign's id
  class GetAccountStatusRecordBySponsorId
    include Service

    def self.call (sponsor_id)

      @account_status_record ||= Accounting::Status.where(sponsors_sponsor_id: sponsor_id).first
    end


  private

  end
end