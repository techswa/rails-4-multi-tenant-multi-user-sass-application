module AccountingService

  # Retrieve campaign details by the campaign's id
  # Returns hash with active 'Takes grace into consideration', suspended and grace
  # [active:{true, false} suspended: {true, false}, grace: {0-x in days}]

  class GetAccountStatusBySponsorId
    include Service

    def self.call (sponsor_id)

      # Get the sponsors status record
      status = Accounting::Status.where(sponsors_sponsor_id: sponsor_id).first

      returned_hash = Hash.new
      active = is_account_active?(status)
      grace_remaining = grace_days_remaining(status)
      # returned_hash[:status] = 'bbbbb' #status.suspended
      # returned_hash[:grace] = 'ccccc' #status.grace
      returned_hash = {active: active, suspended: status.suspended, grace_ends: status.grace, grace_remaining: grace_remaining }
    end


    # Taking grace into consideration
    def self.is_account_active? status

      # If account is suspended
      if status.suspended
        #See if any grace is available
        if Date.today > status.grace
          false
        else #has grace left
          true
        end
      end

    end

    # Calculate days of grace remaining
    def self.grace_days_remaining status

      (status.grace - Date.today).to_i
      # if (Date.today > status.grace)
      #   # No days remain
      #   0
      # else
      #   (Date.today - status.grace).to_i
      # end
      # grace_days_remaining = (Date.today - status.grace).to_i
    end

  end

end