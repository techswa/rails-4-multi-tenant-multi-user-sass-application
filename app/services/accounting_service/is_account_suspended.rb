module AccountingService

  # Retrieve campaign details by the campaign's id
  class IsAccountSuspended
    include Service

    def self.call (account_status_record)

      account_status_record.suspended
    end


  private

  end
end