module CampaignService

  # Retrieve marketing campaign record by the campaign's id
  class GetCampaignRecordById
    include Service

    def self.call (id)

      Marketing::Campaign.find(id)
    end


  private

  end
end
