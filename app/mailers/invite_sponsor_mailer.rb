class InviteSponsorMailer < ActionMailer::Base
  default from: "rails@smarterwebapps.com"

  def test_mail
    mail to: 'britton@voucherguide.com', from: 'rails@smarterwebapps.com', subject: 'Test mail'
  end
end

#
# Send test mailer
# InviteSponsorMailer.test_mail.deliver