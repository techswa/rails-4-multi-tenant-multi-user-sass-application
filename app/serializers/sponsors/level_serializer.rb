class Sponsors::LevelSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :archived
end
