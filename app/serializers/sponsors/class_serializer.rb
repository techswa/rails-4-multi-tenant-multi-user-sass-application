class Sponsors::ClassSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :archived
end
