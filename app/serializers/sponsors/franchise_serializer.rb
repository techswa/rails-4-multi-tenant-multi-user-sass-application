class Sponsors::FranchiseSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :archived
end
