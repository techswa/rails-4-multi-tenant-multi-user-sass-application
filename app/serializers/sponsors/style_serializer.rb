class Sponsors::StyleSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :archived
end
