class Advertisements::TemplateSerializer < ActiveModel::Serializer


  embed :ids, embed_in_root: true


   attributes :id, :title, :description, :sponsor_id, :code, :archived
   attributes :created_at, :updated_at

  #  attributes :big_dog
  # #
  #  def big_dog
  #    "efeeege"
  #  end
   # attributes :lookups_state_id

   # has_one :lookups_state

end