class Advertisements::GroupSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :archived
end
