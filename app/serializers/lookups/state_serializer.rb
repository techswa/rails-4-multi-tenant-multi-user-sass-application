class Lookups::StateSerializer < ActiveModel::Serializer


  embed :ids, embed_in_root: true
  attributes :id, :fips_state, :full_name, :abbreviated_name

  end