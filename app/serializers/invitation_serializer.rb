class InvitationSerializer < ActiveModel::Serializer


  embed :ids, embed_in_root: true


   attributes :id, :user_name, :business_name, :first_name, :last_name, :primary_phone
   # attributes :email, :sign_in_count, :current_sign_in_at, :last_sign_in_at
   # attributes :address_1, :address_2, :city, :zipcode, :website, :notes
   #
   # attributes :campaign_id, :lookups_industry_id, :sponsors_type_id, :license_number, :license_email
   #
   # attributes :lookups_state_id
   #
   # has_one :lookups_state

end