class Sponsorships::VendorSerializer < ActiveModel::Serializer
  attributes :id, :vendor_id, :sponsor_id, :marketing_campaign_id, :start_date, :end_date, :active
end
