# Please enter the model's name: Marketing::Channel
#
# Please enter the instance name: marketing_channel

class Marketing::ChannelsController < ApplicationController

  respond_to :html
  before_action :set_marketing_channel, only: [:show, :edit, :update, :destroy]

  def index
    marketing_channels = Marketing::Channel.all
    @marketing_channels = marketing_channels.collect { |marketing_channel| Marketing::ChannelDecorator.new(marketing_channel, view_context) }
    respond_with(@marketing_channels)
  end

  def show
    @marketing_channel = Marketing::ChannelDecorator.new(@marketing_channel, view_context)
    respond_with(@marketing_channel)

  end

  def new
    @marketing_channel = Marketing::Channel.new
    respond_with(@marketing_channel)
  end

  def edit
  end

  def create
    modified_params = marketing_channel_params
    # Set ownership of the record
    # sponsor = SponsorService::GetSponsorRecord.call(current_user)
    # modified_params[:sponsors_sponsor_id] = sponsor.id
    # @marketing_channel = Marketing::Channel.new(modified_params)

    @marketing_channel = Marketing::Channel.new(marketing_channel_params)

    @marketing_channel.save
    respond_with(@marketing_channel)
  end

  def update
    # modified_params = marketing_channel_params
    # # Set ownership of the record
    # sponsor = SponsorService::GetSponsorRecord.call(current_user)
    # modified_params[:sponsors_sponsor_id] = sponsor.id

    @marketing_channel.update(marketing_channel_params)
    respond_with(@marketing_channel)
  end

  def destroy
    @marketing_channel.destroy
    respond_with(@marketing_channel)
  end

  private
    def set_marketing_channel
      @marketing_channel = Marketing::Channel.find(params[:id])
    end

    def marketing_channel_params
      params.require(:marketing_channel).permit(:sponsor_id, :title, :description, :archived)
    end
end
