
class Marketing::CampaignsController < ApplicationController

  respond_to :html


  before_action :set_marketing_campaign, only: [:show, :edit, :update, :destroy]

  def index
    marketing_campaigns = Marketing::Campaign.all
    # marketing_campaigns = Marketing::Campaign.where(sponsor_id: current_user_sponsored_by.id)
    @marketing_campaigns = marketing_campaigns.collect { |marketing_campaign| Marketing::CampaignDecorator.new(marketing_campaign, view_context) }
    respond_with(@marketing_campaigns)
  end

  def show
    @marketing_campaign = Marketing::CampaignDecorator.new(@marketing_campaign, view_context)
    respond_with(@marketing_campaign)

  end

  def new
    @marketing_campaign = Marketing::Campaign.new
    respond_with(@marketing_campaign)
  end

  def edit
  end

  def create
    modified_params = marketing_campaign_params

    @marketing_campaign = Marketing::Campaign.new(modified_params)
    @marketing_campaign.save
    respond_with(@marketing_campaign)
  end

  def update
    # modified_params = marketing_campaign_params
    # modified_params[:sponsors_sponsor_id] = SponsorService::GetSponsorRecord.call(current_user).id
    @marketing_campaign.update(marketing_campaign_params)
    respond_with(@marketing_campaign)
  end

  def destroy
    @marketing_campaign.destroy
    respond_with(@marketing_campaign)
  end

  private

  def set_sponsor_id
    # Filter by sponsor_id, no monkey business
    @sponsor_id = Sponsor.find(current_sponsor.id)
  end


    def set_marketing_campaign
      @marketing_campaign = Marketing::Campaign.find(params[:id])
    end

    def marketing_campaign_params
      params.require(:marketing_campaign).permit(:sponsor_id, :marketing_channel_id, :advertisements_rotation_id, :title, :description, :start_date, :end_date, :sent, :responses, :signups, :notes, :archived)
    end
end
