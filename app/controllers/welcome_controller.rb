class WelcomeController < ApplicationController

  #before_action :get_sponsor_record, if: cu

  # Custom route for first visit to system
  def invitation

    # get the campaign_id from parameters
    campaign_id = params[:campaign_id]

    # Set cookie on visitors computer that stores the campaign source
    set_campaign_cookie campaign_id

    # Get sponsor id from Campaign record
    sponsor_id = get_campaign_sponsor campaign_id

    #Set the Sponsor cookie on the visiting computer
    set_sponsor_cookie sponsor_id

    #Update campaign's visitor count
    ManageCampaigns::IncrementResponses.call campaign_id

    # Send visitor to the welcome#index pagegit status

    redirect_to welcome_path
  end


  def index

    # # Get the Sponsor of the current user's company
    # sponsoree_id = Sponsor.where(user_id: current_user.id)
    # sponsor_id = Bsponsorship.where(sponsoree_id: sponsoree_id).first.sponsor_id
    # @sponsor = Sponsor.find(sponsor_id)

    # @ad_to_show = AdvertisementService::AdManager.call(current_user, :welcome_index)
  end

  private

  # Set cookie on visitors computer that stores the campaign source
  def set_campaign_cookie campaign_id
    cookies.permanent.signed[:campaign] = campaign_id
  end

  # get the campaign cookie from the user's computer
  def get_campaign_cookie
    cookies.permanent.signed[:campaign]
  end

  # Set cookie on visitors computer
  def set_sponsor_cookie sponsor_id
    cookies.permanent.signed[:sponsor] = sponsor_id
  end

  # get the sponsor that sent the invitation
  def get_campaign_sponsor campaign_id
    Campaign.where(id: campaign_id).first.id
  end



end
