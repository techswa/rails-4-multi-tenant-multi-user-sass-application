# Please enter the model's name: Advertisements::Mechanicals
#
# Please enter the instance name: advertisements_mechanical

class Advertisements::MechanicalsController < ApplicationController

  respond_to :html
  before_action :set_advertisements_mechanical, only: [:show, :edit, :update, :destroy]

  def index
    # advertisements_mechanicals = Advertisements::Mechanical.includes(:sponsors_type).order('sponsors_types.title', 'advertisements_mechanicals.title')
    advertisements_mechanicals = Advertisements::Mechanical.all.order(:title)
    @advertisements_mechanicals = advertisements_mechanicals.collect { |advertisements_mechanical| Advertisements::MechanicalDecorator.new(advertisements_mechanical, view_context) }
    respond_with(@advertisements_mechanicals)
  end

  def show
    @advertisements_mechanical = Advertisements::MechanicalDecorator.new(@advertisements_mechanical, view_context)
    respond_with(@advertisements_mechanical)
  end

  def new
    @advertisements_mechanical = Advertisements::Mechanical.new
    respond_with(@advertisements_mechanical)
  end

  def edit
  end

  def create
    @advertisements_mechanical = Advertisements::Mechanical.new(advertisements_mechanical_params)
    @advertisements_mechanical.save
    respond_with(@advertisements_mechanical)
  end

  def update
    @advertisements_mechanical.update(advertisements_mechanical_params)
    respond_with(@advertisements_mechanical)
  end

  def destroy
    @advertisements_mechanical.destroy
    respond_with(@advertisements_mechanical)
  end

  private
    def set_advertisements_mechanical
      @advertisements_mechanical = Advertisements::Mechanical.find(params[:id])
    end

    def advertisements_mechanical_params
      params.require(:advertisements_mechanical).permit(:title, :description, :display_width, :display_height, :symbol_name, :archived)
    end
end
