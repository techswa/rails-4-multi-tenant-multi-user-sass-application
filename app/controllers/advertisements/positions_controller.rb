# Please enter the model's name: Advertisements::Position
#
# Please enter the instance name: advertisements_position

class Advertisements::PositionsController < ApplicationController

  respond_to :html
  before_action :set_advertisements_position, only: [:show, :edit, :update, :destroy]

  def index
    advertisements_positions = Advertisements::Position.includes(:sponsors_type).order('sponsors_types.title', 'advertisements_positions.title')
    @advertisements_positions = advertisements_positions.collect { |advertisements_position| Advertisements::PositionDecorator.new(advertisements_position, view_context) }
    respond_with(@advertisements_positions)
  end

  def show
    @advertisements_position = Advertisements::PositionDecorator.new(@advertisements_position, view_context)
    respond_with(@advertisements_position)
  end

  def new
    @advertisements_position = Advertisements::Position.new
    respond_with(@advertisements_position)
  end

  def edit
  end

  def create
    @advertisements_position = Advertisements::Position.new(advertisements_position_params)
    @advertisements_position.save
    respond_with(@advertisements_position)
  end

  def update
    @advertisements_position.update(advertisements_position_params)
    respond_with(@advertisements_position)
  end

  def destroy
    @advertisements_position.destroy
    respond_with(@advertisements_position)
  end

  private
    def set_advertisements_position
      @advertisements_position = Advertisements::Position.find(params[:id])
    end

    def advertisements_position_params
      params.require(:advertisements_position).permit(:title, :description, :sponsors_type_id, :display_width, :display_height, :symbol_name, :archived)
    end
end
