class Advertisements::GroupsController < ApplicationController
  before_action :set_advertisements_group, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    advertisements_groups = Advertisements::Group.all.order(:title)
    @advertisements_groups = advertisements_groups.collect { |advertisements_group| Advertisements::GroupDecorator.new(advertisements_group, view_context) }
    respond_with(@advertisements_groups)
  end

  def show
    @advertisements_group = Advertisements::GroupDecorator.new(@advertisements_group, view_context)
    respond_with(@advertisements_group)
  end

  def new
    @advertisements_group = Advertisements::Group.new
    respond_with(@advertisements_group)
  end

  def edit
  end

  def create
    @advertisements_group = Advertisements::Group.new(advertisements_group_params)
    @advertisements_group.save
    respond_with(@advertisements_group)
  end

  def update
    @advertisements_group.update(advertisements_group_params)
    respond_with(@advertisements_group)
  end

  def destroy
    @advertisements_group.destroy
    respond_with(@advertisements_group)
  end

  private
    def set_advertisements_group
      @advertisements_group = Advertisements::Group.find(params[:id])
    end

    def advertisements_group_params
      params.require(:advertisements_group).permit(:title, :description, :sponsors_type_id, :archived)
    end
end
