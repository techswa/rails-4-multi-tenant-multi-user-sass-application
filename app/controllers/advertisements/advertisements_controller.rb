# Please enter the model's name: Advertisements::Advertisement
#
# Please enter the instance name: advertisements_advertisement

class Advertisements::AdvertisementsController < ApplicationController

  respond_to :html
  before_action :set_advertisements_advertisement, only: [:show, :edit, :update, :destroy]

  def index

    # advertisements_advertisements = Advertisements::Advertisement.all
    # @advertisements_advertisements = advertisements_advertisements.collect { |advertisements_advertisement| Advertisements::AdvertisementDecorator.new(advertisements_advertisement, view_context) }
    # respond_with(@advertisements_advertisements)

    #Only show sponsor's own ads
    # if current_sponsor.nil?
    #   #staff show all ads
    #   advertisements_advertisements = Advertisements::Advertisement.all
    #   @advertisements_advertisements = advertisements_advertisements.collect { |advertisements_advertisement| Advertisements::AdvertisementDecorator.new(advertisements_advertisement, view_context) }
    #   respond_with(@advertisements_advertisements)
    #
    # else #Sponsor's ads only
      advertisements_advertisements = Advertisements::Advertisement.where(sponsor_id: current_sponsor.id)
      @advertisements_advertisements = advertisements_advertisements.collect { |advertisements_advertisement| Advertisements::AdvertisementDecorator.new(advertisements_advertisement, view_context) }
      respond_with(@advertisements_advertisements)
    # end

  end

  def show
    @advertisements_advertisement = Advertisements::AdvertisementDecorator.new(@advertisements_advertisement, view_context)
    respond_with(@advertisements_advertisement)
  end

  def new
    @advertisements_advertisement = Advertisements::Advertisement.new
    respond_with(@advertisements_advertisement)
  end

  def edit
  end

  def create
    # Set the sponsor_id to the current_sponsor
    modified_params = advertisements_advertisement_params
    modified_params[:sponsor_id] = current_sponsor.id

    # Set the advertisements_group_id to advertisments_position.advertisements_group_id
    advertisements_group_id = Advertisements::Position.find(modified_params[:advertisements_position_id]).advertisements_group_id
    modified_params[:advertisements_group_id] = advertisements_group_id

    @advertisements_advertisement = Advertisements::Advertisement.new(modified_params)
    @advertisements_advertisement.save
    respond_with(@advertisements_advertisement)
  end

  def update
    @advertisements_advertisement.update(advertisements_advertisement_params)
    respond_with(@advertisements_advertisement)
  end

  def destroy
    @advertisements_advertisement.destroy
    respond_with(@advertisements_advertisement)
  end

  private
    def set_advertisements_advertisement

      @advertisements_advertisement = Advertisements::Advertisement.find(params[:id])

      # @advertisements_advertisement = Advertisements::Advertisement.find(params[:id])
      sponsor_id = current_sponsor.id
      puts "ad_ad current_sponsor #{current_sponsor}"
      @advertisements_advertisement = Advertisements::Advertisement.where(sponsor_id: sponsor_id).first

    end

    def advertisements_advertisement_params
      params.require(:advertisements_advertisement).permit(:sponsor_id, :title, :description, :revision, :advertisements_group_id, :advertisements_template_id, :images_imgfile_id, :archived)
    end
end



# class Advertisements::AdvertisementsController < ApplicationController
#   before_action :set_advertisement, only: [:show, :edit, :update, :destroy]
#
#   def index
#     @advertisements_advertisements = Advertisements::Advertisement.all
#     respond_with(@advertisements_advertisements)
#   end
#
#   def show
#     respond_with(@advertisement)
#   end
#
#   def new
#     @advertisement = Advertisements::Advertisement.new
#     respond_with(@advertisement)
#   end
#
#   def edit
#   end
#
#   def create
#     @advertisement = Advertisements::Advertisement.new(advertisement_params)
#     @advertisements_advertisement.save
#     respond_with(@advertisement)
#   end
#
#   def update
#     @advertisements_advertisement.update(advertisement_params)
#     respond_with(@advertisement)
#   end
#
#   def destroy
#     @advertisements_advertisement.destroy
#     respond_with(@advertisement)
#   end
#
#   private
#     def set_advertisement
#       @advertisement = Advertisements::Advertisement.find(params[:id])
#     end
#
#     def advertisement_params
#       params.require(:advertisement).permit(:sponsors_sponsor_id, :title, :description, :revision, :symbol_name, :advertisements_source_id, :advertisements_position_id, :advertisements_template_id, :images_imgfile_id, :archived)
#     end
# end
