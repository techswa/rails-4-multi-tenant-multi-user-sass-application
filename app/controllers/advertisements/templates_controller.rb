class Advertisements::TemplatesController < ApplicationController

  respond_to :html
  before_action :set_advertisements_template, only: [:show, :edit, :update, :destroy]

  def index

    #http://stackoverflow.com/questions/11993683/rails-3-2-activerecord-association-order-with-eager-loading-and-ordering
    advertisements_templates = Advertisements::Template.includes(:sponsor).order('sponsors.business_name', 'advertisements_templates.title')

    @advertisements_templates = advertisements_templates.collect { |advertisements_template| Advertisements::TemplateDecorator.new(advertisements_template,view_context) }
    respond_with(@advertisements_templates)
  end

  def show
    @advertisements_template = Advertisements::TemplateDecorator.new(@advertisements_template, view_context)
    respond_with(@advertisements_template)

  end

  def new
    @advertisements_template = Advertisements::Template.new
    respond_with(@advertisements_template)
  end

  def edit
  end

  def create
    @advertisements_template = Advertisements::Template.new(advertisements_template_params)
    @advertisements_template.save
    respond_with(@advertisements_template)
  end

  def update

    @advertisements_template.update(advertisements_template_params)
    respond_with(@advertisements_template)
  end

  def destroy
    @advertisements_template.destroy
    respond_with(@advertisements_template)
  end

  private
    def set_advertisements_template
      @advertisements_template = Advertisements::Template.find(params[:id])
    end

    def advertisements_template_params
      params.require(:advertisements_template).permit(:title, :description, :sponsor_id, :advertisements_group_id , :advertisements_mechanical_id, :sponsors_franchise_id, :code, :archived)
    end
end
