# Please enter the model's name: Advertisements::Rotation
#
# Please enter the instance name: advertisements_rotation

class Advertisements::RotationsController < ApplicationController

  respond_to :html
  before_action :set_advertisements_rotation, only: [:show, :edit, :update, :destroy]

  def index
    advertisements_rotations = Advertisements::Rotation.all
    @advertisements_rotations = advertisements_rotations.collect { |advertisements_rotation| Advertisements::RotationDecorator.new(advertisements_rotation, view_context) }
    respond_with(@advertisements_rotations)
  end

  def show
    @advertisements_rotation = Advertisements::RotationDecorator.new(@advertisements_rotation, view_context)
    respond_with(@advertisements_rotation)

  end

  def new
    @advertisements_rotation = Advertisements::Rotation.new
    respond_with(@advertisements_rotation)
  end

  def edit
  end

  def create
    @advertisements_rotation = Advertisements::Rotation.new(advertisements_rotation_params)
    @advertisements_rotation.save
    respond_with(@advertisements_rotation)
  end

  def update
    @advertisements_rotation.update(advertisements_rotation_params)
    respond_with(@advertisements_rotation)
  end

  def destroy
    @advertisements_rotation.destroy
    respond_with(@advertisements_rotation)
  end

  private
    def set_advertisements_rotation
      @advertisements_rotation = Advertisements::Rotation.find(params[:id])
    end

    def advertisements_rotation_params
      params.require(:advertisements_rotation).permit(:advertisements_position_id, :marketing_campaign_id, :sponsor_id, :advertisements_advertisement_id, :running, :start_date, :stop_date, :notes, :archived)
    end
end
