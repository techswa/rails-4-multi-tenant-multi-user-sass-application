# Please enter the model's name: Staff
#
# Please enter the instance name: staff

class StaffsController < ApplicationController

  respond_to :html
  before_action :set_staff, only: [:show, :edit, :update, :destroy]

  def index
    staffs = Staff.all
    @staffs = staffs.collect { |staff| StaffDecorator.new(staff, view_context) }
    respond_with(@staffs)
  end

  def show
    @staff = StaffDecorator.new(@staff, view_context)
    respond_with(@staff)

  end

  def new
    @staff = Staff.new
    respond_with(@staff)
  end

  def edit
  end

  def create
    @staff = Staff.new(staff_params)
    @staff.save
    respond_with(@staff)
  end

  def update
    @staff.update(staff_params)
    respond_with(@staff)
  end

  def destroy
    @staff.destroy
    respond_with(@staff)
  end

  private
    def set_staff
      @staff = Staff.find(params[:id])
    end

    def staff_params
      params.require(:staff).permit(:user_name, :email, :password, :password_confirmation, :first_name, :last_name, :staff_role_id, :note, :disabled)
    end
end




# class StaffsController < ApplicationController
#   before_action :set_staff, only: [:show, :edit, :update, :destroy]
#
#   def index
#     @staffs = Staff.all
#     respond_with(@staffs)
#   end
#
#   def show
#     respond_with(@staff)
#   end
#
#   def new
#     @staff = Staff.new
#     respond_with(@staff)
#   end
#
#   def edit
#   end
#
#   def create
#     @staff = Staff.new(staff_params)
#     @staff.save
#     respond_with(@staff)
#   end
#
#   def update
#     @staff.update(staff_params)
#     respond_with(@staff)
#   end
#
#   def destroy
#     @staff.destroy
#     respond_with(@staff)
#   end
#
#   private
#     def set_staff
#       @staff = Staff.find(params[:id])
#     end
#
#     def staff_params
#       params.require(:staff).permit(:first_name, :last_name, :staff_role_id, :note, :disabled)
#     end
# end
