class TestsController < ApplicationController

  respond_to :html, :js
  before_action :set_test, only: [:show, :edit, :update, :destroy]

  def index
    tests = Test.all
    @test = Test.new # For ajax form
    @tests = tests.collect { |test| TestDecorator.new(test,view_context) }
    respond_with(@tests)
  end

  def show
    test = Test.find(params[:id])
    @test = TestDecorator.new(test, view_context)
    respond_with(@test)
  end

  def new
    @test = Test.new
    respond_with(@test)
  end

  def edit
    respond_to do |format|
      format.html{}
      format.js{}
    end
  end

  def create
    @test = Test.new(test_params)

    # Decorate the record
    # @test = TestDecorator.new(@test, view_context)

    respond_to do |format|
      if @test.save
        # Decorate the record
        @test = TestDecorator.new(@test, view_context)
          format.html { redirect_to @test, notice: 'Record created.'}
          format.js { }
      else
          format.html { render 'new' }
          format.js { }
      end
    end

  end

  def update
    respond_to do |format|
      if @test.update(test_params)
        format.html {respond_with(@test)}
        format.js {@test }
      end

    end
  end

  def destroy
    # @test.destroy
    # respond_with(@test)
    respond_to do |format|
      if @test.destroy
        # Decorate the record
        # @test = TestDecorator.new(@test, view_context)
        format.html { redirect_to @test, notice: 'Record destroyed.'}
        format.js { }
      else
        format.html { render 'show' }
        format.js { }
      end
    end
  end

  private
    def set_test
      @test = Test.find(params[:id])
    end

    def test_params
      params.require(:test).permit(:name, :color)
    end
end
