# Please enter the model's name: Sponsorships::Consumer
#
# Please enter the instance name: sponsorships_consumer

class Sponsorships::ConsumersController < ApplicationController

  respond_to :html
  before_action :set_sponsorships_consumer, only: [:show, :edit, :update, :destroy]

  def index
    sponsorships_consumers = Sponsorships::Consumer.all
    @sponsorships_consumers = sponsorships_consumers.collect { |sponsorships_consumer| Sponsorships::ConsumerDecorator.new(sponsorships_consumer, view_context) }
    respond_with(@sponsorships_consumers)
  end

  def show
    @sponsorships_consumer = Sponsorships::ConsumerDecorator.new(@sponsorships_consumer, view_context)
    respond_with(@sponsorships_consumer)

  end

  def new
    @sponsorships_consumer = Sponsorships::Consumer.new
    respond_with(@sponsorships_consumer)
  end

  def edit
  end

  def create
    @sponsorships_consumer = Sponsorships::Consumer.new(sponsorships_consumer_params)
    @sponsorships_consumer.save
    respond_with(@sponsorships_consumer)
  end

  def update
    @sponsorships_consumer.update(sponsorships_consumer_params)
    respond_with(@sponsorships_consumer)
  end

  def destroy
    @sponsorships_consumer.destroy
    respond_with(@sponsorships_consumer)
  end

  private
    def set_sponsorships_consumer
      @sponsorships_consumer = Sponsorships::Consumer.find(params[:id])
    end

    def sponsorships_consumer_params
      params.require(:sponsorships_consumer).permit(:sponsors_sponsor_id, :user_id, :marketing_campaign_id, :start_date, :end_date)
    end
end




# class Sponsorships::ConsumersController < ApplicationController
#   before_action :set_consumer, only: [:show, :edit, :update, :destroy]
#
#   def index
#     @sponsorships_consumers = Sponsorships::Consumer.all
#     respond_with(@sponsorships_consumers)
#   end
#
#   def show
#     respond_with(@consumer)
#   end
#
#   def new
#     @consumer = Sponsorships::Consumer.new
#     respond_with(@consumer)
#   end
#
#   def edit
#   end
#
#   def create
#     @consumer = Sponsorships::Consumer.new(consumer_params)
#     @sponsorships_consumer.save
#     respond_with(@consumer)
#   end
#
#   def update
#     @sponsorships_consumer.update(consumer_params)
#     respond_with(@consumer)
#   end
#
#   def destroy
#     @sponsorships_consumer.destroy
#     respond_with(@consumer)
#   end
#
#   private
#     def set_consumer
#       @consumer = Sponsorships::Consumer.find(params[:id])
#     end
#
#     def consumer_params
#       params.require(:consumer).permit(:sponsors_sponsor_id, :user_id, :marketing_campaign_id, :start_date, :end_date)
#     end
# end
