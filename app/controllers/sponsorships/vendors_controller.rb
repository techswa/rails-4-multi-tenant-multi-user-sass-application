class Sponsorships::VendorsController < ApplicationController
  before_action :set_sponsorships_vendor, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    sponsorships_vendors = Sponsorships::Vendor.all
    @sponsorships_vendors = sponsorships_vendors.collect { |sponsorships_vendor| Sponsorships::VendorDecorator.new(sponsorships_vendor, view_context) }
    respond_with(@sponsorships_vendors)
  end

  def show
    @sponsorships_vendor = Sponsorships::VendorDecorator.new(@sponsorships_vendor, view_context)
    respond_with(@sponsorships_vendor)
  end

  def new
    @sponsorships_vendor = Sponsorships::Vendor.new
    respond_with(@sponsorships_vendor)
  end

  def edit
  end

  def create
    @sponsorships_vendor = Sponsorships::Vendor.new(sponsorships_vendor_params)
    @sponsorships_vendor.save
    respond_with(@sponsorships_vendor)
  end

  def update
    @sponsorships_vendor.update(sponsorships_vendor_params)
    respond_with(@sponsorships_vendor)
  end

  def destroy
    @sponsorships_vendor.destroy
    respond_with(@sponsorships_vendor)
  end

  private
    def set_sponsorships_vendor
      @sponsorships_vendor = Sponsorships::Vendor.find(params[:id])
    end

    def sponsorships_vendor_params
      params.require(:sponsorships_vendor).permit(:vendor_id, :sponsor_id, :marketing_campaign_id, :start_date, :end_date, :active)
    end
end
