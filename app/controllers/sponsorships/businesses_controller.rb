# Please enter the model's name: Sponsorships::Business
#
# Please enter the instance name: sponsorships_business

class Sponsorships::BusinessesController < ApplicationController

  respond_to :html
  before_action :set_sponsorships_business, only: [:show, :edit, :update, :destroy]

  def index
    sponsorships_businesses = Sponsorships::Business.all
    puts sponsorships_businesses.inspect    
    @sponsorships_businesses = sponsorships_businesses.collect { |sponsorships_business| Sponsorships::BusinessDecorator.new(sponsorships_business, view_context) }
    respond_with(@sponsorships_businesses)
  end

  def show
    @sponsorships_business = Sponsorships::BusinessDecorator.new(@sponsorships_business, view_context)
    respond_with(@sponsorships_business)

  end

  def new
    @sponsorships_business = Sponsorships::Business.new
    respond_with(@sponsorships_business)
  end

  def edit
  end

  def create
    @sponsorships_business = Sponsorships::Business.new(sponsorships_business_params)
    @sponsorships_business.save
    respond_with(@sponsorships_business)
  end

  def update
    @sponsorships_business.update(sponsorships_business_params)
    respond_with(@sponsorships_business)
  end

  def destroy
    @sponsorships_business.destroy
    respond_with(@sponsorships_business)
  end

  private
    def set_sponsorships_business
      @sponsorships_business = Sponsorships::Business.find(params[:id])
    end

    def sponsorships_business_params
      params.require(:sponsorships_business).permit(:sponsors_sponsor_id, :sponsors_sponsored_id, :marketing_campaign_id, :start_date, :end_date)
    end
end



# class Sponsorships::BusinessesController < ApplicationController
#   before_action :set_business, only: [:show, :edit, :update, :destroy]
#
#   def index
#     @sponsorships_businesses = Sponsorships::Business.all
#     respond_with(@sponsorships_businesses)
#   end
#
#   def show
#     respond_with(@business)
#   end
#
#   def new
#     @business = Sponsorships::Business.new
#     respond_with(@business)
#   end
#
#   def edit
#   end
#
#   def create
#     @business = Sponsorships::Business.new(business_params)
#     @sponsorships_business.save
#     respond_with(@business)
#   end
#
#   def update
#     @sponsorships_business.update(business_params)
#     respond_with(@business)
#   end
#
#   def destroy
#     @sponsorships_business.destroy
#     respond_with(@business)
#   end
#
#   private
#     def set_business
#       @business = Sponsorships::Business.find(params[:id])
#     end
#
#     def business_params
#       params.require(:business).permit(:sponsors_sponsor_id, :sponsors_sponsored_id, :marketing_campaign_id, :start_date, :end_date)
#     end
# end
