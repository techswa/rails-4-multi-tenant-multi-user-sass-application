# Please enter the model's name: Images::Preset
#
# Please enter the instance name: images_preset

class Images::TypesController < ApplicationController

  respond_to :html
  before_action :set_images_type, only: [:show, :edit, :update, :destroy]

  def index
    images_types = Images::Type.all
    @images_types = images_types.collect { |images_type| Images::TypeDecorator.new(images_type, view_context) }
    respond_with(@images_types)
  end

  def show
    @images_type = Images::TypeDecorator.new(@images_type, view_context)
    respond_with(@images_type)

  end

  def new
    @images_type = Images::Type.new
    respond_with(@images_type)
  end

  def edit
  end

  def create
    @images_type = Images::Type.new(images_type_params)
    @images_type.save
    respond_with(@images_type)
  end

  def update
    @images_type.update(images_type_params)
    respond_with(@images_type)
  end

  def destroy
    @images_type.destroy
    respond_with(@images_type)
  end

  private
    def set_images_type
      @images_type = Images::Type.find(params[:id])
    end

    def images_type_params
      params.require(:images_type).permit(:title, :description, :symbol_name, :images_preset_id, :archived)
    end
end



# class Images::TypesController < ApplicationController
#   before_action :set_type, only: [:show, :edit, :update, :destroy]
#
#   def index
#     @images_types = Images::Type.all
#     respond_with(@images_types)
#   end
#
#   def show
#     respond_with(@type)
#   end
#
#   def new
#     @type = Images::Type.new
#     respond_with(@type)
#   end
#
#   def edit
#   end
#
#   def create
#     @type = Images::Type.new(type_params)
#     @images_type.save
#     respond_with(@type)
#   end
#
#   def update
#     @images_type.update(type_params)
#     respond_with(@type)
#   end
#
#   def destroy
#     @images_type.destroy
#     respond_with(@type)
#   end
#
#   private
#     def set_type
#       @type = Images::Type.find(params[:id])
#     end
#
#     def type_params
#       params.require(:type).permit(:title, :description, :symbol_name, :images_preset_id, :archived)
#     end
# end
