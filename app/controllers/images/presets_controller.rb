
class Images::PresetsController < ApplicationController

  respond_to :html
  before_action :set_images_preset, only: [:show, :edit, :update, :destroy]

  def index
    images_presets = Images::Preset.all
    @images_presets = images_presets.collect { |images_preset| Images::PresetDecorator.new(images_preset, view_context) }
    respond_with(@images_presets)
  end

  def show
    @images_preset = Images::PresetDecorator.new(@images_preset, view_context)
    respond_with(@images_preset)

  end

  def new
    @images_preset = Images::Preset.new
    respond_with(@images_preset)
  end

  def edit
  end

  def create
    @images_preset = Images::Preset.new(images_preset_params)
    @images_preset.save
    respond_with(@images_preset)
  end

  def update
    @images_preset.update(images_preset_params)
    respond_with(@images_preset)
  end

  def destroy
    @images_preset.destroy
    respond_with(@images_preset)
  end

  private
    def set_images_preset
      @images_preset = Images::Preset.find(params[:id])
    end

    def images_preset_params
      params.require(:images_preset).permit(:title, :description, :symbol_name, :preview_width, :preview_height, :display_width, :display_height, :archived)
    end
end




# class Images::PresetsController < ApplicationController
#   before_action :set_preset, only: [:show, :edit, :update, :destroy]
#
#   def index
#     @images_presets = Images::Preset.all
#     respond_with(@images_presets)
#   end
#
#   def show
#     respond_with(@preset)
#   end
#
#   def new
#     @preset = Images::Preset.new
#     respond_with(@preset)
#   end
#
#   def edit
#   end
#
#   def create
#     @preset = Images::Preset.new(preset_params)
#     @images_preset.save
#     respond_with(@preset)
#   end
#
#   def update
#     @images_preset.update(preset_params)
#     respond_with(@preset)
#   end
#
#   def destroy
#     @images_preset.destroy
#     respond_with(@preset)
#   end
#
#   private
#     def set_preset
#       @preset = Images::Preset.find(params[:id])
#     end
#
#     def preset_params
#       params.require(:preset).permit(:title, :description, :symbol_name, :preview_width, :preview_height, :display_width, :display_height, :archived)
#     end
# end
