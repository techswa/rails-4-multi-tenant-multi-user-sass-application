# Please enter the model's name: Images::Imgfile
#
# Please enter the instance name: images_imagefile

class Images::ImgfilesController < ApplicationController

  respond_to :html
  before_action :set_images_imgfile, only: [:show, :edit, :update, :destroy]

  def index
    images_imgfiles = Images::Imgfile.all
    @images_imgfiles = images_imgfiles.collect { |images_imgfile| Images::ImgfileDecorator.new(images_imgfile, view_context) }
    respond_with(@images_imgfiles)
  end

  def show
    @images_imgfile = Images::ImgfileDecorator.new(@images_imgfile, view_context)
    respond_with(@images_imgfile)

  end

  def new
    @images_imgfile = Images::Imgfile.new
    respond_with(@images_imgfile)
  end

  def edit
  end

  def create
    @images_imgfile = Images::Imgfile.new(images_imgfile_params)
    @images_imgfile.save
    respond_with(@images_imgfile)
  end

  def update
    @images_imgfile.update(images_imgfile_params)
    respond_with(@images_imgfile)
  end

  def destroy
    @images_imgfile.destroy
    respond_with(@images_imgfile)
  end

  private
    def set_images_imgfile
      @images_imgfile = Images::Imgfile.find(params[:id])
    end

    def images_imgfile_params
      params.require(:images_imgfile).permit(:title, :description, :sponsorships_sponsor_id, :images_type_id, :attachment, :archived)
    end
end

