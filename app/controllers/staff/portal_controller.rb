class Staff::PortalController < ApplicationController
    respond_to :html

  def index
      sponsors = Sponsor.latest_visitors(10)
      @sponsors = sponsors.collect { |sponsor| SponsorDecorator.new(sponsor, view_context) }
      respond_with(@sponsors)

  end
end
