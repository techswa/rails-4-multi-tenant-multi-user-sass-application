class SponsoredController < ApplicationController
  respond_to :html

  def index
    sponsored = User.all
    @sponsored = sponsored.collect { |sponsoree| SponsoredDecorator.new(sponsoree,view_context) }
    respond_with(@sponsored)
  end

  def show
    @sponsored = SponsoredDecorator.new(@sponsored, view_context)
    respond_with(@sponsored)
  end

end
