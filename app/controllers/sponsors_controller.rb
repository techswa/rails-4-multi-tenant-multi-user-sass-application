# Please enter the model's name: Sponsor
#
# Please enter the instance name: sponsor

class SponsorsController < ApplicationController

  respond_to :html

   # before_action :check_privileges!, except: [:new, :show, :edit]
  # before_action

  before_action :set_sponsor, only: [:show, :edit, :update]

  def index
    sponsors = Sponsor.all
    @sponsors = sponsors.collect { |sponsor| SponsorDecorator.new(sponsor, view_context) }
    respond_with(@sponsors)
  end

  def show
    @sponsor = SponsorDecorator.new(@sponsor, view_context)
    @section = 'license'
    respond_with(@sponsor)
  end

  def new
    @sponsor = Sponsor.new
    respond_with(@sponsor)
  end

  def edit
    # only display current_user unless staff role
    # Get step if any
    @section = params[:section]
  end

  def create
    # @sponsor.new_sponsor = true
    # puts 'creating new sponsor'
    @sponsor = Sponsor.new(sponsor_params)
    @sponsor.save
    respond_with(@sponsor)
  end

  def update
    @sponsor.update(sponsor_params)
    respond_with(@sponsor)
  end

  def destroy
    @sponsor.destroy
    respond_with(@sponsor)
  end

  private

    # Only allow staff
    def check_privileges!
      puts '-----'
      puts current_sponsor.inspect
      true unless current_sponsor.nil?
    end

    def set_sponsor
      staff = true
      if staff
        @sponsor = Sponsor.find(params[:id])
      else
        @sponsor = Sponsor.find(current_sponsor.id)
      end
    end

    def sponsor_params
      params.require(:sponsor).permit(:authentication_token, :marketing_campaign_id, :lookups_industry_id, :sponsors_type_id, :sponsors_level_id, :license_number, :license_email, :business_name, :first_name, :last_name, :primary_phone, :address_1, :address_2, :city, :lookups_state_id, :zipcode, :website, :notes)
    end
end



# class SponsorsController < ApplicationController
#   before_action :set_sponsor, only: [:show, :edit, :update, :destroy]
#
#   def index
#     @sponsors = Sponsor.all
#     respond_with(@sponsors)
#   end
#
#   def show
#     respond_with(@sponsor)
#   end
#
#   def new
#     @sponsor = Sponsor.new
#     respond_with(@sponsor)
#   end
#
#   def edit
#   end
#
#   def create
#     @sponsor = Sponsor.new(sponsor_params)
#     @sponsor.save
#     respond_with(@sponsor)
#   end
#
#   def update
#     @sponsor.update(sponsor_params)
#     respond_with(@sponsor)
#   end
#
#   def destroy
#     @sponsor.destroy
#     respond_with(@sponsor)
#   end
#
#   private
#     def set_sponsor
#       @sponsor = Sponsor.find(params[:id])
#     end
#
#     def sponsor_params
#       params.require(:sponsor).permit(:campaign_id, :lookups_industry_id, :sponsors_type_id, :license_number, :license_email, :business_name, :first_name, :last_name, :primary_phone, :address_1, :address_2, :city, :lookups_state_id, :zipcode, :website, :notes)
#     end
# end
