class MyportalController < ApplicationController
  layout "sponsor"
  def index
    # Define advertisement mechanical to be shown via a symbol
    mechanical_to_be_shown = 'sponsor_portal_banner'.to_sym

    # For testing without devise
    #Currently logged in Sponsor
    # sponsor is MW
    # current_sponsor = Sponsor.find('a67fc27a-573c-4bc9-9145-39fac1902ed4')

    puts 'myportal current sponsor'
    puts current_sponsor.inspect
    # Retrieve sponsor ad for the current sponsor and screen
    # AdManagerSponsor display the correct ad to a Realtor/Consumer Sponsor
    advertisement_template = AdvertisementService::AdManagerSponsor.call(current_sponsor, mechanical_to_be_shown)
    puts ''
    puts 'Back in the portals_controller'
    puts "Advertisement template returned is: #{advertisement_template.inspect}"
    puts ''

    sponsorship_record = Sponsorships::Business.where(sponsor_id: current_sponsor.id).where(active: true).first
    puts "sponsorship_record #{sponsorship_record.inspect}"
    puts ''

    current_vendor = Sponsor.find(sponsorship_record.vendor_id)
    puts "Current vendor: #{current_vendor.inspect}"
    puts ''

    current_vendor_details = Sponsors::Detail.where(sponsor_id: current_vendor.id).first
    puts "current vendor details: #{current_vendor_details.inspect}"
    puts ''
    vendor_liquid_drop = VendorDrop.new(current_vendor_details)
    puts "vendor drop: #{vendor_liquid_drop.inspect}"
    puts ''
    # Create sponsor drop
    # sponsor_liquid_drop = SponsorDrop.new(current_sponsor)
    # puts '---------'
    # puts sponsor_liquid_drop.inspect
    puts '---------'
    puts 'Parsing the template'
    merge_ready_template = Liquid::Template.parse(advertisement_template.code)

    puts merge_ready_template.inspect

    # @portal_banner_advertisement = merge_ready_template.render('sponsor' => sponsor_liquid_drop )
    @portal_banner_advertisement = merge_ready_template.render('vendor' => vendor_liquid_drop )

    # Retrieve records for partial displays

    # Sponsored clients
    # Find all that belong to a vendor throught sponsorhips::business
    sponsorships = Sponsorships::Business.where(vendor_id: current_sponsor.id).where(active: true)
    @sponsorships = sponsorships.collect { |sponsorship| Sponsorships::BusinessDecorator.new(sponsorship, view_context) }
    puts "Vendor's sponsorships: #{@sponsorships.inspect}"


    sponsors = Sponsor.all
    @sponsors = sponsors.collect { |sponsor| Sponsors::SponsorDecorator.new(sponsor, view_context) }

    #Advertisement rotations
    # @rotations = Advertisements::Rotation.all
    advertisements_rotations = Advertisements::Rotation.where(sponsor_id: current_sponsor.id)
    @advertisements_rotations = advertisements_rotations.collect { |advertisements_rotation| Advertisements::RotationDecorator.new(advertisements_rotation, view_context) }

    # Advertisements
    # @advertisements = Advertisements::Advertisement.where(sponsor_id: current_sponsor.id)
    advertisements_advertisements = Advertisements::Advertisement.where(sponsor_id: current_sponsor.id)
    @advertisements_advertisements = advertisements_advertisements.collect { |advertisements_advertisement| Advertisements::AdvertisementDecorator.new(advertisements_advertisement, view_context) }

  end

  def details
  end

  def visitors
  end

  def advertisements
  end
end
