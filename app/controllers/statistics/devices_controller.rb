# Please enter the model's name: Statistics::Device
#
# Please enter the instance name: statistics_device

class Statistics::DevicesController < ApplicationController

  respond_to :html
  before_action :set_statistics_device, only: [:show, :edit, :update, :destroy]

  def index
    statistics_devices = Statistics::Device.all
    @statistics_devices = statistics_devices.collect { |statistics_device| Statistics::DeviceDecorator.new(statistics_device, view_context) }
    respond_with(@statistics_devices)
  end

  def show
    @statistics_device = Statistics::DeviceDecorator.new(@statistics_device, view_context)
    respond_with(@statistics_device)

  end

  def new
    @statistics_device = Statistics::Device.new
    respond_with(@statistics_device)
  end

  def edit
  end

  def create
    @statistics_device = Statistics::Device.new(statistics_device_params)
    @statistics_device.save
    respond_with(@statistics_device)
  end

  def update
    @statistics_device.update(statistics_device_params)
    respond_with(@statistics_device)
  end

  def destroy
    @statistics_device.destroy
    respond_with(@statistics_device)
  end

  private
    def set_statistics_device
      @statistics_device = Statistics::Device.find(params[:id])
    end

    def statistics_device_params
      params.require(:statistics_device).permit(:note)
    end
end



# class Statistics::DevicesController < ApplicationController
#   respond_to :html
#   before_action :set_device, only: [:show, :edit, :update, :destroy]
#
#   def index
#     @statistics_devices = Statistics::Device.all
#     respond_with(@statistics_devices)
#   end
#
#   def show
#     respond_with(@device)
#   end
#
#   def new
#     @device = Statistics::Device.new
#     respond_with(@device)
#   end
#
#   def edit
#   end
#
#   def create
#     @device = Statistics::Device.new(device_params)
#     @statistics_device.save
#     respond_with(@device)
#   end
#
#   def update
#     @statistics_device.update(device_params)
#     respond_with(@device)
#   end
#
#   def destroy
#     @statistics_device.destroy
#     respond_with(@device)
#   end
#
#   private
#     def set_device
#       @device = Statistics::Device.find(params[:id])
#     end
#
#     def device_params
#       params.require(:device).permit(:note)
#     end
# end
