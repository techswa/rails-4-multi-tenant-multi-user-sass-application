# Please enter the model's name: Statistics::Visit
#
# Please enter the instance name: statistics_visit

class Statistics::VisitsController < ApplicationController

  respond_to :html
  before_action :set_statistics_visit, only: [:show, :edit, :update, :destroy]

  def index
    statistics_visits = Statistics::Visit.all
    @statistics_visits = statistics_visits.collect { |statistics_visit| Statistics::VisitDecorator.new(statistics_visit, view_context) }
    respond_with(@statistics_visits)
  end

  def show
    @statistics_visit = Statistics::VisitDecorator.new(@statistics_visit, view_context)
    respond_with(@statistics_visit)

  end

  def new
    @statistics_visit = Statistics::Visit.new
    respond_with(@statistics_visit)
  end

  def edit
  end

  def create
    @statistics_visit = Statistics::Visit.new(statistics_visit_params)
    @statistics_visit.save
    respond_with(@statistics_visit)
  end

  def update
    @statistics_visit.update(statistics_visit_params)
    respond_with(@statistics_visit)
  end

  def destroy
    @statistics_visit.destroy
    respond_with(@statistics_visit)
  end

  private
    def set_statistics_visit
      @statistics_visit = Statistics::Visit.find(params[:id])
    end

    def statistics_visit_params
      params.require(:statistics_visit).permit(:user_id, :sponsors_sponsor_id, :marketing_campaign_id, :session_id, :statistics_device_id)
    end
end




# class Statistics::VisitsController < ApplicationController
#   before_action :set_visit, only: [:show, :edit, :update, :destroy]
#
#   def index
#     @statistics_visits = Statistics::Visit.all
#     respond_with(@statistics_visits)
#   end
#
#   def show
#     respond_with(@visit)
#   end
#
#   def new
#     @visit = Statistics::Visit.new
#     respond_with(@visit)
#   end
#
#   def edit
#   end
#
#   def create
#     @visit = Statistics::Visit.new(visit_params)
#     @statistics_visit.save
#     respond_with(@visit)
#   end
#
#   def update
#     @statistics_visit.update(visit_params)
#     respond_with(@visit)
#   end
#
#   def destroy
#     @statistics_visit.destroy
#     respond_with(@visit)
#   end
#
#   private
#     def set_visit
#       @visit = Statistics::Visit.find(params[:id])
#     end
#
#     def visit_params
#       params.require(:visit).permit(:user_id, :sponsors_sponsor_id, :marketing_campaign_id, :session_id, :statistics_device_id)
#     end
# end
