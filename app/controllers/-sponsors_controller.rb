class SponsorsController < ApplicationController
  respond_to :html

  before_action :set_sponsor, only: [:edit, :update, :destroy]
  before_action :set_portal, only: :show
  before_action :set_sponsor_type, only: :show
  before_action :set_clients, only: :show

  # before_action :update_sponsored_count, only:  :show
  after_action :update_sponsored_count, only: [:create, :destroy]

  def index
    sponsors = Sponsor.all
    @sponsors = sponsors.collect { |sponsor| SponsorDecorator.new(sponsor,view_context) }
    respond_with(@sponsors)
  end

  def show
    sponsor = Sponsor.find(params[:id])
    @sponsor = SponsorDecorator.new(sponsor, view_context)
    respond_with(@sponsor)

  end

  def new
    @sponsor = Sponsor.new
    # @sponsor.users.new
    respond_with(@sponsor)
  end

  def edit
  end

  def create
    @sponsor = Sponsor.new(sponsor_params)
    @sponsor.save
    respond_with(@sponsor)
  end

  def update
    @sponsor.update(sponsor_params)
    respond_with(@sponsor)
  end

  def destroy
    @sponsor.destroy
    respond_with(@sponsor)
  end

  private
    def set_sponsor
      @sponsor = Sponsor.find(params[:id])
    end

    def set_portal
      sponsor = Sponsor.find(params[:id])
      @sponsor = SponsorDecorator.new(sponsor, view_context)
    end

    #Used to determine which ad is served
    def set_sponsor_type
      puts "Entering set_sponsor_type"
      @sponsored = @sponsor.sponsor_type.symbol_name.to_sym
      puts "set_sponsor_type: #{@sponsored}"
    end

    #Set clients based upon user type
    def set_clients

      if @sponsored  == :reagent
        @prospects = Csponsorship.where(sponsor_id: @sponsor.id)
      else
        @prospects = Bsponsorship.where(sponsor_id: @sponsor.id)
      end
    end

    #Update count for display
    def update_sponsored_count
      if @sponsored  == :reagent

        count = Csponsorship.where(sponsor_id: @sponsor.id).count
        @sponsor = Sponsor.find(params[:id])
        @sponsor.sponsored = count
        @sponsor.save!

      else
        puts "inside update sponsors"
        count = Bsponsorship.where(sponsor_id: @sponsor.id).count
        @sponsor = Sponsor.find(params[:id])
        puts "sponsor: #{@sponsor.inspect}"
        @sponsor.sponsored = count
        @sponsor.save!

      end
    end

    def sponsor_params
      params.require(:sponsor).permit(:industry_id, :user_id, :sponsor_type_id, :business_name, :phone, :address_1, :address_2, :city, :state_id, :zipcode, :website, :notes)
    end
end
