class Api::V1::Lookups::StatesController < ApplicationController

  respond_to :json

  def index
    render json: Lookups::State.all
  end

  def show
    render json: state
  end

  def create
    respond_with :api, :v1, Lookups::State.all.create(lookups_state_params)
  end

  def update
    respond_with sponsor.update(sponsor_params)
  end

  def destroy
    respond_with sponsor.destroy
  end

  private

  def state
    Lookups::State.all.find(params[:id])
  end

  def state_params
    params.require(:lookups_state).permit(:fips_state, :full_name, :abbreviated_name)
  end

end