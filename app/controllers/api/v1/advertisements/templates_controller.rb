class Api::V1::Advertisements::TemplatesController < ApplicationController

  respond_to :json

  # skip_before_filter :verify_authenticity_token

  def index
    render json: Advertisements::Template.all, serializer: Advertisements::TemplateSerializer
  end

  def show
    render json: template, serializer: Advertisements::TemplateSerializer
  end
  #
  # def new
  #
  # end

  def create

    template = Advertisements::Template.new(template_params)

    if template.save
      render json: template
    else
      render json: template.errors, status: 422
    end

    # render json: Sponsor.create(sponsor_params)
  end

  def update
    render json: template.update(template_params)
  end

  def destroy
    render json: template.destroy
  end

  private

  def template
    Advertisements::Template.find(params[:id])
     # mouser = '99502e37-001e-419b-a3ec-b32121e598c7'
     # Sponsor.find(mouser)
  end

  def template_params
    # params.require(:template).permit(:user_name, :business_name, :first_name, :last_name, :primary_phone, :email, :campaign_id, :password, :password_confirmation)
  end

end

