module Api
  module V1
    class SponsorsController < ApplicationController

      respond_to :json

      skip_before_filter :verify_authenticity_token

      def index
        render json: Sponsor.all
      end

      def show
        render json: sponsor
      end
      #
      # def new
      #
      # end

      def create

        sponsor = Sponsor.new(sponsor_params)

        if sponsor.save
          render json: sponsor
        else
          render json: sponsor.errors, status: 422
        end

        # render json: Sponsor.create(sponsor_params)
      end

      def update
        render json: sponsor.update(sponsor_params)
      end

      def destroy
        render json: sponsor.destroy
      end

      private

      def sponsor
         Sponsor.find(params[:id])
         # mouser = '99502e37-001e-419b-a3ec-b32121e598c7'
         # Sponsor.find(mouser)
      end

      def sponsor_params
        params.require(:sponsor).permit(:user_name, :business_name, :first_name, :last_name, :primary_phone, :email, :campaign_id, :password, :password_confirmation)
      end

    end

  end #end v1
end # end api

