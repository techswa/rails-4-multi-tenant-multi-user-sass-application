class Api::V1::MyportalsController < ApplicationController

  respond_to :json

  def index
    # respond_with Sponsor.select(:id,  :user_name,  :email, :campaign_id).all
  end

  def show
    respond_with sponsor
  end

  def create
    respond_with :api, :v1, Sponsor.create(lead_params)
  end

  def update
    respond_with sponsor.update(sponsor_params)
  end

  def destroy
    respond_with sponsor.destroy
  end

  private

  def sponsor
    Sponsor.find(params[:id])
  end

  def sponsor_params
    params.require(:sponsor).permit(:user_name, :email, :campaign_id)
  end

end