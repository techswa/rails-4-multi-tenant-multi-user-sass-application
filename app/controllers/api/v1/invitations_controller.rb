module Api
  module V1
    class InvitationsController < ApplicationController

      # Sample marketing campaign id to test with
      # a2e34f2c-d23f-47d3-9980-fb9b4932d465

      respond_to :json

      skip_before_filter :verify_authenticity_token

      rescue_from ActiveRecord::RecordNotFound, with: :campaign_not_found
      rescue_from PG::InvalidTextRepresentation, with: :invalid_campaign_id

      # before_action ->(campaign_id=params[:id]) {verify_campaign_exists campaign_id}, only:%w{invitation}

      def index
        render json: {error:'unfortunately'}
      end

      def invitation
        # get the campaign_id from parameters
        campaign_id = params[:invitation_id]

        puts "campaign_id #{campaign_id}"

        if campaign_id
          @campaign_record = CampaignService::GetCampaignRecordById.call(campaign_id)
          # rescue ActiveRecord::RecordNotFound
          #   redirect_to :controller => "sponsors_invitations", :action => "index"
          #   return
          # end

          # Save the campaign_id to a cookie for use in registration
          cookies.permanent.signed['campaign'] = campaign_id

          # Get sponsor status
          sponsor_id = @campaign_record.sponsor_id
          render json: @sponsor = Sponsor.find(sponsor_id), serializer: InvitationSerializer

          # Returns hash with active, suspended, grace_ends and grace_remaining
          # [active: {true,false}, suspended: {true, false}, grace_ends: date, grace_remaining: {0-x in days}]
          # @sponsor_account_status = AccountingService::GetAccountStatusBySponsorId.call(sponsor_id)


          # render 'sponsors/welcome/new'

        else
          render json: {error:'Invitation not found'}
        end
      end

      def show

      end

      def unfortunately
        render json: sponsor.errors, status: 422
      end

      private
      #Has a valid campaign been submitted
      def verify_campaign_exists campaign_id

        marketing_campaign = Marketing::Campaign.find(campaign_id)

        # Send the user away if the campaign doesn't exist
        # otherwise continue
        render '/welcome' unless marketing_campaign

      end


      def campaign_not_found
        redirect_to sponsors_invitations_unfortunately_path, :notice => 'Campaign record not found'
      end

      def invalid_campaign_id
        puts 'Bad UUID in campaign'
        redirect_to sponsors_invitations_unfortunately_path, :notice => 'Campaign record not found'
      end
    end
  end
end # end api
