class Lookups::CountiesController < ApplicationController

  respond_to :html
  before_action :set_lookups_county, only: [:show, :edit, :update, :destroy]

  def index
    lookups_counties = Lookups::County.all
    @lookups_counties = lookups_counties.collect { |lookups_county| Lookups::CountyDecorator.new(lookups_county, view_context) }
    respond_with(@lookups_countiess)
  end

  def show
    @lookups_county = Lookups::CountyDecorator.new(@lookups_county, view_context)
    respond_with(@lookups_county)

  end

  def new
    @lookups_county = Lookups::County.new
    respond_with(@lookups_county)
  end

  def edit
  end

  def create
    @lookups_county = Lookups::County.new(lookups_county_params)
    @lookups_county.save
    respond_with(@lookups_county)
  end

  def update
    @lookups_county.update(lookups_county_params)
    respond_with(@lookups_county)
  end

  def destroy
    @lookups_county.destroy
    respond_with(@lookups_county)
  end

  private
    def set_lookups_county
      @lookups_county = Lookups::County.find(params[:id])
    end

    def lookups_county_params
      params.require(:lookups_county).permit(:full_name, :fips_county, :fips_state)
    end
end



# class Lookups::CountiesController < ApplicationController
#   before_action :set_county, only: [:show, :edit, :update, :destroy]
#
#   def index
#     @lookups_counties = Lookups::County.all
#     respond_with(@lookups_counties)
#   end
#
#   def show
#     respond_with(@county)
#   end
#
#   def new
#     @county = Lookups::County.new
#     respond_with(@county)
#   end
#
#   def edit
#   end
#
#   def create
#     @county = Lookups::County.new(county_params)
#     @lookups_county.save
#     respond_with(@county)
#   end
#
#   def update
#     @lookups_county.update(county_params)
#     respond_with(@county)
#   end
#
#   def destroy
#     @lookups_county.destroy
#     respond_with(@county)
#   end
#
#   private
#     def set_county
#       @county = Lookups::County.find(params[:id])
#     end
#
#     def county_params
#       params.require(:county).permit(:full_name, :fips_county, :fips_state)
#     end
# end
