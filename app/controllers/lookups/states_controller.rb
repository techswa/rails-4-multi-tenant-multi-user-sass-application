# Please enter the model's name: Lookups::State
#
# Please enter the instance name: lookups_state

class Lookups::StatesController < ApplicationController

  respond_to :html
  before_action :set_lookups_state, only: [:show, :edit, :update, :destroy]

  def index
    lookups_states = Lookups::State.all
    @lookups_states = lookups_states.collect { |lookups_state| Lookups::StateDecorator.new(lookups_state, view_context) }
    respond_with(@lookups_states)
  end

  def show
    @lookups_state = Lookups::StateDecorator.new(@lookups_state, view_context)
    respond_with(@lookups_state)

  end

  def new
    @lookups_state = Lookups::State.new
    respond_with(@lookups_state)
  end

  def edit
  end

  def create
    @lookups_state = Lookups::State.new(lookups_state_params)
    @lookups_state.save
    respond_with(@lookups_state)
  end

  def update
    @lookups_state.update(lookups_state_params)
    respond_with(@lookups_state)
  end

  def destroy
    @lookups_state.destroy
    respond_with(@lookups_state)
  end

  private
    def set_lookups_state
      @lookups_state = Lookups::State.find(params[:id])
    end

    def lookups_state_params
      params.require(:lookups_state).permit(:fips_state, :full_name, :abbreviated_name)
    end
end



# class Lookups::StatesController < ApplicationController
#   before_action :set_state, only: [:show, :edit, :update, :destroy]
#
#   def index
#     @lookups_states = Lookups::State.all
#     respond_with(@lookups_states)
#   end
#
#   def show
#     respond_with(@state)
#   end
#
#   def new
#     @state = Lookups::State.new
#     respond_with(@state)
#   end
#
#   def edit
#   end
#
#   def create
#     @state = Lookups::State.new(state_params)
#     @lookups_state.save
#     respond_with(@state)
#   end
#
#   def update
#     @lookups_state.update(state_params)
#     respond_with(@state)
#   end
#
#   def destroy
#     @lookups_state.destroy
#     respond_with(@state)
#   end
#
#   private
#     def set_state
#       @state = Lookups::State.find(params[:id])
#     end
#
#     def state_params
#       params.require(:state).permit(:fips_state, :full_name, :abbreviated_name)
#     end
# end
