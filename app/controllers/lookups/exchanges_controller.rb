
class Lookups::ExchangesController < ApplicationController

  respond_to :html
  before_action :set_lookups_exchange, only: [:show, :edit, :update, :destroy]

  def index
    lookups_exchanges = Lookups::Exchange.all
    @lookups_exchanges = lookups_exchanges.collect { |lookups_exchange| Lookups::ExchangeDecorator.new(lookups_exchange, view_context) }
    respond_with(@lookups_exchanges)
  end

  def show
    @lookups_exchange = Lookups::ExchangeDecorator.new(@lookups_exchange, view_context)
    respond_with(@lookups_exchange)

  end

  def new
    @lookups_exchange = Lookups::Exchange.new
    respond_with(@lookups_exchange)
  end

  def edit
  end

  def create
    @lookups_exchange = Lookups::Exchange.new(lookups_exchange_params)
    @lookups_exchange.save
    respond_with(@lookups_exchange)
  end

  def update
    @lookups_exchange.update(lookups_exchange_params)
    respond_with(@lookups_exchange)
  end

  def destroy
    @lookups_exchange.destroy
    respond_with(@lookups_exchange)
  end

  private
    def set_lookups_exchange
      @lookups_exchange = Lookups::Exchange.find(params[:id])
    end

    def lookups_exchange_params
      params.require(:lookups_exchange).permit(:full_name, :abbreviated_name, :city, :lookups_state_id)
    end
end

