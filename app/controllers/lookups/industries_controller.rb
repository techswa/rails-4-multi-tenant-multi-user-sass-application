
class Lookups::IndustriesController < ApplicationController

  respond_to :html
  before_action :set_lookups_industry, only: [:show, :edit, :update, :destroy]

  def index
    lookups_industries = Lookups::Industry.all
    @lookups_industries = lookups_industries.collect { |lookups_industry| Lookups::IndustryDecorator.new(lookups_industry, view_context) }
    respond_with(@lookups_industries)
  end

  def show
    @lookups_industry = Lookups::IndustryDecorator.new(@lookups_industry, view_context)
    respond_with(@lookups_industry)

  end

  def new
    @lookups_industry = Lookups::Industry.new
    respond_with(@lookups_industry)
  end

  def edit
  end

  def create
    @lookups_industry = Lookups::Industry.new(lookups_industry_params)
    @lookups_industry.save
    respond_with(@lookups_industry)
  end

  def update
    @lookups_industry.update(lookups_industry_params)
    respond_with(@lookups_industry)
  end

  def destroy
    @lookups_industry.destroy
    respond_with(@lookups_industry)
  end

  private
    def set_lookups_industry
      @lookups_industry = Lookups::Industry.find(params[:id])
    end

    def lookups_industry_params
      params.require(:lookups_industry).permit(:naics_code, :title, :note)
    end
end



# class Lookups::IndustriesController < ApplicationController
#   before_action :set_industry, only: [:show, :edit, :update, :destroy]
#
#   def index
#     @lookups_industries = Lookups::Industry.all
#     respond_with(@lookups_industries)
#   end
#
#   def show
#     respond_with(@industry)
#   end
#
#   def new
#     @industry = Lookups::Industry.new
#     respond_with(@industry)
#   end
#
#   def edit
#   end
#
#   def create
#     @industry = Lookups::Industry.new(industry_params)
#     @lookups_industry.save
#     respond_with(@industry)
#   end
#
#   def update
#     @lookups_industry.update(industry_params)
#     respond_with(@industry)
#   end
#
#   def destroy
#     @lookups_industry.destroy
#     respond_with(@industry)
#   end
#
#   private
#     def set_industry
#       @industry = Lookups::Industry.find(params[:id])
#     end
#
#     def industry_params
#       params.require(:industry).permit(:naics_code, :title, :note)
#     end
# end
