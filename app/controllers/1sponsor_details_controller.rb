class SponsorDetailsController < ApplicationController

  # Restrict this to sponsors only

  respond_to :html
  before_action :set_sponsor_detail, only: [:edit, :update, :destroy]
  #before_action :set_sponsor, only: :show
  #before_action :verify_sponsor_detail_record_exists, only: :show

  def index
    sponsor_details = SponsorDetail.all
    @sponsor_details = sponsor_details.collect { |sponsor_detail| SponsorDetailDecorator.new(sponsor_detail,view_context) }
    respond_with(@sponsor_details)
  end

  def show
    # Get the current user's sponsor record
    # sponsor_id = Sponsor.where(user_id: current_user.id).first

    # Get the sponsoree record
    sponsoree = SponsorService::GetSponsoreeRecord.call(current_user)

    sponsor_detail = SponsorDetail.where(sponsor_id: sponsoree.id).first

    # Has this sponsor completed their ad details?
    if sponsor_detail.nil?
      puts 'render new'
      redirect_to :new_sponsor_detail
    else
      @sponsor_detail = SponsorDetailDecorator.new(sponsor_detail, view_context)
      respond_with(@sponsor_detail)
    end
  end

  def new
    puts 'sponsor_details#new'
    @sponsor_detail = SponsorDetail.new
    respond_with(@sponsor_detail)
  end

  def edit
  end

  def create
    @sponsor_detail = SponsorDetail.new(sponsor_detail_params)
    @sponsor_detail.sponsor_id = SponsorService::GetSponsoreeRecord.call(current_user).id
    # @sponsor.sponsor_id = Sponsor.where(user_id: current_user_id).first.id
    @sponsor_detail.save
    respond_with(@sponsor_detail)
  end

  def update
    @sponsor_detail.update(sponsor_detail_params)
    respond_with(@sponsor_detail)
  end

  def destroy
    @sponsor_detail.destroy
    respond_with(@sponsor_detail)
  end

  private

    def set_sponsor
      @sponsor = Sponsor.where(user_id: current_user.id).first
      puts "set_sponsor #{@sponsor}"
    end

    def verify_sponsor_detail_record_exists
      #If no record is found the redirect to new
      # Look for the record that belongs to this person
      @sponsor_detail = SponsorDetail.where(sponsor_id: @sponsor.id).first
      puts "@sponsor_detail: #{@sponsor_detail.inspect}"
      if @sponser_detail == nil?
        redirect_to new_sponsor_detail
      end
    end

    def set_sponsor_detail
      @sponsor_detail = SponsorDetail.find(params[:id])
    end

    def sponsor_detail_params
      params.require(:sponsor_detail).permit(:sponsor_id, :company_name, :contact_last_name, :contact_first_name, :phone_1, :phone_2, :website, :email, :tag_line_1, :tag_line_2, :welcome_greeting, :return_greeting, :logo_file, :headshot_file)
    end
end
