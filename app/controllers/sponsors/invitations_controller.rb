class Sponsors::InvitationsController < ApplicationController

   rescue_from ActiveRecord::RecordNotFound, with: :campaign_not_found
   rescue_from PG::InvalidTextRepresentation, with: :invalid_campaign_id

  #  before_action ->(campaign_id=params[:id]) {verify_campaign_exists campaign_id}, only:%w{invitation}

  def index
    render 'unfortunately'
  end

  def invitation
    # get the campaign_id from parameters
    campaign_id = params[:campaign_id]

    puts "campaign_id #{campaign_id}"

    if campaign_id
      puts "Entered if campaign_id"
      @campaign_record = CampaignService::GetCampaignRecordById.call(campaign_id)
      # rescue ActiveRecord::RecordNotFound
      #   redirect_to :controller => "sponsors_invitations", :action => "index"
      #   return
      # end
      # Get sponsor status
      sponsor_id = @campaign_record.sponsor_id
      sponsor = Sponsor.find(sponsor_id)
      puts "Sponsors::Invitations sponsor : #{sponsor.inspect}"

      # pass sponsor and advertisment_i

      @ad_html = AdvertisementService::AdComposer.call(sponsor, @campaign_record)
      puts "@ads_html #{@ad_html}"
      # @ad_html = '<div><b>Ad Goes Here</b></div>'
      # Ad to be shown
      #  @campaign_record.advertisements_advertisement_id

      # Returns hash with active, suspended, grace_ends and grace_remaining
      # [active: {true,false}, suspended: {true, false}, grace_ends: date, grace_remaining: {0-x in days}]
      # @sponsor_account_status = AccountingService::GetAccountStatusBySponsorId.call(sponsor_id)

      # Save the campaign_id to a cookie for use in registration
      puts 'Saving Sponsors cookie'
      cookies.permanent.signed['campaign'] = campaign_id

        # render 'sponsors/welcome/new'

    else
      render 'unfortunately'
    end
  end

  def show

  end

  def unfortunately

  end

private
  #Has a valid campaign been submitted
  def verify_campaign_exists campaign_id

    marketing_campaign = Marketing::Campaign.find(campaign_id)

    # Send the user away if the campaign doesn't exist
    # otherwise continue
    render '/welcome' unless marketing_campaign

  end


  def campaign_not_found
    redirect_to sponsors_invitations_unfortunately_path, :notice => 'Campaign record not found'
  end

  def invalid_campaign_id
    puts 'Bad UUID in campaign'
    redirect_to sponsors_invitations_unfortunately_path, :notice => 'Campaign record not found'
  end

end
