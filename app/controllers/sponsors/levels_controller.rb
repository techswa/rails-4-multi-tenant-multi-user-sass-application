class Sponsors::LevelsController < ApplicationController
  before_action :set_sponsors_level, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    sponsors_levels = Sponsors::Level.all
    @sponsors_levels = sponsors_levels.collect { |sponsors_level| Sponsors::LevelDecorator.new(sponsors_level, view_context) }
    respond_with(@sponsors_levels)
  end

  def show
    @sponsors_level = Sponsors::LevelDecorator.new(@sponsors_level, view_context)
    respond_with(@sponsors_level)
  end

  def new
    @sponsors_level = Sponsors::Level.new
    respond_with(@sponsors_level)
  end

  def edit
  end

  def create
    @sponsors_level = Sponsors::Level.new(sponsors_level_params)
    @sponsors_level.save
    respond_with(@sponsors_level)
  end

  def update
    @sponsors_level.update(sponsors_level_params)
    respond_with(@sponsors_level)
  end

  def destroy
    @sponsors_level.destroy
    respond_with(@sponsors_level)
  end

  private
    def set_sponsors_level
      @sponsors_level = Sponsors::Level.find(params[:id])
    end

    def sponsors_level_params
      params.require(:sponsors_level).permit(:title, :description, :archived)
    end
end
