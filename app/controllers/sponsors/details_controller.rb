class Sponsors::DetailsController < ApplicationController

  # Restrict this to sponsors only

  respond_to :html
   before_action :set_sponsors_detail, only: [:show, :edit, :update, :destroy]
  layout "sponsor"

  respond_to :html

  before_action :set_sponsors_detail, only: [:show, :edit, :update, :destroy]


  # before_action :set_sponsor, only: :show
  #before_action :verify_sponsor_detail_record_exists, only: :show

  def index

    sponsors_details = Sponsors::Detail.all
    @sponsors_details = sponsors_details.collect { |sponsors_detail| Sponsors::DetailDecorator.new(sponsors_detail,view_context) }
    respond_with(@sponsors_details)
  end

  def show
    # Get the current user's sponsor record
    # sponsor_id = Sponsor.where(user_id: current_user.id).first

    # Get the sponsoree record
    # sponsoree = SponsorService::GetSponsoreeRecord.call(current_user)
    # puts ''
    # sponsors_detail = Sponsors::Detail.where(sponsor_id: params[:id]).first
    # puts "sponsor_detail: #{sponsors_detail.inspect}"
    # puts ''
    # Has this sponsor completed their ad details?
    # if sponsor_detail.nil?
    #   puts 'render new'
    #   redirect_to :new_sponsor_detail
    # else
    # @sponsors_type = Sponsors::TypeDecorator.new(@sponsors_type, view_context)
    # respond_with(@sponsors_type)

      @sponsors_detail = Sponsors::DetailDecorator.new(@sponsors_detail, view_context)

    # Only display for staff
    unless current_sponsor.nil?
      redirect_to '/myportal'
    else
    sponsors_details = Sponsors::Detail.all
    @sponsors_details = sponsors_details.collect { |sponsors_detail| Sponsors::DetailDecorator.new(sponsors_detail,view_context) }
    respond_with(@sponsors_details)
    end
  end

  def show
          @sponsors_detail = Sponsors::DetailDecorator.new(@sponsors_detail, view_context)

      respond_with(@sponsors_detail)
    # end
  end

  def new
    puts 'sponsors_details#new'
    @sponsors_detail = SponsorsDetail.new
    respond_with(@sponsors_detail)
  end

  def edit
  end

  def create
    @sponsors_detail = Sponsors::Detail.new(sponsors_detail_params)
    @sponsors_detail.sponsor_id = SponsorService::GetSponsoreeRecord.call(current_user).id
    # @sponsor.sponsor_id = Sponsor.where(user_id: current_user_id).first.id
    @sponsor_detail.save
    respond_with(@sponsors_detail)
  end

  def update
    @sponsors_detail.update(sponsors_detail_params)
    respond_with(@sponsors_detail)
  end

  def destroy
    @sponsors_detail.destroy
    respond_with(@sponsors_detail)
  end

  private

    #
    # def set_sponsor
    #   # @sponsor = Sponsor.where(user_id: current_user.id).first
    #   @sponsors_
    #   puts "set_sponsor #{@sponsor}"
    # end
    #
    # def verify_sponsor_detail_record_exists
    #   #If no record is found the redirect to new
    #   # Look for the record that belongs to this person
    #   @sponsors_detail = Sponsors::Detail.where(sponsor_id: @sponsor.id).first
    #   puts "@sponsor_detail: #{@sponsors_detail.inspect}"
    #   if @sponsers_detail == nil?
    #     redirect_to new_sponsors_detail
    #   end
    # end

    def set_sponsors_detail

      @sponsors_detail = Sponsors::Detail.find(params[:id])

      # @sponsors_detail = Sponsors::Detail.find(params[:id])
      sponsor_id = current_sponsor.id
      # Get the current user's sponsor record unless staff
      @sponsors_detail = Sponsors::Detail.where(sponsor_id: sponsor_id).first

    end

    def sponsors_detail_params
      params.require(:sponsors_detail).permit(:sponsor_id, :company_name, :contact_last_name, :contact_first_name, :phone_1, :phone_2, :website, :email, :tag_line_1, :tag_line_2, :welcome_greeting, :return_greeting, :logo_file, :headshot_file, :sponsors_style_id)
    end
end
