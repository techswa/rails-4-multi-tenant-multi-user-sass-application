class Sponsors::ClassesController < ApplicationController
  before_action :set_sponsors_class, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    sponsors_classes = Sponsors::Class.all
    @sponsors_classes = sponsors_classes.collect { |sponsors_class| Sponsors::ClassDecorator.new(sponsors_class, view_context) }
    # @sponsors_classes = Sponsors::Class.all
    respond_with(@sponsors_classes)
  end

  def show
    @sponsors_class = Sponsors::ClassDecorator.new(@sponsors_class, view_context)
     respond_with(@sponsors_class)
  end

  def new
    @sponsors_class = Sponsors::Class.new
    respond_with(@sponsors_class)
  end

  def edit
  end

  def create
    @sponsors_class = Sponsors::Class.new(sponsors_class_params)
    @sponsors_class.save
    respond_with(@sponsors_class)
  end

  def update
    @sponsors_class.update(sponsors_class_params)
    respond_with(@sponsors_class)
  end

  def destroy
    @sponsors_class.destroy
    respond_with(@sponsors_class)
  end

  private
    def set_sponsors_class
      @sponsors_class = Sponsors::Class.find(params[:id])
    end

    def sponsors_class_params
      params.require(:sponsors_class).permit(:name, :description, :archived)
    end
end
