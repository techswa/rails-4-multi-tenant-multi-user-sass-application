class Sponsors::FranchisesController < ApplicationController
  before_action :set_sponsors_franchise, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    sponsors_franchises = Sponsors::Franchise.all
    @sponsors_franchises = sponsors_franchises.collect { |sponsors_franchise| Sponsors::FranchiseDecorator.new(sponsors_franchise,view_context) }
    respond_with(@sponsors_franchises)
  end

  def show
    @sponsors_franchise = Sponsors::FranchiseDecorator.new(@sponsors_franchise, view_context)
    respond_with(@sponsors_franchise)
  end

  def new
    @sponsors_franchise = Sponsors::Franchise.new
    respond_with(@sponsors_franchise)
  end

  def edit
  end

  def create
    @sponsors_franchise = Sponsors::Franchise.new(sponsors_franchise_params)
    @sponsors_franchise.save
    respond_with(@sponsors_franchise)
  end

  def update
    @sponsors_franchise.update(sponsors_franchise_params)
    respond_with(@sponsors_franchise)
  end

  def destroy
    @sponsors_franchise.destroy
    respond_with(@sponsors_franchise)
  end

  private
    def set_sponsors_franchise
      @sponsors_franchise = Sponsors::Franchise.find(params[:id])
    end

    def sponsors_franchise_params
      params.require(:sponsors_franchise).permit(:title, :description, :archived)
    end
end
