
# Used to override SponsorCtrl for Devise processes
#puts Allows Sponsor Ctrl to be used for admin interaction

class Sponsors::RegistrationsController < Devise::RegistrationsController


  def new
    puts 'Sponsors::RegistrationController'
    @sponsor = Sponsor.new
    @sponsor.campaign_id = params[:campaign_id]
    #todo comment
    puts "Sponsors::Regisitration.new #{@sponsor.inspect}"

    # super
  end

  def create

    #todo Add campaign for default for when they don't have a sponsor
    # @sponsor = Sponsor.new(sign_up_params)
    # super
    #  :new_sponsor is used to skip extra validations
    @sponsor = Sponsor.new(sign_up_params)

    # Disable extra validation that is used during the next step
    # Uses :new_sponsor_sign_up attribute on the model to bypass certain validations
    @sponsor.new_sponsor_sign_up = true

    @sponsor.save

    # Only add campign record if the new sponsor record as been saved
     unless @sponsor.id.nil?
      # Add new Sponsor and their sponsor to the Sponsorships::Business
      my_sponsor = Marketing::Campaign.find(sign_up_params[:campaign_id])
      sponsorship = Sponsorships::Business.new(sponsor_id: my_sponsor.id, sponsored_id: @sponsor.id, marketing_campaign_id: @sponsor.campaign_id )
      sponsorship.save
     end
    respond_with(@sponsor)
  end


  protected

  def sign_up(resource_name, resource)

    # Auto sign_in new sponsor
    sign_in(:sponsor, resource)
    # Send them to the next stage
    # Should redirect to Sponsor's portal
    sponsors_portals_myportal_path
  end

  def after_sign_up_path_for(resource)
    # Need to complete sponsor details
    # Should redirect to Sponsor's portal
    sponsors_portals_myportal_path
  end

private
  def sign_up_params
    params.require(:sponsor).permit(:campaign_id, :user_name, :email, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:sponsor).permit(:user_name, :email, :password, :password_confirmation, :current_password)
  end



end
