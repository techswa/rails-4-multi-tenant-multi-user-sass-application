
class Sponsors::TypesController < ApplicationController

  respond_to :html
  before_action :set_sponsors_type, only: [:show, :edit, :update, :destroy]

  def index
    sponsors_types = Sponsors::Type.all
    @sponsors_types = sponsors_types.collect { |sponsors_type| Sponsors::TypeDecorator.new(sponsors_type, view_context) }
    respond_with(@sponsors_types)
  end

  def show
    @sponsors_type = Sponsors::TypeDecorator.new(@sponsors_type, view_context)
    respond_with(@sponsors_type)

  end

  def new
    @sponsors_type = Sponsors::Type.new
    respond_with(@sponsors_type)
  end

  def edit
  end

  def create
    @sponsors_type = Sponsors::Type.new(sponsors_type_params)
    @sponsors_type.save
    respond_with(@sponsors_type)
  end

  def update
    @sponsors_type.update(sponsors_type_params)
    respond_with(@sponsors_type)
  end

  def destroy
    @sponsors_type.destroy
    respond_with(@sponsors_type)
  end

  private
    def set_sponsors_type
      @sponsors_type = Sponsors::Type.find(params[:id])
    end

    def sponsors_type_params
      params.require(:sponsors_type).permit(:title, :description, :symbol_name, :archived)
    end
end



