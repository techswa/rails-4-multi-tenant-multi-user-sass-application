# Note that we are extending
# from: Devise::SessionsController
class Sponsors::SessionsController < Devise::SessionsController

  respond_to :html

  # puts '----Sponsors::SessionCtrl----'
  # puts current_sponsor.inspect
  # GET /resource/sign_in

  def create

    puts 'Entered Sponsors::SessionsController::Create'
    puts "parameters: #{params.inspect}"

    puts "auth_options: #{auth_options}"
    self.resource = warden.authenticate!(auth_options)
    puts "resource_name: #{resource_name}"
    puts "resource: #{self.resource}"
    sign_in(resource_name, resource)
    # session[:current_sponsor] = current_sponsor

    puts "beginning respond_with resource type : #{resource}"
    respond_with resource, location: after_sign_in_path_for(resource)
    puts 'finished respond_with'

    # respond_to do |format|
    #
    #   # format.html { super }
    #   format.html do
    #     # respond_with resource, location: after_sign_in_path_for(resource)

    #     # # puts "sessions html resource: #{self.resource.inspect}"
    #     # puts '----- current_sponsor -----'
    #     # puts current_sponsor.inspect
    #     # redirect_to sponsors_portals_myportal_path

    #   end
    #
    #   format.json do
    #     # puts "auth_options: #{auth_options}"
    #     # self.resource = warden.authenticate!(auth_options)
    #     # puts "resource_name: #{resource_name}"
    #     # puts "resource: #{self.resource}"
    #     # sign_in(resource_name, resource)
    #     data = {
    #         user_token: self.resource.authentication_token,
    #         # user_email: self.resource.email
    #         user_name: self.resource.user_name
    #     }
    #     render json: data, status: 201
    #   end
    # end
  end


  #DELETE /resource/sign_out
  def destroy

    respond_to do |format|
      format.html do
        super # Use normal behavior for html
      end
      format.json {
        if current_user
          current_user.update authentication_token: nil
          signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
          render :json => {}.to_json, :status => :ok
        else
          render :json => {}.to_json, :status => :unprocessable_entity
        end


      }
    end
  end

end
