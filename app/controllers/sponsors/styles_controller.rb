class Sponsors::StylesController < ApplicationController
  before_action :set_sponsors_style, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    sponsors_styles = Sponsors::Style.all
    @sponsors_styles = sponsors_styles.collect { |sponsors_style| Sponsors::StyleDecorator.new(sponsors_style, view_context) }
    respond_with(@sponsors_styles)
  end

  def show
    @sponsors_style = Sponsors::StyleDecorator.new(@sponsors_style, view_context)
    respond_with(@sponsors_style)
  end

  def new
    @sponsors_style = Sponsors::Style.new
    respond_with(@sponsors_style)
  end

  def edit
  end

  def create
    @sponsors_style = Sponsors::Style.new(sponsors_style_params)
    @sponsors_style.save
    respond_with(@sponsors_style)
  end

  def update
    @sponsors_style.update(sponsors_style_params)
    respond_with(@sponsors_style)
  end

  def destroy
    @sponsors_style.destroy
    respond_with(@sponsors_style)
  end

  private
    def set_sponsors_style
      @sponsors_style = Sponsors::Style.find(params[:id])
    end

    def sponsors_style_params
      params.require(:sponsors_style).permit(:title, :description, :archived)
    end
end
