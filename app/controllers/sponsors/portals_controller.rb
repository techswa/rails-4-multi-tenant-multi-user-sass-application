# Please enter the model's name: Sponsors::Portal
#
# Please enter the instance name: sponsors_portal

class Sponsors::PortalsController < ApplicationController


  layout "sponsor"


  layout "sponsor"



  respond_to :html

  # Is someone signed in
  before_filter :authenticate_sponsor!
  puts 'after authenticate_sponsor'
  # is the a signed in sponsor
  # logger.info session.inspect
  before_action :additional_information_needed
  before_action :active_sponsor_session?


  # before_action :set_sponsors_portal, only: [:show, :edit, :update, :destroy]

  def myportal

    # Define advertisement mechanical to be shown via a symbol
    mechanical_to_be_shown = 'sponsor_portal_banner'.to_sym

    # For testing without devise
    #Currently logged in Sponsor
    # sponsor is MW
    # current_sponsor = Sponsor.find('a67fc27a-573c-4bc9-9145-39fac1902ed4')

    puts 'myportal current sponsor'
    puts current_sponsor.inspect
    # Retrieve sponsor ad for the current sponsor and screen
    # AdManagerSponsor display the correct ad to a Realtor/Consumer Sponsor
    advertisement_template = AdvertisementService::AdManagerSponsor.call(current_sponsor, mechanical_to_be_shown)
    puts ''
    puts 'Back in the portals_controller'
    puts "Advertisement template returned is: #{advertisement_template.inspect}"
    puts ''

    sponsorship_record = Sponsorships::Business.where(sponsor_id: current_sponsor.id).where(active: true).first
    puts "sponsorship_record #{sponsorship_record.inspect}"
    puts ''

    current_vendor = Sponsor.find(sponsorship_record.vendor_id)
    puts "Current vendor: #{current_vendor.inspect}"
    puts ''

    current_vendor_details = Sponsors::Detail.where(sponsor_id: current_vendor.id).first
    puts "current vendor details: #{current_vendor_details.inspect}"
    puts ''
    vendor_liquid_drop = VendorDrop.new(current_vendor_details)
    puts "vendor drop: #{vendor_liquid_drop.inspect}"
    puts ''
    # Create sponsor drop
    sponsor_liquid_drop = SponsorDrop.new(current_sponsor)
     puts '---------'
     puts sponsor_liquid_drop.inspect
     puts '---------'
     puts 'Parsing the template'
     merge_ready_template = Liquid::Template.parse(advertisement_template.code)

     puts merge_ready_template.inspect

    # @portal_banner_advertisement = merge_ready_template.render('sponsor' => sponsor_liquid_drop )
    @portal_banner_advertisement = merge_ready_template.render('vendor' => vendor_liquid_drop )

    # Retrieve records for partial displays

    # Sponsored clients
    # Find all that belong to a vendor throught sponsorhips::business
    sponsorships = Sponsorships::Business.where(vendor_id: current_sponsor.id).where(active: true)
    @sponsorships = sponsorships.collect { |sponsorship| Sponsorships::BusinessDecorator.new(sponsorship, view_context) }
    puts "Vendor's sponsorships: #{@sponsorships.inspect}"


    sponsors = Sponsor.all
    @sponsors = sponsors.collect { |sponsor| Sponsors::SponsorDecorator.new(sponsor, view_context) }

    #Advertisement rotations
    # @rotations = Advertisements::Rotation.all
    advertisements_rotations = Advertisements::Rotation.where(sponsor_id: current_sponsor.id)
    @advertisements_rotations = advertisements_rotations.collect { |advertisements_rotation| Advertisements::RotationDecorator.new(advertisements_rotation, view_context) }

    # Advertisements
    # @advertisements = Advertisements::Advertisement.where(sponsor_id: current_sponsor.id)
    advertisements_advertisements = Advertisements::Advertisement.where(sponsor_id: current_sponsor.id)
    @advertisements_advertisements = advertisements_advertisements.collect { |advertisements_advertisement| Advertisements::AdvertisementDecorator.new(advertisements_advertisement, view_context) }


  end

  def index
    sponsors_portals = Sponsors::Portal.all
    @sponsors_portals = sponsors_portals.collect { |sponsors_portal| Sponsors::PortalDecorator.new(sponsors_portal, view_context) }
    respond_with(@sponsors_portals)
  end

  def show
    @sponsors_portal = Sponsors::PortalDecorator.new(@sponsors_portal, view_context)
    respond_with(@sponsors_portal)

  end

  def new
    @sponsors_portal = Sponsors::Portal.new
    respond_with(@sponsors_portal)
  end

  def edit
  end

  def create
    @sponsors_portal = Sponsors::Portal.new(sponsors_portal_params)
    @sponsors_portal.save(validate: false)
    respond_with(@sponsors_portal)
  end

  def update
    @sponsors_portal.update(sponsors_portal_params)
    respond_with(@sponsors_portal)
  end

  def destroy
    @sponsors_portal.destroy
    respond_with(@sponsors_portal)
  end

  private

    def additional_information_needed
      # If the sponsor is missing details then take them to the update area
      return
      redirect_to edit_sponsor_path current_sponsor
    end

    def active_sponsor_session?
      unless sponsor_signed_in?
        puts 'Sponsor not signed in - redirecting'
        redirect_to new_sponsor_session_path
      end
    end

    def set_sponsors_portal
      @sponsors_portal = Sponsors::Portal.find(params[:id])
    end

    def sponsors_portal_params
      params.require(:sponsors_portal).permit(:sponsor_id, :portal_setting_id, :disabled)
    end
end
