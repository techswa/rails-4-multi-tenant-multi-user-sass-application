
class Accounting::StatusesController < ApplicationController

  respond_to :html
  before_action :set_accounting_status, only: [:show, :edit, :update, :destroy]

  def index
    accounting_statuses = Accounting::Status.all
    @accounting_statuses = accounting_statuses.collect { |accounting_status| Accounting::StatusDecorator.new(accounting_status, view_context) }
    respond_with(@accounting_statuses)
  end

  def show
    @accounting_status = Accounting::StatusDecorator.new(@accounting_status, view_context)
    respond_with(@accounting_status)

  end

  def new
    @accounting_status = Accounting::Status.new
    respond_with(@accounting_status)
  end

  def edit
  end

  def create
    @accounting_status = Accounting::Status.new(accounting_status_params)
    @accounting_status.save
    respond_with(@accounting_status)
  end

  def update
    @accounting_status.update(accounting_status_params)
    respond_with(@accounting_status)
  end

  def destroy
    @accounting_status.destroy
    respond_with(@accounting_status)
  end

  private
    def set_accounting_status
      @accounting_status = Accounting::Status.find(params[:id])
    end

    def accounting_status_params
      params.require(:accounting_status).permit(:sponsors_sponsor_id, :suspended, :grace)
    end
end



# class Accounting::StatusesController < ApplicationController
#   before_action :set_status, only: [:show, :edit, :update, :destroy]
#
#   def index
#     @accounting_statuses = Accounting::Status.all
#     respond_with(@accounting_statuses)
#   end
#
#   def show
#     respond_with(@status)
#   end
#
#   def new
#     @status = Accounting::Status.new
#     respond_with(@status)
#   end
#
#   def edit
#   end
#
#   def create
#     @status = Accounting::Status.new(status_params)
#     @accounting_status.save
#     respond_with(@status)
#   end
#
#   def update
#     @accounting_status.update(status_params)
#     respond_with(@status)
#   end
#
#   def destroy
#     @accounting_status.destroy
#     respond_with(@status)
#   end
#
#   private
#     def set_status
#       @status = Accounting::Status.find(params[:id])
#     end
#
#     def status_params
#       params.require(:status).permit(:sponsors_sponsor_id, :suspended, :grace)
#     end
# end
