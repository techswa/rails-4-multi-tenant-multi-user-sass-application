class Accounting::PaymentsController < ApplicationController
  before_action :set_payment, only: [:show, :edit, :update, :destroy]

  def index
    @accounting_payments = Accounting::Payment.all
    respond_with(@accounting_payments)
  end

  def show
    respond_with(@payment)
  end

  def new
    @payment = Accounting::Payment.new
    respond_with(@payment)
  end

  def edit
  end

  def create
    @payment = Accounting::Payment.new(payment_params)
    @accounting_payment.save
    respond_with(@payment)
  end

  def update
    @accounting_payment.update(payment_params)
    respond_with(@payment)
  end

  def destroy
    @accounting_payment.destroy
    respond_with(@payment)
  end

  private
    def set_payment
      @payment = Accounting::Payment.find(params[:id])
    end

    def payment_params
      params.require(:payment).permit(:sponsor_id, :accounting_payment_type_id, :amount, :staff_id, :note)
    end
end
