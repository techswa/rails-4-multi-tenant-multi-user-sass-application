# Please enter the model's name: Accounting::PaymentType
#
# Please enter the instance name: accounting_payment_type

class Accounting::PaymentTypesController < ApplicationController

  respond_to :html
  before_action :set_accounting_payment_type, only: [:show, :edit, :update, :destroy]

  def index
    accounting_payment_types = Accounting::PaymentType.all
    @accounting_payment_types = accounting_payment_types.collect { |accounting_payment_type| Accounting::PaymentTypeDecorator.new(accounting_payment_type, view_context) }
    respond_with(@accounting_payment_types)
  end

  def show
    @accounting_payment_type = Accounting::PaymentTypeDecorator.new(@accounting_payment_type, view_context)
    respond_with(@accounting_payment_type)

  end

  def new
    @accounting_payment_type = Accounting::PaymentType.new
    respond_with(@accounting_payment_type)
  end

  def edit
  end

  def create
    @accounting_payment_type = Accounting::PaymentType.new(accounting_payment_type_params)
    @accounting_payment_type.save
    respond_with(@accounting_payment_type)
  end

  def update
    @accounting_payment_type.update(accounting_payment_type_params)
    respond_with(@accounting_payment_type)
  end

  def destroy
    @accounting_payment_type.destroy
    respond_with(@accounting_payment_type)
  end

  private
    def set_accounting_payment_type
      @accounting_payment_type = Accounting::PaymentType.find(params[:id])
    end

    def accounting_payment_type_params
      params.require(:accounting_payment_type).permit(:name, :sort)
    end
end



# class Accounting::PaymentTypesController < ApplicationController
#   before_action :set_payment_type, only: [:show, :edit, :update, :destroy]
#
#   def index
#     @accounting_payment_types = Accounting::PaymentType.all
#     respond_with(@accounting_payment_types)
#   end
#
#   def show
#     respond_with(@payment_type)
#   end
#
#   def new
#     @payment_type = Accounting::PaymentType.new
#     respond_with(@payment_type)
#   end
#
#   def edit
#   end
#
#   def create
#     @payment_type = Accounting::PaymentType.new(payment_type_params)
#     @accounting_payment_type.save
#     respond_with(@payment_type)
#   end
#
#   def update
#     @accounting_payment_type.update(payment_type_params)
#     respond_with(@payment_type)
#   end
#
#   def destroy
#     @accounting_payment_type.destroy
#     respond_with(@payment_type)
#   end
#
#   private
#     def set_payment_type
#       @payment_type = Accounting::PaymentType.find(params[:id])
#     end
#
#     def payment_type_params
#       params.require(:payment_type).permit(:name, :sort)
#     end
# end
