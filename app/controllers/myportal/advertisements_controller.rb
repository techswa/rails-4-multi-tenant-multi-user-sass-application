
class Myportal::AdvertisementsController < ApplicationController

  layout 'sponsor'

  respond_to :html
  before_action :set_advertisements_advertisement, only: [:show, :edit, :update, :destroy]

  def index
      # Basic index code
      advertisements_advertisements = Advertisements::Advertisement.where(sponsor_id: current_sponsor.id)
      @advertisements_advertisements = advertisements_advertisements.collect { |advertisements_advertisement| Advertisements::AdvertisementDecorator.new(advertisements_advertisement, view_context) }

      #Display portal ad banner
      @portal_banner_advertisement_html = create_advertisement_html(current_sponsor, :sponsor_portal_banner)

      #Display test code
      @available_company_advertisements = get_sponsors_advertisement_previews_html current_sponsor

      #@return_visit_advertisement = create_advertisement_preview_html(current_sponsor, :sponsor_portal_banner)

      respond_with(@advertisements_advertisements)
    # end
  end

  def show
    @advertisements_advertisement = Advertisements::AdvertisementDecorator.new(@advertisements_advertisement, view_context)
    respond_with(@advertisements_advertisement)
  end

  def new
    @advertisements_advertisement = Advertisements::Advertisement.new
    respond_with(@advertisements_advertisement)
  end

  def edit

  end

  def create
    # Set the sponsor_id to the current_sponsor
    modified_params = advertisements_advertisement_params
    modified_params[:sponsor_id] = current_sponsor.id

    # Set the advertisements_group_id to advertisments_position.advertisements_group_id
    advertisements_group_id = Advertisements::Position.find(modified_params[:advertisements_position_id]).advertisements_group_id
    modified_params[:advertisements_group_id] = advertisements_group_id

    @advertisements_advertisement = Advertisements::Advertisement.new(modified_params)
    @advertisements_advertisement.save
    respond_with(@advertisements_advertisement)
  end

  def update
    @advertisements_advertisement.update(advertisements_advertisement_params)
    respond_with(@advertisements_advertisement)
  end

  def destroy
    @advertisements_advertisement.destroy
    respond_with(@advertisements_advertisement)
  end

  private

    def get_sponsors_advertisement_previews_html sponsor
      puts 'Entering sponsors_templates_html'

      # Create array to hold returned advertisement previews
      advertisement_preview_array = Array.new

      # Gather sponsor's advertisements
      sponsors_advertisements = Advertisements::Advertisement.where(sponsor_id: sponsor.id)

      # advertisement_template = Advertisements::Template.find('7d5fb6c5-6828-45ff-83ed-039c787b5c4b')

      puts "sponsors_advertisements: #{sponsors_advertisements.inspect}"

      sponsors_advertisements.each do |advertisement|
        # Get template
        advertisement_template = Advertisements::Template.find(advertisement.advertisements_template_id)

        #Create ad html
        advertisement_preview = create_advertisement_preview_html(sponsor, advertisement_template)

        # Append to array for display
        advertisement_preview_array << advertisement_preview
        puts '***********'
        puts advertisement_preview_array.inspect
        puts '***********'
      end
      # Return array of ads
      advertisement_preview_array

    end


    # Used to display the preview of the sponsor's advertisement
    def create_advertisement_preview_html(sponsor, advertisement_template)
      # # AdManagerSponsor display the correct ad to a Realtor/Consumer Sponsor
      # advertisement_template = AdvertisementService::AdManagerSponsor.call(sponsor, advertisement_type_symbol)

      #
      # # Find this company's sponsorship record
      # sponsorship_record = Sponsorships::Business.where(sponsor_id: current_sponsor.id).where(active: true).first
      #
      # # Retreive this company's vendor
      # current_vendor = Sponsor.find(sponsorship_record.vendor_id)
      #
      # current_vendor_details = Sponsors::Detail.where(sponsor_id: current_vendor.id).first

      current_vendor_details = Sponsors::Detail.where(sponsor_id: sponsor.id).first

      vendor_liquid_drop = VendorDrop.new(current_vendor_details)

      # Create sponsor drop
      # sponsor_liquid_drop = SponsorDrop.new(current_sponsor)

      merge_ready_template = Liquid::Template.parse(advertisement_template.code)

      # @portal_banner_advertisement = merge_ready_template.render('sponsor' => sponsor_liquid_drop )
      merge_ready_template.render('vendor' => vendor_liquid_drop )

    end


    def create_advertisement_html(sponsor, advertisement_type_symbol)

      # AdManagerSponsor display the correct ad to a Realtor/Consumer Sponsor
      advertisement_template = AdvertisementService::AdManagerSponsor.call(sponsor, advertisement_type_symbol)

      # Find this company's sponsorship record
      sponsorship_record = Sponsorships::Business.where(sponsor_id: current_sponsor.id).where(active: true).first

      # Retreive this company's vendor
      current_vendor = Sponsor.find(sponsorship_record.vendor_id)

      current_vendor_details = Sponsors::Detail.where(sponsor_id: current_vendor.id).first

      vendor_liquid_drop = VendorDrop.new(current_vendor_details)

      # Create sponsor drop
      sponsor_liquid_drop = SponsorDrop.new(current_sponsor)

      merge_ready_template = Liquid::Template.parse(advertisement_template.code)

      # @portal_banner_advertisement = merge_ready_template.render('sponsor' => sponsor_liquid_drop )
      merge_ready_template.render('vendor' => vendor_liquid_drop )
    end

# Standard controller code
    def set_advertisements_advertisement
      # @advertisements_advertisement = Advertisements::Advertisement.find(params[:id])
      sponsor_id = current_sponsor.id
      # puts "ad_ad current_sponsor #{current_sponsor}"
      @advertisements_advertisement = Advertisements::Advertisement.where(id: params[:id]).where(sponsor_id: sponsor_id)
    end

    def advertisements_advertisement_params
      params.require(:advertisements_advertisement).permit(:sponsor_id, :title, :description, :revision, :advertisements_group_id, :advertisements_template_id, :images_imgfile_id, :archived)
    end
end
