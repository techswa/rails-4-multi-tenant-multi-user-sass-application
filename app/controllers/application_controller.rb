class ApplicationController < ActionController::Base


  # # select layout for user
  # layout :layout_to_use

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_rails forgery with: :exception
  #include ActionController::HttpAuthentication::Token::ControllerMethods

  # before_action :authenticate_user_from_token!
  #before_filter :authenticate_user!

  # if sponsor_signed_in?
  #   puts "Application Controller current_sponsor: #{sponsor_signed_in?}"
  # end
  protect_from_forgery with: :null_session,
    if: Proc.new { |c| c.request.format == 'application/json' }


   before_action :authenticate_sponsor!, except: [:'sponsors/invitations', :invitation]
                #  [:sponsors, :index, :show]
                #  except: [:sponsors, :index, :show]


  before_action :get_users_sponsor_name, if: :user_signed_in?

  #Create @roles that can be checked by current_user_has_role? :role_symbol
  before_action :set_current_user_role, if: :user_signed_in?

  # skip_before_action :verify_authenticity_token, if: :json_request?
  # http://edgeapi.rubyonrails.org/classes/ActionController/RequestForgeryProtection.html#method-i-protect_against_forgery-3F


  # after_filter :cors_set_access_control_headers
  #
  # def cors_set_access_control_headers
  #   headers['Access-Control-Allow-Origin']      = '*'
  #   headers['Access-Control-Allow-Methods']     = 'POST, GET, OPTIONS'
  #   headers['Access-Control-Max-Age']           = '1728000'
  #   headers['Access-Control-Allow-Credentials'] = 'true'
  # end

  # Redirect devise sign_in
  #  http://stackoverflow.com/questions/4987841/devise-redirect-after-sign-up
  def after_sign_in_path_for(resource)
    if resource.is_a?(Sponsor)

      # Test by sending to sponsors index
      # Should redirect to Sponsor's portal
      # sponsors_portals_myportal_path

      puts 'entered after_sign_in_path'
      # Test by sending to sponsors index
      # Should redirect to Sponsor's portal

      # # Change layout to sponsor version
      # layout "sponsor"

      myportal_path

    elsif Staff
      staff_portal_index_path
    else
      super
    end
  end
  def after_sign_out_path_for(resource_or_scope)
    # if resource_or_scope.is_a?(Sponsor)
    #   puts 'Sign out resource: Sponsor'
    #   root_path
    # end
    puts 'Sending to root path'

    myportal_path

  #
  # before_action :set_sponsor_id, if: :user_signed_in?
  #
  # before_action :current_user_sponsored_by, if: :user_signed_in?

  # Check for or issue system cookie
  #before_action :set_cookie_system, if: @cookie_system.blank?

  # Redirect devise sign_out
  #  http://stackoverflow.com/questions/4987841/devise-redirect-after-sign-up
  end


  # Global rescue_from go below.
  # rescue_from PG::InvalidTextRepresentation, with: :test_global_db_error

  ##############
  #
  # The following controller variables are set here
  #
  # @cookie_client_machine
  # @cookie_campaign
  # @cookie_sponsor
  # @cookie_user
  # @cookie_last_session
  # @current_user_role
  # @current_user_sponsored_by
  # @current_sponsor_id
  #
  #
  ##############

  # before_action :cookie_service_get_client_cookies
  # before_action :cookie_service_update


  #Add this session to Visits if it is new
  # before_action :log_session


  # def test_global_db_error
  #   redirect_to sponsors_invitations_unfortunately_path
  # end

  # Set current users sponsors_id




  # Create service object for cookies
  def cookie_service_load
    cookie_hash =  get_client_cookies
    @cookie_service = CookieService::GetClientCookies.call(cookie_hash)
  end

  # Write current cookies to client system
  def cookie_service_update
    cookie_hash = {sign_in: 'lots of cheese', boat: 'water'}
    update_client_cookies cookie_hash
  end


  # get @current_user_sponsored_by
  def current_user_sponsored_by
    # Is user only a property owner
    if @current_user_role == :user
      @current_user_sponsored_by = Csponsorship.where(user_id: current_user.id).where.(archived: false).first.sponsor_id
      puts "@current_user_sponsored_by is #{@current_user_sponsored_by}, role: :user"
    else
      # User is a sponsor
      # Set @sponsor_id
      set_sponsor_id
      @current_user_sponsored_by = Bsponsorship.where(sponsoree_id: @sponsor_id).where(archived: false).first.sponsor_id
      puts "@current_user_sponsored_by is #{@current_user_sponsored_by}, role: :b2b"
    end
  end


  def get_users_sponsor_name
    if current_user.nil?
      puts 'Not logged in'
      return
    end
    puts "current_user #{current_user.inspect}"
    @sponsors_name = ''#SponsorService::SponsorsName.call(current_user)
    puts "Sponsors name #{@sponsors_name}"
  end

  # Set record id of Sponsor using the system
  # def set_sponsor_id
  #   @sponsor_id = Sponsor.where(user_id: current_user.id).first
  # end

  def current_user_has_role? (roles)
      puts "current_user_role: #{current_user.role}"
      puts '==== start test ====='
    roles.each do |role|
      puts "role being tested: #{role.to_s}"
      if @current_user_role == role
        puts "Role has matched"
        #Return on first match
        return true
      else
        puts "role doesn\'t match."
      end
      puts '==== end test ===='
    end
      # No match - return False
      puts 'No match found - redirect user'
      return FALSE
  end



  # Use this for authorization - role checks

  # Devise redirect upon signin
  # def after_sign_in_path_for(resource_or_scope)
  #   welcome_path
  # end

  #############
  #
  # Cookie getter and setter for use with CookieService
  # is not a private method
  #############

  # Loads client's cookies and returns a hash
  def client_cookies_getter
    cookie_hash =  {
      client_machine: cookies.permanent.signed[:client_machine],
      campaign: cookies.permanent.signed[:campaign],
      sponsor: cookies.permanent.signed[:sponsor],
      user: cookies.permanent.signed[:user],
      last_session: cookies.permanent.signed[:last_session]
    }
  end

  # Update cookies on client computer from a hash of cookies
  def client_cookies_setter cookie_hash
  # Only update cookies that are present in the hash
    cookie_hash.each do |key, value|
       cookies.permanent.signed[key] = value
    end
  end

  protected

  def json_request?
    request.format.json?
  end
  # The curl below works as 01/07/15
  # curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST -d '{"sponsor":{"user_name":"mouser","password":"password"}}' http://localhost:3000/sponsors/sign_in
  private

    #Determine layout to use
    def determine_layout current_user
      puts "current role is: #{current_user.role}"
      @layout_to_use = "sponsor"
    end


    def authenticate_user_from_token!
      puts 'Entered authenticate_user_from_token!'
      puts "http_token: #{authenticate_with_http_token.inspect} "
      authenticate_with_http_token do |token, options|
        puts "token: #{token}"
        puts "option: #{options}"
        user_name = options[:user_name].presence
        puts "user_name #{user_name}"
        user       = user_name && User.find_by_user_name(user_name)

        if user && Devise.secure_compare(user.authentication_token, token)
          sign_in user, store: false
        end
      end
    end

    #Create instance var for current_user role
    def set_current_user_role
      @current_user_role = current_user.role.to_sym
    end


end
