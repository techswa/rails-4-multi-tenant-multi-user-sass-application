class VendorDrop < Liquid::Drop


  def initialize(vendor)
    @vendor = vendor
  end

  def company_name
    @vendor["company_name"]
  end

  def first_name
    @vendor["first_name"]
  end

  def last_name
    @vendor["last_name"]
  end

  def tag_line_1
    @vendor["tag_line_1"]
  end

  def email
    @vendor["email"]
  end

  def style_id
    @vendor["style_id"]
  end

  def style_name
    @vendor.sponsors_style.title
  end
end
