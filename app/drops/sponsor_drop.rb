class SponsorDrop < Liquid::Drop


  def initialize(sponsor)
    @sponsor = sponsor
  end

  def business_name
    @sponsor["business_name"]
  end

  def first_name
    @sponsor["first_name"]
  end

  def last_name
    @sponsor["last_name"]
  end

end