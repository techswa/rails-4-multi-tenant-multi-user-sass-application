class Marketing::Channel < ActiveRecord::Base

  belongs_to :sponsor
  has_many :marketing_campaigns, :class_name => 'Marketing::Campaign'

end
