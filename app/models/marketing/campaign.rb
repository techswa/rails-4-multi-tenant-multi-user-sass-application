class Marketing::Campaign < ActiveRecord::Base

  belongs_to :sponsor
  belongs_to :marketing_channel, :class_name => 'Marketing::Channel'
  belongs_to :advertisements_rotation, :class_name => 'Advertisements::Rotation'


  has_many :sponsors
  has_many :sponsorships_businesses, :class_name => 'Sponsorships::Businesses'
  has_many :marketing_campaigns, :class_name => 'Marketing::Campaigns'

  # Increment responses on campaign ++
  def update_visitor_count campaign_id
    campaign = Campaign.find(campaign_id).first
    responses = campaign.responses + 1
    campaign.responses = responses
    Campaign.update!
  end


end
