class Images::Preset < ActiveRecord::Base

  has_many :images_types, :class_name => 'Images::Type'
end
