class Images::Type < ActiveRecord::Base

  has_many :images_imgfiles, :class_name => 'Images::Imgfile'

  belongs_to :images_preset, :class_name => 'Images::Preset'
end
