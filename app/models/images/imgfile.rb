class Images::Imgfile < ActiveRecord::Base

  belongs_to :images_type, :class_name => 'Images::Type'
  # TODO reenable paths
  # For Paperclip
  # has_attached_file :attachment, :path => ":sponsoree_id/:image_category_id/:style/:filename", :url => ":s3_domain_url", :styles => lambda { |img| { :preview => (img.instance.preview_size), :display => (img.instance.display_size) } }
  has_attached_file :attachment, :path => "test/:style/:filename", :url => ":s3_domain_url", :styles => lambda { |img| { :preview => (img.instance.preview_size), :display => (img.instance.display_size) } }
  validates_attachment_content_type :attachment, :content_type => /\Aimage\/.*\Z/

  # compute styles for image sizes
  # These vary based upon the ad selected.
  def preview_size
    # Load presets from database
    get_image_presets
    "#{@image_presets[:preview_width]}x#{@image_presets[:preview_height]}#"
  end

  def display_size
    "#{@image_presets[:display_width]}x#{@image_presets[:display_height]}#"
  end

  private

  def get_image_presets
    images_type = ImagePresets::GetImageTypeById.call(self.images_type_id)
    @image_presets = ImagePresets::GetImagePresetValuesById.call(images_type.images_preset_id)
  end

  def set_image_category_id
    @images_type_id = self.images_type
  end

end
