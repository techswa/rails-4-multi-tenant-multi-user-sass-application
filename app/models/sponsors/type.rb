class Sponsors::Type < ActiveRecord::Base

  has_many :advertisements_positions, :class_name => 'Advertisements::Position'
  has_many :advertisements_groups, :class_name => 'Advertisements::Group'
  has_many :sponsors
end
