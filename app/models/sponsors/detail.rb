class Sponsors::Detail < ActiveRecord::Base

  belongs_to :sponsor, foreign_key: "sponsor_id"

  belongs_to :sponsors_style, :class_name => 'Sponsors::Style'

  has_many :headshots
  has_many :images

  #Return the vendor's name of this detail record
  def sponsors_name
     sponsors_record = Sponsorships::Business.where(sponsor_id: self.sponsor_id).first
     sponsors_record.vendor.business_name
  end

end
