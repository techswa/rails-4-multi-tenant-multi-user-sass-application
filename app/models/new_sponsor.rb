# Used by the new sponsor form
class NewSponsor
  include ActiveModel::Model

  # Campaign methods
  attr_accessor :campaign_id

  # User methods
  attr_accessor :email, :last_name, :first_name, :password, :password_confirmation

  # Sponsor methods
  attr_accessor :business_name, :phone, :address_1, :address_2, :city, :state_id, :zipcode
  attr_accessor :website, :sponsor_email




end