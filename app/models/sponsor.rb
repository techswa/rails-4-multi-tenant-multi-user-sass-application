class Sponsor < ActiveRecord::Base
  # Currently used model as of 01/06/15

  belongs_to :level
  belongs_to :sponsors_type, :class_name => 'Sponsors::Type'
  belongs_to :sponsors_level, :class_name => 'Sponsors::Level'
  belongs_to :lookups_state, :class_name => 'Lookups::State'

  has_one :sponsors_details, :class_name => 'Sponsors::Detail' #, foreign_key: "sponsor_id"

  has_many :advertisements_advertisements, :class_name => 'Advertisements::Advertisement'
  has_many :advertisements_templates, :class_name => 'Advertisements::Template'
  has_many :advertisements_rotations, :class_name => 'Advertisements::Rotation'
  has_many :marketing_campaigns, :class_name => 'Marketing::Campaign'
  has_many :marketing_channels, :class_name => 'Marketing::Channel'
  has_many :sponsorships_businesses, :class_name => 'Sponsorships::Businesses', foreign_key: "sponsor_id"
  has_many :sponsorships_businesses, :class_name => 'Sponsorships::Businesses', foreign_key: "vendor_id"

  # , :class_name => 'Sponsorships::Businesses'
  # has_many :sponsorships_businesses, :class_name => 'Sponsorships::Businesses', foreign_key: "sponsored"

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  #  Added for ember devise
  before_save :ensure_authentication_token

  attr_accessor :new_sponsor_sign_up # Used to skip full validations on sign_up form
  # validates:
  validates :user_name, presence: true, length: { within: 6..20 }
  validates :email, presence:true
  validates :license_number, presence: true, unless: :new_sponsor_sign_up
  validates :license_email, presence: true, unless: :new_sponsor_sign_up
  validates :business_name, presence: true, unless: :new_sponsor_sign_up
  validates :first_name, presence: true, unless: :new_sponsor_sign_up
  validates :last_name, presence: true, unless: :new_sponsor_sign_up
  validates :primary_phone, presence: true, unless: :new_sponsor_sign_up
  validates :address_1, presence: true, unless: :new_sponsor_sign_up
  # validates :address_2
  validates :city, presence: true, unless: :new_sponsor_sign_up
  # lookups_state_id uuid,
  validates :zipcode, presence: true, unless: :new_sponsor_sign_up
  validates :website, presence: true, unless: :new_sponsor_sign_up


  # Create sort order / used in templates ctrl
  def order_by_business_name
    order(self.business_name)
  end

  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end


  # Used in staff portal
  def self.latest_visitors(limit)
    where("last_sign_in_at IS NOT NULL").order('last_sign_in_at DESC').limit(limit)
  end



  #For Devise to use user_name
  def email_required?
    false
  end

  private
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless Sponsor.where(authentication_token: token).first
    end
  end

end
