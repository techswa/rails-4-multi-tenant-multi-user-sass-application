class Advertisements::Group < ActiveRecord::Base

  belongs_to :sponsors_type, :class_name => 'Sponsors::Type'

  has_many :advertisements_templates, :class_name => 'Advertisements::Template'
end
