class Advertisements::Template < ActiveRecord::Base

  belongs_to :sponsor
  belongs_to :advertisements_mechanical, :class_name => 'Advertisements::Mechanical'
  belongs_to :advertisements_group, :class_name => 'Advertisements::Group'
  belongs_to :sponsors_franchise, :class_name => 'Sponsors::Franchise'


  has_many :advertisements_advertisements, :class_name => 'Advertisements::Advertisement'


end
