class Advertisements::Advertisement < ActiveRecord::Base

  belongs_to :sponsor
  belongs_to :advertisements_mechanical, :class_name => 'Advertisements::Mechanical'
  belongs_to :advertisements_template, :class_name => 'Advertisements::Template'

  has_many :advertisements_rotations, :class_name => 'Advertisements::Rotation'
  has_many :marketing_campaigns, :class_name => 'Marketing::Campaign'


  # def self.find_active_by_position()
  #
  # end

end
