class Advertisements::Position < ActiveRecord::Base

  belongs_to :sponsors_type, :class_name => 'Sponsors::Type'

  has_many :advertisements_advertisements, :class_name => 'Advertisements::Advertisement'
  has_many :advertisements_templates, :class_name => 'Advertisements::Template'
  has_many :advertisements_rotations, :class_name => 'Advertisements::Rotation'

end
