class Advertisements::Rotation < ActiveRecord::Base

  belongs_to :advertisements_mechanical, :class_name => 'Advertisements::Mechanical'
  belongs_to :advertisements_advertisement, :class_name => 'Advertisements::Advertisement'
  belongs_to :marketing_campaign, :class_name => 'Marketing::Campaign'
  belongs_to :sponsor

  has_many :marketing_campaigns, :class_name => 'Marketing::Campaigns'
end
