class Advertisementorg < ActiveRecord::Base

  belongs_to :ad_position
  belongs_to :ad_source_type
  belongs_to :sponsor
  belongs_to :system_ad
  belongs_to :sponsor_ad

  has_many :ad_rotations


end
