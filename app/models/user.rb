class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable



  belongs_to :campaign

  has_many :sponsors_sponsors, :class_name => 'Sponsors::Sponsor'


  # Works with Devise in signup form
  # accepts_nested_attributes_for :sponsors

  # Scopes
  def self.consumers
    where(role: 'user')
  end

  def user_full_name
    "#{last_name}, #{first_name}"
  end

end
