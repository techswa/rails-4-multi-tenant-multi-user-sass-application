class Sponsorships::Vendor < ActiveRecord::Base


  belongs_to :vendor, class_name: 'Sponsor'
  belongs_to :sponsor, class_name: 'Sponsor'
  belongs_to :marketing_campaign, class_name: 'Marketing::Campaign'

end
