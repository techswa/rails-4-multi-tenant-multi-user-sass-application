class Sponsorships::Business < ActiveRecord::Base

  belongs_to :vendor, class_name: 'Sponsor', foreign_key: 'vendor_id'
  belongs_to :sponsor, class_name: 'Sponsor', foreign_key: 'sponsor_id'
  belongs_to :marketing_campaign, class_name: 'Marketing::Campaign'

end
