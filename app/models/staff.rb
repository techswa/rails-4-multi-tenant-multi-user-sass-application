class Staff < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Used by devise for validations
  validates_presence_of :user_name
  validates_uniqueness_of :user_name



  #For Devise to use user_name
  def email_required?
    false
  end
end
