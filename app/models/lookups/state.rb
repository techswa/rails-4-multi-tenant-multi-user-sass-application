class Lookups::State < ActiveRecord::Base

  has_many :lookups_exchanges, :class_name => 'Lookups::Exchange'
  has_many :sponsors
end
