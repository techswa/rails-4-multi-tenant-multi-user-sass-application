module ImagePresets

  # Get image presets for use in a Model calling Paperclip

  class GetImageTypeById
    # extend ImageProcessing
    include Service



    def self.call images_type_id
      # Takes symbol name and returns preview and display
      # can be extended in the future to return additional image sizes if needed

       # Retrieve image category record
      Images::Type.find(images_type_id)

    end


    private
    #
  end
end