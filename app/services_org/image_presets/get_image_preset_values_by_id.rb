module ImagePresets

  # Get image presets for use in a Model calling Paperclip

  class GetImagePresetValuesById
    # extend ImageProcessing
    include Service



    def self.call images_preset_id
      # Takes symbol name and returns preview and display
      # can be extended in the future to return additional image sizes if needed

      # Retrieve preset record
      image_presets_record = Images::Preset.find(images_preset_id)

      # load record into hash and return
      image_presets = {preview_width: image_presets_record.preview_width,
                       preview_height: image_presets_record.preview_height,
                       display_width: image_presets_record.display_width,
                       display_height: image_presets_record.display_height}

    end


    private
    # def self.get_sponsor_details
    #   # 'These are sponsor details'
    # end

  end
end