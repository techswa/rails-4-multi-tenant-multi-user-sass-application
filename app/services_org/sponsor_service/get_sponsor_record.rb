module SponsorService

  # Get the sponsor record of the current user
  class GetSponsorRecord
    include Service

    # call with current-user and return the user's company's sponsor record
    def self.call (current_user)

      Sponsors::Sponsor.where(user_id: current_user.id).first

    end

    private

  end

end