module SponsorService
#TODO Is this being used?
  # Get the all of the business this company sponsors
  class GetSponsoredRecords
    include Service

    def self.call (sponsoree, view_context)
      # Get raw records
      bsponsorships = Bsponsorship.where(sponsor_id: sponsoree.id)

      # Decorate records
      bsponsorships_decorated = bsponsorships.collect { |bsponsorship| BsponsorshipDecorator.new(bsponsorship,view_context) }

      # Return
      bsponsorships_decorated
    end

    private

  end

end