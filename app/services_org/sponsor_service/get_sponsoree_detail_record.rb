module SponsorService
#TODO Is this being used?
  # Get the sponsor of the business
  class GetSponsoreeDetailRecord
    include Service

    # call with current-user and return the user's company's sponsor record
    def self.call (current_user)

      # Get sponsoree record for the current user
      sponsoree = SponsorService::GetSponsoreeRecord.call(current_user)

      # get actual detail record
      SponsorDetail.where(sponsor_id: sponsoree.id).first

    end

    private

  end

end