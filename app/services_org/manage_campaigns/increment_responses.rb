module ManageCampaigns
  #TODO Is this being used?
  # This adds one to the response count every time someone arrives from a specific campaign
  class IncrementResponses
    include Service

    def self.call campaign_id
        campaign = Campaign.find(campaign_id)
        responses = campaign.responses + 1
        campaign.responses = responses
        campaign.save!
      new_count = campaign = Campaign.find(campaign_id).responses
    end


    private
  end

end