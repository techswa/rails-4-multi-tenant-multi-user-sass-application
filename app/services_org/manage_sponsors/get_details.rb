module ManageSponsors

  #TODO Is this being used?
  # This takes the sponsor_id and returns the Sponsor's detail record
  class GetDetails
    include Service

    #Return sponsor_id
    #THis @raise used to build system ads
    def self.call sponsor_id
      puts "ManageSponsors:GetDetails #{sponsor_id} "
      sponsor = SponsorDetail.where(sponsor_id: sponsor_id).first
      puts "Returned record: #{sponsor.inspect}"
      sponsor
    end


    private
  end

end