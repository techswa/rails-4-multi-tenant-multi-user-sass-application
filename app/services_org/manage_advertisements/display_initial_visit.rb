module ManageAdvertisements
#TODO Is this being used?
  # Creates or gets the advertisement that will be displayed
  # The ad can be created from the selected system template
  # utilizing the SponsorDetails information of the Sponsor
  # or the Sponsors custom graphic file ad can be used.

  class DisplayInitialVisit
    include Service



    def self.call sponsor_details
      details = sponsor_details.inspect
      puts "Passed sponsor_details #{details}"
      "<p><b>This</b> is the ad content<br/> so far #{details}"
    end


    private
    # def self.get_sponsor_details
    #   # 'These are sponsor details'
    # end

  end
end