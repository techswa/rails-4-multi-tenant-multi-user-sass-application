module RoleService
#TODO Is this being used?
  # Takes current_user and returns role_symbol
  class GetUserRole
    include Service

    # gets current user role_sym

    def self.call current_user
      current_user.role.to_sym
    end


    private
  end

end