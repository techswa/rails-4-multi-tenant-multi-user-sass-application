module RoleService
#TODO Is this being used?
  # Takes current_user and returns role_symbol
  class GetUserRole
    include Service

    # gets current user role

    def self.call current_user, role_sym
      user = User.find(current_user.id)
      user.role = role_sym.to_s
      user.save
    end


    private
  end

end