// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require jquery
//= require jquery_ujs
//= require foundation
// Remove Turbolinks for the sake of Ember
// require turbolinks
//= require_tree .

$(function(){ $(document).foundation(); });


// From http://www.dextablogs.com/blog/2014/05/25/handling-ajax-redirects-in-rails/

//$.ajaxSetup({
//    statusCode: {
//        302: function (response) {
//            var redirect_url = response.getResponseHeader('X-Ajax-Redirect-Url');
//            if (redirect_url != undefined) {
//                window.location.pathname = redirect_url;
//            }
//        }
//    }
//});