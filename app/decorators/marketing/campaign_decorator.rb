class Marketing::CampaignDecorator < Decorator

  def campaign
    @component
  end

  def d_marketing_channel_id
    self.marketing_channel_id
  end

  def d_marketing_channel_title
    self.marketing_channel.title
  end

  def d_sponsor_id
    self.sponsor_id
  end

  def d_sponsor_business_name
     self.sponsor.business_name
  end

  def d_advertisements_rotation_title
    self.advertisements_rotation.title
  end

  def d_title
      self.title
  end

  def d_description
    self.description
  end

  def d_start_date
    self.start_date
  end

  def d_end_date
    self.end_date
  end

  def d_sent
    self.sent
  end

  def d_responses
    self.responses
  end

  def d_signups
    self.signups
  end

  def d_notes
    self.notes
  end

  def d_archived
    self.archived
  end

end
