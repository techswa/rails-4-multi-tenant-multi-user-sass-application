class Marketing::ChannelDecorator < Decorator

  def channel
    @component
  end

  def d_marketing_channel_id
    self.marketing_channel_id
  end

  def d_sponsor_id
     self.sponsor_id
  end

  def d_sponsor_business_name
    self.sponsor.business_name
  end

  def d_title
      self.title
  end

  def d_description
    self.description
  end

  def d_archived
    self.archived
  end

end
