class Images::ImgfileDecorator < Decorator
  #include Modules::StringFormatting

  def imgfile
    @component
  end

  def d_title
    self.title
  end

  def d_description
    self.description
  end

  def d_sponsorships_sponsor_id
    # self.symbol_name
  end


  def d_images_type_id
    self.images_type_id
  end

  def d_images_type_title
    self.images_type.title
  end

  def d_attachment_file_name
    self.attachment_file_name
  end

  def d_attachment_content_type
    self.attachment_content_type
  end

  def d_attachment_file_size
    self.attachment_file_size
  end

  def d_attachment_updated_at
    self.attachment_updated_at
  end

  def d_archived
    self.archived
  end

end