class Images::PresetDecorator < Decorator
  #include Modules::StringFormatting

  def image_preset
    @component
  end

  def d_title
    self.title
  end

  def d_description
    self.description
  end

  def d_symbol_name
    self.symbol_name
  end

  def d_preview_width
    self.preview_width
  end

  def d_preview_height
    self.preview_height
  end

  def d_display_width
    self.display_width
  end

  def d_display_height
    self.display_height
  end

  def d_archived
    self.archived
  end

end