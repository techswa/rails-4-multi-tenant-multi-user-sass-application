class Images::TypeDecorator < Decorator
  #include Modules::StringFormatting

  def image_preset
    @component
  end

  def d_title
    self.title
  end

  def d_description
    self.description
  end

  def d_symbol_name
    self.symbol_name
  end

  def d_images_preset_id
    self.images_preset_id
  end

  def d_images_preset_title
    self.images_preset.title
  end

  def d_archived
    self.archived
  end

end