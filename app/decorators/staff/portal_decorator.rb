class Staff::PortalDecorator < Decorator
  #include Modules::StringFormatting

  def portal
    @component
  end

  def d_user_name
    self.user_name
  end

  def d_email
    self.email
  end

  def d_campaign_id
    self.campaign_id
  end

  def d_campaign_sponsor_business_name
     self.d_campaign_id
     # .sponsor.business_name
  end

  def d_lookups_industry_id
    self.lookups_industry_id
  end

  def d_sponsors_type_id
    self.sponsors_type_id
  end

  def d_license_number
    self.license_number
  end

  def d_license_email
    self.license_email
  end

  def d_business_name
    self.business_name
  end

  def d_first_name
    self.first_name
  end

  def d_last_name
    self.last_name
  end

  def d_primary_phone
    self.primary_phone
  end

  def d_address_1
    self.address_1
  end

  def d_address_2
    self.address_2
  end

  def d_city
    self.city
  end

  def d_user_full_name
    "#{self.last_name}, #{self.first_name}"
  end

  def d_lookups_state_id
    self.lookups_state_id
  end

  def d_state_abbreviated_name
    "#{self.lookups_state.abbreviated_name}"
  end

  def d_zipcode
    self.zipcode
  end

  def d_website
    self.website
  end

  def d_notes
    self.notes
  end



  #agent_id

  # email

  #brokerage_name

  #City_State

  # def account
  #   @component
  # end
  #
  # def d_company
  #   self.name
  # end
  #
  # def d_contact
  #
  # end
  #
  # def d_anniversary
  #   self.anniversary.strftime("%m/%d/%Y")
  # end
  #
  # def d_number
  #   self.number
  # end
  #
  # def d_account_users
  #   # h.link_to translate('account.str.list_users'), h.users_path(client.account.users)
  #   h.link_to 'List users', h.clients_path(account_id: id)
  # end
  #
  # def d_contact
  #   #'d_l_contact'
  #   # if account.users.blank?
  #   # # h.link_to 'Select a contact', h.users_path(account_id: account.id)
  #   # else
  #   #     h.link_to "#{account.users[0].first_name} #{account.users[0].last_name}", h.manage_user_path(account.users[0].id)
  #   # end
  #   "#{account.users[0].inspect}"
  # end
  # def d_l_contact
  #   #'d_l_contact'
  #   # if account.users.blank?
  #   # # h.link_to 'Select a contact', h.users_path(account_id: account.id)
  #   # else
  #   #     h.link_to "#{account.users[0].first_name} #{account.users[0].last_name}", h.manage_user_path(account.users[0].id)
  #   # end
  #   #"#{account.user_first_name} #{account.user_last_name}"
  # end
  #
  # def d_state_name
  #   account.state_name
  # end
  #
  # def d_state_abbreviation
  #   self.state.abbreviation
  # end
  #
  # def d_standing_name
  #   account.standing_name
  # end
  #
  # def d_phone_office_formatted
  #   #"#{account.phone_office} #{account.phone_office_ext}"
  #   format_phone_number_with_ext account.phone_office, account.phone_office_ext
  # end
  #
  # def d_phone_cell_formatted
  #   format_phone_number account.phone_cell
  # end
  #
  # def d_l_email
  #   if account.users[0].blank?
  #     ''
  #   else
  #     h.link_to account.users[0].email, "mailto:#{account.users[0].email}"
  #     #format_email_link account.user.email
  #   end
  # end

end