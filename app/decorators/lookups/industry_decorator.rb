class Lookups::IndustryDecorator < Decorator

  def industry
    @component
  end

  def d_naics_code
    self.naics_code
  end

  def d_title
    self.title
  end

  def d_note
      self.note
  end



end