class Lookups::CountyDecorator < Decorator

  def county
    @component
  end

  def d_full_name
    self.full_name
  end

  def d_fips_county
    self.fips_county
  end

  def d_fips_state
      self.fips_state
  end



end