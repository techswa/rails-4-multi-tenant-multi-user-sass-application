class Lookups::StateDecorator < Decorator

  def state
    @component
  end

  def d_fips_state
      self.fips_state
  end

  def d_full_name
    self.full_name
  end

  def d_abbreviated_name
    self.abbreviated_name
  end

end