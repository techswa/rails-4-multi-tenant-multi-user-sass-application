class Lookups::ExchangeDecorator < Decorator

  def exchange
    @component
  end

  def d_full_name
      self.full_name
  end

  def d_abbreviated_name
    self.abbreviated_name
  end

  def d_city
    self.city
  end

  def d_lookups_state_id
    self.lookups_state_id
  end

  def d_lookups_state_abbreviation
    self.lookups_state.abbreviated_name
  end
end