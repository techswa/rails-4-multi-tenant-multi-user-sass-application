class Sponsorships::VendorDecorator < Decorator
  #include Modules::StringFormatting

  def vendor
    @component
  end

  def d_vendor_business_name
     self.vendor.inspect
  end

  def d_sponsor_business_name
    #  self.sponsored_id
    self.sponsor.business_name
  end

  def d_marketing_campaign_title
    self.marketing_campaign.title
  end

  def d_start_date
    self.start_date
  end

  def d_end_date
    self.end_date
  end

  def d_active
    self.active
  end

end
