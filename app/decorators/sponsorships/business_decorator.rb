class Sponsorships::BusinessDecorator < Decorator
  #include Modules::StringFormatting

  def business
    @component
  end

  def d_vendor_business_name
     self.vendor.business_name
  end

  def d_sponsor_business_name
    self.sponsor.business_name
  end

  def d_sponsor_sign_in_count
    self.sponsor.sign_in_count
  end

  def d_marketing_campaign_title
    self.marketing_campaign.title
  end

  def d_start_date
    self.start_date
  end

  def d_end_date
    self.end_date
  end

  def d_active
    self.active
  end

end
