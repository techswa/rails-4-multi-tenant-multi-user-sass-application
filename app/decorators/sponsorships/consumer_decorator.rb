class Sponsorships::ConsumerDecorator < Decorator
  #include Modules::StringFormatting

  def consumer
    @component
  end

  def d_user_id
    self.user_id
  end

  def d_sponsors_sponsor_id
    self.sponsors_sponsor_id
  end

  def d_user_id
    self.user_id
  end

  def d_marketing_campaign_id
    self.marketing_campaign_id
  end

  def d_session_id
    self.session_id
  end

  def d_statistics_device_id
    self.statistics_device_id
  end

end