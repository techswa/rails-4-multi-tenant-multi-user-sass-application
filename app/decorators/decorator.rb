require 'delegate'
require 'i18n'

class Decorator < BasicObject

  #include Modules::StringFormatting


  undef_method :==

  def initialize(component, view_context)
    @component = component
    @view_context = view_context
  end

  def method_missing(name, *args, &block)
    @component.send(name, *args, &block)
  end

  def h
    @view_context
  end

  def blank_to_nbsp(value)
    value.blank? ? '&nbsp;'.html_safe : value
  end

  def blank_to_dashes(value)
    value.blank? ? '-----'.html_safe : value
  end

end