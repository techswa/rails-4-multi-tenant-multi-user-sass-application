class Accounting::StatusDecorator < Decorator
  #include Modules::StringFormatting

  def status
    @component
  end

  def d_sponsors_sponsor_id
    self.sponsors_sponsor_id
  end

  def d_sponsors_sponsor_business_name
    self.sponsors_sponsor.business_name
  end

  def d_suspended
    self.suspended
  end

  def d_grace
    self.grace
  end
  def d_start_date
    self.start.strftime("%m/%d")
  end

  def d_stop_date
    self.stop.strftime("%m/%d")
  end

  def d_ad_type_title
    self.ad_type.title
  end
end