class Accounting::PaymentTypeDecorator < Decorator
  #include Modules::StringFormatting

  def payment_type
    @component
  end

  def d_name
    self.name
  end

  def d_sort
    self.sort
  end

end