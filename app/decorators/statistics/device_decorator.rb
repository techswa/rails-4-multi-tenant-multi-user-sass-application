class Statistics::DeviceDecorator < Decorator
  #include Modules::StringFormatting

  def device
    @component
  end

  def d_note
    self.note
  end

end