class Advertisements::GroupDecorator < Decorator

  def position
    @component
  end

  def d_title
    self.title
  end

  def d_description
    self.description
  end

  def d_sponsor_type_title
    self.sponsors_type.title
  end

  def d_archived
    self.archived
  end

end
