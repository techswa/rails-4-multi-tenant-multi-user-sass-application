class Advertisements::RotationDecorator < Decorator


  def rotation
    @component
  end

  def d_sponsors_sponsor_id
    self.sponsors_sponsor_id
  end

  def d_marketing_campaign_title
     self.marketing_campaign.title
  end

  def d_advertisements_mechanical_title
    self.advertisements_mechanical.title
  end

  def d_sponsor_business_name
     self.sponsor.business_name
  end

  def d_advertisements_advertisement_id
    self.advertisements_advertisement_id
  end

  def d_advertisements_advertisement_title
    self.advertisements_advertisement.title
  end

  def d_running
    self.running
  end

  def d_start_date
    self.start_date
  end

  def d_stop_date
    self.stop_date
  end

  def d_notes
    self.notes
  end

  def d_archived
    self.archived
  end

end
