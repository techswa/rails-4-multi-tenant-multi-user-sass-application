class Advertisements::MechanicalDecorator < Decorator

  def position
    @component
  end

  def d_title
    self.title
  end

  def d_description
    self.description
  end

  def d_sponsors_type_id
    # self.sponsors_type.title
  end

  def d_display_width
    self.display_width
  end

  def d_display_height
    self.display_height
  end

  def d_symbol_name
    self.symbol_name
  end

  def d_archived
    self.archived
  end

end
