class Advertisements::TemplateDecorator < Decorator

  def template
    @component
  end

  def d_title
    self.title
  end

  def d_description
    self.description
  end

  def d_franchise_title
    self.sponsors_franchise.title
  end

  def d_sponsor_business_name
    self.sponsor.business_name
  end

  def d_advertisements_mechanical_title
    self.advertisements_mechanical.title
  end

  def d_advertisements_group_title
    self.advertisements_group.title
  end

  def d_code
    self.code
  end

  def d_archived
    self.archived
  end

end
