class Advertisements::AdvertisementDecorator < Decorator

  def advertisement
    @component
  end

  def d_title
    self.title
  end

  def d_sponsor_id
    self.sponsor_id
  end

  def d_sponsor_business_name
    self.sponsor.business_name
  end

  def d_description
    self.description
  end

  def d_revision
    self.revision
  end

  def d_advertisements_source_id
      self.advertisements_source_id
  end

  def d_advertisements_source_title
    self.advertisements_source.title
  end

  def d_advertisements_position_id
    self.advertisements_position_id
  end

  def d_advertisements_template_id
    self.advertisements_template_id
  end

  def d_advertisements_template_title
    self.advertisements_template.title
  end

  def d_advertisements_template_title_link
    h.link_to self.d_advertisements_template_title, h.edit_advertisements_template_path(self.advertisements_template_id)
  end


  def d_images_imgfile_id
    self.images_imgfile_id
  end

  def d_images_imgfile
    self.images_imgfile
  end

  def d_archived
    self.archived
  end

end
