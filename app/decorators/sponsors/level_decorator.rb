class Sponsors::LevelDecorator < Decorator
  #include Modules::StringFormatting

  def level_class
    @component
  end

  def d_title
  self.title
  end

  def d_description
     self.description
  end

  def d_archived
    self.archived
  end


end
