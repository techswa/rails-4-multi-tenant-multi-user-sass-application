class Sponsors::ClassDecorator < Decorator
  #include Modules::StringFormatting

  def sponsor_class
    @component
  end

  def d_name
  self.name
  end

  def d_description
     self.description
  end

  def d_archived
    self.archived
  end


end
