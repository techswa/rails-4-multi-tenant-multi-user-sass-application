class Sponsors::DetailDecorator < Decorator
  #include Modules::StringFormatting

  def detail
    @component
  end

  def d_sponsors_name
    #Sponsor of this company
    self.sponsors_name
  end
  def d_sponsor_business_name
    #self.sponsor.inspect #business_name

  end

  def d_company_name
    self.company_name
  end

  def d_contact_last_name
    self.contact_last_name
  end

  def d_contact_first_name
    self.contact_first_name
  end

  def d_phone_1
    self.phone_1
  end

  def d_phone_2
    self.phone_2
  end

  def d_website
    self.website
  end

  def d_email
    self.email
  end

  def d_tag_line_1
    self.tag_line_1
  end

  def d_tag_line_2
    self.tag_line_2
  end

  def d_welcome_greeting
    self.welcome_greeting
  end

  def d_return_greeting
    self.return_greeting
  end

  def d_logo_filename
    self.logo_filename
  end

  def d_headshot_filename
    self. headshot_filename
  end

  def d_style_title
    self.sponsors_style.title
  end

end
