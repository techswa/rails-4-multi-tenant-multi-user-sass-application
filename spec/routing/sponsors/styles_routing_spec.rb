require "rails_helper"

RSpec.describe Sponsors::StylesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/sponsors/styles").to route_to("sponsors/styles#index")
    end

    it "routes to #new" do
      expect(:get => "/sponsors/styles/new").to route_to("sponsors/styles#new")
    end

    it "routes to #show" do
      expect(:get => "/sponsors/styles/1").to route_to("sponsors/styles#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/sponsors/styles/1/edit").to route_to("sponsors/styles#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/sponsors/styles").to route_to("sponsors/styles#create")
    end

    it "routes to #update" do
      expect(:put => "/sponsors/styles/1").to route_to("sponsors/styles#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/sponsors/styles/1").to route_to("sponsors/styles#destroy", :id => "1")
    end

  end
end
