require "rails_helper"

RSpec.describe Sponsors::LevelsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/sponsors/levels").to route_to("sponsors/levels#index")
    end

    it "routes to #new" do
      expect(:get => "/sponsors/levels/new").to route_to("sponsors/levels#new")
    end

    it "routes to #show" do
      expect(:get => "/sponsors/levels/1").to route_to("sponsors/levels#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/sponsors/levels/1/edit").to route_to("sponsors/levels#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/sponsors/levels").to route_to("sponsors/levels#create")
    end

    it "routes to #update" do
      expect(:put => "/sponsors/levels/1").to route_to("sponsors/levels#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/sponsors/levels/1").to route_to("sponsors/levels#destroy", :id => "1")
    end

  end
end
