require "rails_helper"

RSpec.describe Sponsors::FranchisesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/sponsors/franchises").to route_to("sponsors/franchises#index")
    end

    it "routes to #new" do
      expect(:get => "/sponsors/franchises/new").to route_to("sponsors/franchises#new")
    end

    it "routes to #show" do
      expect(:get => "/sponsors/franchises/1").to route_to("sponsors/franchises#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/sponsors/franchises/1/edit").to route_to("sponsors/franchises#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/sponsors/franchises").to route_to("sponsors/franchises#create")
    end

    it "routes to #update" do
      expect(:put => "/sponsors/franchises/1").to route_to("sponsors/franchises#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/sponsors/franchises/1").to route_to("sponsors/franchises#destroy", :id => "1")
    end

  end
end
