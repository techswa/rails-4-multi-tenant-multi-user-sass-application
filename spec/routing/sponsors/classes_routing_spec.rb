require "rails_helper"

RSpec.describe Sponsors::ClassesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/sponsors/classes").to route_to("sponsors/classes#index")
    end

    it "routes to #new" do
      expect(:get => "/sponsors/classes/new").to route_to("sponsors/classes#new")
    end

    it "routes to #show" do
      expect(:get => "/sponsors/classes/1").to route_to("sponsors/classes#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/sponsors/classes/1/edit").to route_to("sponsors/classes#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/sponsors/classes").to route_to("sponsors/classes#create")
    end

    it "routes to #update" do
      expect(:put => "/sponsors/classes/1").to route_to("sponsors/classes#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/sponsors/classes/1").to route_to("sponsors/classes#destroy", :id => "1")
    end

  end
end
