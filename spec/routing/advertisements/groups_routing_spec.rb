require "rails_helper"

RSpec.describe Advertisements::GroupsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/advertisements/groups").to route_to("advertisements/groups#index")
    end

    it "routes to #new" do
      expect(:get => "/advertisements/groups/new").to route_to("advertisements/groups#new")
    end

    it "routes to #show" do
      expect(:get => "/advertisements/groups/1").to route_to("advertisements/groups#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/advertisements/groups/1/edit").to route_to("advertisements/groups#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/advertisements/groups").to route_to("advertisements/groups#create")
    end

    it "routes to #update" do
      expect(:put => "/advertisements/groups/1").to route_to("advertisements/groups#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/advertisements/groups/1").to route_to("advertisements/groups#destroy", :id => "1")
    end

  end
end
