require "rails_helper"

RSpec.describe Sponsorships::VendorsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/sponsorships/vendors").to route_to("sponsorships/vendors#index")
    end

    it "routes to #new" do
      expect(:get => "/sponsorships/vendors/new").to route_to("sponsorships/vendors#new")
    end

    it "routes to #show" do
      expect(:get => "/sponsorships/vendors/1").to route_to("sponsorships/vendors#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/sponsorships/vendors/1/edit").to route_to("sponsorships/vendors#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/sponsorships/vendors").to route_to("sponsorships/vendors#create")
    end

    it "routes to #update" do
      expect(:put => "/sponsorships/vendors/1").to route_to("sponsorships/vendors#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/sponsorships/vendors/1").to route_to("sponsorships/vendors#destroy", :id => "1")
    end

  end
end
