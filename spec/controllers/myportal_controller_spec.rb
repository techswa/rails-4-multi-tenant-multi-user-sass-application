require 'rails_helper'

RSpec.describe MyportalController, :type => :controller do

  describe "GET index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET details" do
    it "returns http success" do
      get :details
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET visitors" do
    it "returns http success" do
      get :visitors
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET advertisements" do
    it "returns http success" do
      get :advertisements
      expect(response).to have_http_status(:success)
    end
  end

end
