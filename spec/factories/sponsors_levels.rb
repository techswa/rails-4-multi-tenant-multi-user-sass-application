# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sponsors_level, :class => 'Sponsors::Level' do
    name "MyString"
    description "MyText"
    archived false
  end
end
