FactoryGirl.define do
  factory :advertisements_position, :class => 'Advertisements::Position' do
    title "Advertisement title"
    description "Advertisement description"
    sponsors_type_id "9c75385c-b65c-4398-a209-263cf978693f"
    display_width 760
    display_height 200
    symbol_name ":symbol_name"
    archived false
  end

end
