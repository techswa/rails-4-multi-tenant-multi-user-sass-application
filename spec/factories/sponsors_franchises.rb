# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sponsors_franchise, :class => 'Sponsors::Franchise' do
    title "MyString"
    description "MyText"
    archived false
  end
end
