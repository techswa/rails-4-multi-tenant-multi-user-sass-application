FactoryGirl.define do
  factory :sponsor do
    # association :marketing_campaign, strategy: :build
    user_name "superuser"
    password "password"
    password_confirmation "password"
    marketing_campaign_id ""
    lookups_industry_id ""
    sponsors_type_id ""
    license_number "012456789"
    license_email "license@email.com"
    business_name "Dallas Realty"
    first_name "Robert"
    last_name "Jones"
    primary_phone "214-589-6985"
    address_1 "2135 South Stemmons Frwy"
    address_2 "Suite 324"
    city "Dallas"
    lookups_state_id "" # Association
    zipcode "75287"
    website "www.mywebsite.com"
    notes "Some notes go here."
  end

end
