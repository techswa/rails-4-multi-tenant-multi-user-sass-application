# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sponsors_style, :class => 'Sponsors::Style' do
    title "MyString"
    description "MyText"
    archived false
  end
end
