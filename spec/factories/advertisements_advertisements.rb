FactoryGirl.define do
  factory :advertisements_advertisement, :class => 'Advertisements::Advertisement' do
    association :sponsor, strategy: :build
    title "MyString"
    description "MyText"
    revision 1.5
    symbol_name "MyString"
    association :advertisements_source, strategy: :build
    association :advertisements_position, strategy: :build
    association :advertisements_template, strategy: :build
    images_imgfile_id ""
    archived false
  end

end
