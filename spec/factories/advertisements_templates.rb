FactoryGirl.define do
  factory :advertisements_template, :class => 'Advertisements::Template' do
    title "Template title"
    description "Template description"
    sponsor_id "46eb4e3b-7a4d-4ed8-aa53-bfcb7ad55685" #House account
    code "Code"
    archived false
  end

end
