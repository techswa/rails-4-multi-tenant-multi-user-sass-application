FactoryGirl.define do
  factory :lookups_industry, :class => 'Lookups::Industry' do
    naics_code "MyString"
    title "MyString"
    note "MyText"
  end

end
#
# CREATE TABLE lookups_industries
# (
# id uuid NOT NULL DEFAULT uuid_generate_v4(),
# naics_code character varying(255),
# title character varying(255),
# note text,
# created_at timestamp without time zone,
# updated_at timestamp without time zone,
# CONSTRAINT lookups_industries_pkey PRIMARY KEY (id)
# )
# WITH (
# OIDS=FALSE
# );
# ALTER TABLE lookups_industries
# OWNER TO postgres;
