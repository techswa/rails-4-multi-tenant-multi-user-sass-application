FactoryGirl.define do
  factory :lookups_state, :class => 'Lookups::State' do
    fips_state "MyString"
    full_name "MyString"
    abbreviated_name "MyString"
  end

end

# CREATE TABLE lookups_states
# (
# id uuid NOT NULL DEFAULT uuid_generate_v4(),
# fips_state character varying(255),
# full_name character varying(255),
# abbreviated_name character varying(255),
# created_at timestamp without time zone,
# updated_at timestamp without time zone,
# CONSTRAINT lookups_states_pkey PRIMARY KEY (id)
# )
# WITH (
# OIDS=FALSE
# );
# ALTER TABLE lookups_states
# OWNER TO postgres;
