FactoryGirl.define do
  factory :marketing_campaign, :class => 'Marketing::Campaign' do
    sponsor_id "46eb4e3b-7a4d-4ed8-aa53-bfcb7ad55685"
    title "CPT Website"
    description "Derived from the main website"
    start_date "2014-11-26"
    end_date "2014-11-26"
    sent 1
    responses 1
    signups 1
    notes "Any notes regarding the campaign go here"
    archived false
  end

end
