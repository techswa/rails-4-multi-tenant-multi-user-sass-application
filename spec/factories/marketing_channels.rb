FactoryGirl.define do
  factory :marketing_channel, :class => 'Marketing::Channel' do
    sponsor_id "46eb4e3b-7a4d-4ed8-aa53-bfcb7ad55685"
    title "CPT Website"
    description "This lead came from the CPT website"
    archived false
  end

end
