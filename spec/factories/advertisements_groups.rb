# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :advertisements_group, :class => 'Advertisements::Group' do
    title "MyString"
    description "MyText"
    archived false
  end
end
