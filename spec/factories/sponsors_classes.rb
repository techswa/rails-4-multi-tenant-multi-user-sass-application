# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sponsors_class, :class => 'Sponsors::Class' do
    name "MyString"
    description "MyText"
    archived false
  end
end
