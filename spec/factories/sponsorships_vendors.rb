# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sponsorships_vendor, :class => 'Sponsorships::Vendor' do
    vendor_id ""
    sponsor_id ""
    marketing_campaign_id ""
    start_date "2015-01-30"
    end_date "2015-01-30"
    active false
  end
end
