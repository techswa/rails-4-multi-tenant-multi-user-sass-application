FactoryGirl.define do
  factory :advertisements_rotation, :class => 'Advertisements::Rotation' do
    sponsors_sponsor_id ""
    advertisements_advertisement_id "3c566e6c-d404-4a6d-80ad-9eb79c47427d"
    running false
    start_date "2014-11-28 15:39:29"
    stop_date "2014-11-28 15:39:29"
    notes "MyText"
    archived false
  end

end
