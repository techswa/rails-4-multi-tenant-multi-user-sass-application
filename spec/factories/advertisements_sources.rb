FactoryGirl.define do
  factory :advertisements_source, :class => 'Advertisements::Source' do
    title "Source title"
    description "Description"
    symbol_name ":symbol_name"
    archived false
  end

end
