class ActiveRecord::Base
    mattr_accessor :shared_connection
    @@shared_connection = nil

    def self.connection
      @@shared_connectin || retrieve_connection
    end

end
ActiveRecord::Base.shared_connection = ActiveRecord::Base.connection
