# README #

This is a multi-tenant, multi-user SasS application.  It is focused on the real estate industry.  The end product would allow the consumer to map property values across local markets using google maps.  The application also contained a self-serve advertising platform to generate ads.  The GeoEncoding and Map calls will take place on the client side to reduce server traffic.  A decision to move to a headless Rails 4.x JSON server with an Ember SPA client was made.

##Project status:##
It became obvious that the technical requirements at the time exceeded the capabilities of a single programmer using the current technology and multiple server costs would be prohibitive.  Another issue was the EmberJS was barely in Beta, it was common to have breaking changes and poor documentation. The decision was made to ice-box the project and possibly revisit it when Elixir and Elm were viable alternatives for development.

##Notable items:##
* Ported Rails 3.x to 4.x
* Versioned JSON end points
* JSON Serializers
* EmberJS client
* Foundation SASS
* Custom HAML form generators
* View Decorators

##Project details:##
This project was created Rails 3.x using a custom HAML form generators.  It was later revised to use Rails 4.x, JSON, and  EmberJS 

##Project environment:##
* Ruby 1.9.x & Rails 3.2.x 
* Ruby 2.x & Rails 4.x
* Devise
* EmberJS
* PhantomJS
* Node.js
* ActiveModel Serializers
* SimpleForm
* AWS-S3
* Liquid
* Rspec
* Capybara
* Selenium Webdriver
* FactoryGirl
* Capistrano 3
* Foundation SASS