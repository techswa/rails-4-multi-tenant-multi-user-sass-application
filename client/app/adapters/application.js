import DS from 'ember-data';

export default DS.ActiveModelAdapter.extend({
  namespace: 'api/v1',
  host: 'http://localhost:3000'
});

// var ApplicationAdapter = DS.ActiveModelAdapter.extend({
//   namesapce: 'api/v1',
//   host: 'localhost:3000'
// });
//
// export default ApplicationAdapter;
