import DS from 'ember-data';

export default DS.ActiveModelAdapter.extend({
  namespace: 'api/v1/advertisements/',
  host: 'http://localhost:3000'
});
