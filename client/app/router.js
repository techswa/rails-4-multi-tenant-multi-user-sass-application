import Ember from "ember";
import config from "./config/environment";

// http://stackoverflow.com/questions/20521967/emberjs-how-to-load-multiple-models-on-the-same-route
// handling loading multiple models in a route

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {

  console.log("====console.log('Router main')====");
  console.log("Router main");
  console.log("n/a");
  console.log("==================================");
  this.route("sign-in");

//--------------------------
  this.route("advertisements", function() {
    this.route("templates", function(){
      this.route("show", {
        path: ":template_id"
      });
    });
  });

//--------------------------
  this.resource("invitations", {
    path: "invitation"
  }, function() {
    this.route("invitation", {
      path: ":invitation_id"
    });

    console.log("==================================");
    console.log("invitations route");
    console.log("==================================");
  });

//--------------------------
  this.resource("welcome", {
    path: "welcome"
  }, function() {
    this.route("index");
  });

// //--------------------------
//   this.resource("sponsors", function() {
//     this.route("index");
//
//     this.route("show", {
//       path: ":sponsor_id"
//     }, function() {
//       this.resource("lookups_states", function() {});
//     });
//
// //--------------------------
//     this.route("edit", {
//       path: ":sponsor_id/edit"
//     });
//
//     this.route("new");
//   });

//--------------------------
  this.route("taxes", function() {
    this.route("overview");
    this.route("neighborhood");
    this.route("compare");
    this.route("summary");
    this.route("property");
    this.route("cma");
  });

//--------------------------
  this.route("dispute", function() {});

//--------------------------
  this.route("appeal", function() {
    this.route("overview");
    this.route("diy");
    this.route("package");
    this.route("professional");
  });

//--------------------------
  this.route("market", function() {
    this.route("overview");
    this.route("amenities");
    this.route("condition");
    this.route("configuration");
    this.route("location");
    this.route("neighborhood");
    this.route("size");
    this.route("upgrades");
  });

});

export default Router;
