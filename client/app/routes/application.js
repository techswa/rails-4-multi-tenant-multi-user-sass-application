// app/routes/application.js

import Ember from 'ember';
import ApplicationRouteMixin from 'simple-auth/mixins/application-route-mixin';

export default Ember.Route.extend(ApplicationRouteMixin, {

  // model: function(){
  //   var menuItems = [];
  //   menuItems.pushObject(Ember.Object.create({"name":"menu 1"}));
  //   menuItems.pushObject(Ember.Object.create({"name":"menu 2"}));
  //   menuItems.pushObject(Ember.Object.create({"name":"menu 3"}));
  //   console.log('model');
  //   console.log(menuItems);
  //   var xmenu = xmenu;
  //   return xmenu;
  //   // var items = [{name:'item-1'},{name:'item-2'}];
  // },

  // model: function(params) {
  //   return {
  //     //Get sponsor record
  //     // this.store.fetch('sponsors', params.post_id);
  //     // this.store.fetch('sponsors', 'e4d3a7c5-daab-4ff5-abd2-b4071f1f0c89').then
  //
  //     // // uuid of the user's sponsor
  //     // sponsorId: '99502e37-001e-419b-a3ec-b32121e598c7',
  //      campaignId: 'xxxf9991067-7b60-4d98-81ec-0338e44b3c0d',
  //
  //     // Below are test items
  //     currentMenu: "N/A",
  //     title: "Rails is omakase",
  //     body: 'More stuff',
  //     mainMenuItems: [{name: 'Sponsor', route: '/sponsors'}, {name:'Users', route: '/users'}],
  //     sponsorHeaderAdContent: '<div style="color:red">[Sponsor Header Advertisement]</div>',
  //     appSponsor: 'Base advert sponsor value from application route',
  //   };
  // },

  beforeModel: function(){
    // Send action to setup sponsor template
    this.advertisementService.getTemplateId();
  },
  model: function() {
    return this.store.find('sponsor', '99502e37-001e-419b-a3ec-b32121e598c7');
  },
  afterModel: function(){
      // try in the app controller
      // this.send('getSponsorCookieFromClient');
  },


  actions: {
    appRouterTestAction: function(){
        console.log('appRouterTestAction called');
    },

    willTransition: function(transition) {
      console.log('willTranslition Called');
      // do your stuff
    },

    getSponsorCookieFromClient: function() {
      console.log('getting sponsor cookie');
      // this.set('model.sponsorId', 'junk');
    },

    save: function() {
      console.log('+---- save action bubbled up to application route');
      return true;
    },
    cancel: function() {
        console.log('+---- cancel action bubbled up to application route');
        return true;
    },
    testFunction: function(){
      console.log('called application::testFunction');
      return false;
      //this.model.set('sponsorId', 'new sponsor id# 123');
    },

    // Manage cookies
    // cookieOperation can be [set, get]
    manageCookies: function(cookieName, cookieValue, cookieOperation){
      // this.controllerFor('application').set('campaignId', 'test 123');

      // call function in child controller
      //App.Invitations.invitationController.zookies();
      console.log("call - this.controllerFor('invitations.invitation').send('zookies')");
      this.controllerFor('invitations.invitation').send('zookies');
      console.log('return after calling application:manageCookies:send.zookies');
      // break out of function for testing
      return;
            // branch based upon cookie
      //campaign cookie
      if(cookieName==='campaign'){
        logCookie(cookieName, cookieValue, cookieOperation);
        cookieValue = getCookie(cookieName);
        console.log('cookie value: '+cookieValue);
//************
      //  this.controllerFor('invitations.invitation').set(appData.model.sponsorId, cookieValue);
      // var ctrlInvitations = this.controllerFor('invitations');
      // ctrlInvitations.set('applicationModelBody','scooby doo');
//************
        // this.model.set('campaignId', 'test 123');
      } else if (cookieName === 'sponsor'){
        logCookie(cookieName, cookieValue, cookieOperation);
      }

      //sponsor cookie

      //log debugger
      function logCookie(cookieName, cookieValue, cookieOperation){
        console.log('=================================');
        console.log('cookie name: '+cookieName+' cookieValue: '+cookieValue+' cookieOperation: '+cookieOperation);
        console.log('=================================');
      }
      // Does the cookie exist
      function checkForExistingCookie(cookieToCheckFor){

      }
      // set cookie value
      function setCookie(cookieName, cookieValue){
        document.cookie=cookieName+'='+cookieValue;
      }
      // get the cookie value
      function getCookie(cookieName){
        var cookieArray;
        var cookieName;
        cookieName = cookieName + "=";
        cookieArray = document.cookie.split(';');
        console.log('cookieArray: '+cookieArray);
        // Is our cookie in th array
        for(var i=0; i<cookieArray.length; i++){
          var cookie = cookieArray[i];
          while (cookie.charAt(0)===' ') { cookie = cookie.substring(1); }
            if (cookie.indexOf(cookieName) ===0) {
              cookie = cookie.substring(cookieName.length,cookie.length);
              console.log('cookie is: '+cookie);
              return cookie;
            }
          }
          return false;
      }
    },
    // Set user and sponsor cookies
    createCampaignCookie: function() {
      console.log('========route.application.createCampaignCookie==========');
      var href = window.location.href;
      var hrefArray = href.split("/");
      // take last array element
      var campaignId = hrefArray[hrefArray.length-1];
      var campaignCookie = 'campaign='+campaignId;
      document.cookie = campaignCookie;
      var result = checkForExistingCookie('campaign');
      console.log('campaignCookie exists: '+result);

      function checkForExistingCookie(cookieToCheckFor){
        var cookieArray;
        var cookieName;
        cookieName = cookieToCheckFor + "=";
        cookieArray = document.cookie.split(';');
        console.log('cookieArray: '+cookieArray);
        // Is our cookie in th array
        for(var i=0; i<cookieArray.length; i++){
          var cookie = cookieArray[i];
          while (cookie.charAt(0)===' ') { cookie = cookie.substring(1); }
          //if (cookie.indexOf(cookieName) ==0) return cookie.substring(cookieName, cookie.length);
          if (cookie.indexOf(cookieName) ===0) { return true; }
        }
        return false;
      }
    },

    //Syncs main menu to side-menu component
    setCurrentMenu: function(currentMenu) {
      var currentRoute = this.routename;
      console.log("currentRoute: " + currentRoute);
      console.log("currentMenu: " + currentMenu);
      return true;
    },
    createSession: function() {
    //  console.log('createSessionInAppRouter');
      this.transitionTo('sign-in');
    },
    destroySession: function() {
    //  console.log('destroySessionInAppRouter');
      this.session.invalidate();
    },
    mySideMenuMarketLoaded: function() {
      console.log('application.js responding to mySideMenuMarketLoaded');
    },
    shortCircuitSideMenuLoaded: function(menuName) {
      console.log('application.js recieved message from directly from side-menu component');
      //Print existing controller menu setting
      console.log('Current menu is: ' + this.controller.get('selectedMainMenu'));

      //Change selectedMainMenu in Controller - tracks active class
      this.controller.set('selectedMainMenu', menuName);
      //Print after update
      console.log('Current menu is: ' + this.controller.get('selectedMainMenu'));

      console.log('Current route: ' + this.routeName);

      // console.log('currentHandlerIfo() ' + Client.Router.router.currentHandlerInfos[0][0]);

      // console.log('model.currentMenu prior to update: ' + this.model.get('currentMenu'));
      // model.set('currentMenu', 'Dog');
    },
  }, //end of actions



});
