// client/app/controllers/application.js

import Ember from 'ember';
//
export default Ember.Controller.extend({
  beforeModel: function(transition) {
    // if (!App.Post) {
    //   return Ember.$.getScript('/models/post.js');
    // }
  },

  model: function() {
    return this.store.find('sponsor', '99502e37-001e-419b-a3ec-b32121e598c7');
  },
  //needs: ['invitations'],
  // Tracks which main menu should be active
  // syncs main menu with the side menu
  // otherwise the main menu will loose the active class
  //when the side menu goes to the sub route
  selectedMainMenu: 'menu-welcome',

  appCtrlData2: 'appCtrlData2 - from application controller',
  appCtrlData3: 'appCtrlData3 - from application route',

  // uuid of the user's sponsor
  sponsorId: '99502e37-001e-419b-a3ec-b32121e598c7',
  campaignId: 'xxxf9991067-7b60-4d98-81ec-0338e44b3c0d',

  // model.set(title, 'Scooby do');

  // // Set user and sponsor cookies
  // updateCookies: function(params) {
  //   console.log('======JSON.stringify(params))================');
  //   console.log(JSON.stringify(params));
  //   console.log('=============================================');
  //   var userPath = window.location.href;
  //   var userCookie = 'userCookieData='+userPath;
  //   var sponsorCookie = 'sponsorCookieData=AAAAAA';
  //   document.cookie = userCookie;
  //   document.cookie = sponsorCookie;
  // }.observes('currentPath'),

  // Called each time the path is switched
  // currentPathDidChange: function() {
  //   var path = this.get('currentPath');
  //   console.log('path changed to: ', path);
  //   window.document.title = path;
  //   var cookieName = 'myCookie';
  //   var cookieName2 = 'myCookie2';
  //   var cookieData = cookieName+"="+path;
  //   var cookieData2 = cookieName2+"="+path;
  //   document.cookie = cookieData;
  //   document.cookie = cookieData2;
  //   console.log('**********************************');
  //   var cookieValue = document.cookie.split(';');
  //   console.log('cookieValue\n'+cookieValue);
  // }.observes('currentPath'),

  getCookieTest: function() {
    // var model = this.get('application.model');
    // var model = this.get('model');
  //  this.set('campaignId', 'mouse');
    // this.set('campaignId', 'dddddd');
    // console.log(model);
    console.log('Application controller init');
    // this.send('testAction');

    // this.send('readCampaignCookie');

    //  this.model.set('campaignId', 'junk');
    //  this.send('application.save');
  }.on('init'),

  SetAdvertisment: function(){
    console.log('*****************************');
    console.log('Calling advertisement service');
    this.advertisementService.logFoo();
  }.on('init'),

  actions: {
      // testFunction: function(){
      //   console.log('called application::testFunction');
      //   //this.model.set('sponsorId', 'new sponsor id# 123');
      // }
      testAction: function(){
          console.log('testAction called');
          // this.set('model.campaignId', 'ddddd');
          console.log(document.cookie);
          var sId;
          var myModel = this.get('model');

          //sId = this.controller.get('campaignId');
          console.log('testAction SponsorId: '+sId);
          // this.send('appRouterTestAction');
      },

      readCampaignCookie: function(){
        console.log('Read Campaign Cookie');
        // get the cookie value

          var cookieArray;
          var cookieName;
          cookieName='campaign';
          cookieName = cookieName + "=";
          cookieArray = document.cookie.split(';');
          console.log('cookieArray: '+cookieArray);
          // Is our cookie in the array
          var dog = 'dog';
          for(var i=0; i<cookieArray.length; i++){
            console.log ('1 dog is '+dog);
            var cookie = cookieArray[i];
            while (cookie.charAt(0)===' ') { cookie = cookie.substring(1); }
              if (cookie.indexOf(cookieName) ===0) {
                console.log ('2 dog is '+dog);
                cookie = cookie.substring(cookieName.length,cookie.length);
                console.log('cookie is: '+cookie);
                //return cookie;
                console.log('Setting campaign Id');
                var t;
                t = this.get('campaignId');
                console.log(t);
                // this.set('campaignId', cookie);
              }
            }
            return false;
          }
        },
  menuChanged: function() {
      console.log('menuChanged-Observer fired');
  }.observes('selectedMainMenu'),

  //http://stackoverflow.com/questions/15019212/ember-app-router-router-currentstate-undefined
  updateCurrentPath: function() {
    // was App.this.set() - was show error, so changed to this.set()
    this.set('currentPath', this.get('currentPath'));
  }.observes('currentPath')

});
//  app-stuff: 'application stuff goes here',
//  app-more_stuff: 'application more stuff',
//
////  setupController: function(controller, model) {
////    controller.set('model', model);
////  },
//});
