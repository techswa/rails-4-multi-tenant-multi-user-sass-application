import Ember from 'ember';

// export default Ember.Controller.extend({

  export default Ember.Component.extend({
    insertMap: function() {
      var container = this.$(".map-canvas");

      var options = {
        center: new window.google.maps.LatLng(this.get("latitude"),
        this.get("longitude")),
        zoom: 17,
        mapTypeId: window.google.maps.MapTypeId.ROADMAP
      };

      this.set('map', new window.google.maps.Map(container[0], options));
      //Display markers
      this.setMarkers();
    }.on('didInsertElement'),

  coordinatesChanged: function() {
      var map = this.get('map');

      if (map) {
        map.setCenter(new window.google.maps.LatLng(this.get('latitude'), this.get('longitude')));
      }
    }.observes('latitude', 'longitude'),

  setMarkers: function() {
      var map = this.get('map'),
          markers = this.get('markers'),
          markerCache = this.get('markerCache');

      // Remove all existing markers
      // markerCache.forEach(function(marker){
      //    marker.setMap(null);
      // });

      markers.forEach(function(marker){
        var gMapsMarker = new window.google.maps.Marker({
          position: new window.google.maps.LatLng(marker.get('latitude'), marker.get('longitude')),
          map: map
        });
        // Add this marker to our cache
        markerCache.pushObject(gMapsMarker);
      }, this);
    }.observes('markers.@each.latitude', 'markers.@each.longitude')

});
