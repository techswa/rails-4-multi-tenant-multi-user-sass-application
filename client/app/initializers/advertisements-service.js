// from http://chrishenn.net/writing/using-services-ember.html
// http://stackoverflow.com/questions/27932790/how-to-do-dependency-injection-in-ember-with-ember-cli/27933932#27933932

import Advertisements from '../services/advertisements';

export default {
  name: 'advertisement-service',
  after: '',

  initialize: function(container, app) {
    app.register('advertisments:main', Advertisements, {singleton: true} );
    // Inject into all routes and controllers

    // Component injection
    //app.inject( 'component:system-notifications', 'notificationService', 'service:notifications' );

    app.inject('route', 'advertisementService', 'service:advertisements');
    app.inject('controller', 'advertisementService', 'service:advertisements');
  }
};
