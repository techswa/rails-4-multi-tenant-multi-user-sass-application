// from http://chrishenn.net/writing/using-services-ember.html
// http://stackoverflow.com/questions/27932790/how-to-do-dependency-injection-in-ember-with-ember-cli/27933932#27933932

import Notifications from '../services/notifications';

export default {
  name: 'notification-service',
  after: '',

  initialize: function(container, app) {
    app.register('notifications:main', Notifications, {singleton: true} );
    // Inject into all routes and controllers

    // Component injection
    //app.inject( 'component:system-notifications', 'notificationService', 'service:notifications' );

    app.inject('route', 'notificationService', 'service:notifications');
    app.inject('controller', 'notificationService', 'service:notifications');
  }
};
