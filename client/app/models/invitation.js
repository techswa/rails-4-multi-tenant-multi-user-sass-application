import DS from 'ember-data';

export default DS.Model.extend({
  businessName: DS.attr('string'),
  firstName: DS.attr('string'),
  lastName: DS.attr('string'),
  primaryPhone: DS.attr('string'),
  userName: DS.attr('string'),
});
