import DS from 'ember-data';

export default DS.Model.extend({
  businessName: DS.attr('string'),
  firstName: DS.attr('string'),
  lastName: DS.attr('string'),
  primaryPhone: DS.attr('string'),
  userName: DS.attr('string'),
  email: DS.attr('string'),
  currentSignInAt: DS.attr('string'),
  lastSignInAt: DS.attr('string'),
  currentSignInIp: DS.attr('string'),
  lastSignInIp: DS.attr('string'),
  signInCount: DS.attr('string'),
  campaignId: DS.attr('string'),
  lookupsIndustryId: DS.attr('string'),
  sponsorsTypeId: DS.attr('string'),
  licenseNumber: DS.attr('string'),
  licenseEmail: DS.attr('string'),
  address_1: DS.attr('string'),
  address_2: DS.attr('string'),
  city: DS.attr('string'),
  lookupsState: DS.belongsTo('lookups_state'),
  zipcode: DS.attr('string'),
  website: DS.attr('string'),
  notes: DS.attr('string'),

});
