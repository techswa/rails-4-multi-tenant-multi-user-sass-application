import DS from 'ember-data';

export default DS.Model.extend({
  fipsState: DS.attr('string'),
  fullName: DS.attr('string'),
  abbreviatedName: DS.attr('string'),
  sponsors: DS.hasMany('sponsor')
});
