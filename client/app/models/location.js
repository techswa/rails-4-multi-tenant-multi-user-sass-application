import DS from 'ember-data';

export default DS.Model.extend({
  businessName: DS.attr('string'),
  firstName: DS.attr('string'),
  lastName: DS.attr('string'),
  primaryPhone: DS.attr('string'),
  userName: DS.attr('string'),
  password: DS.attr('string'),
  passwordConfirmation: DS.attr('string'),
  // email: DS.attr('string'),
  // currentSignInAt: DS.attr('string'),
  // lastSignInAt: DS.attr('string'),
  // currentSignInIp: DS.attr('string'),
  // lastSignInIp: DS.attr('string'),
  // signInCount: DS.attr('string'),
  // campaignId: DS.attr('string'),
  // lookupsIndustryId: DS.attr('string'),
  // sponsorsTypeId: DS.attr('string'),
  // licenseNumber: DS.attr('string'),
  // licenseEmail: DS.attr('string'),
  // address_1: DS.attr('string'),
  // address_2: DS.attr('string'),
  // city: DS.attr('string'),
  // lookupsStateId: DS.attr('string'),
  // zipcode: DS.attr('string'),
  // website: DS.attr('string'),
  // notes: DS.attr('string'),
  // 
  // bizName: Ember.computed('businessName', 'lastName', function(){
  //   return this.get('businessName') + this.get('lastName');
  // }),
  //
  // fullName: Ember.computed('firstName', 'lastName', function(){
  //   return this.get('firstName') + ' ' + this.get('lastName');
  // })

});
