import Ember from 'ember';

export default Ember.Handlebars.makeBoundHelper(function(value) {
  console.log(value);
  return new Ember.Handlebars.SafeString(value);
});
