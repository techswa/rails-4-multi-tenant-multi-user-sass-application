
import Ember from 'ember';

export default Ember.Object.extend({
  initNotifications: function(){
    console.log('Notifications service is initializing');
  }.on('init'),

  logFoo: function(){
    console.log('-----------------------------------------------');
    console.log('Notification service has been called and works.');
    console.log('-----------------------------------------------');
  }
});
