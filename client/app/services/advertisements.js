
import Ember from 'ember';

export default Ember.Object.extend({

  sponsorX: 'abcdef',
  templateId: 'xf9991067-7b60-4d98-81ec-0338e44b3c0d',

  initNotifications: function(){
    console.log('Advertisements service is initializing');
    var cookie = document.cookie;
    this.set('sponsorX', cookie);

  }.on('init'),

  getTemplateId: function(){
    // This will retieve the cookie and or local storage cookie data
    // it will then set the templateId var above
    // the application router will thn request the tempalteID and execute the query
    console.log('===================');
    console.log('Cookie data: '+document.cookie);
    this.set('templateId','f9991067-7b60-4d98-81ec-0338e44b3c0d');
    console.log('===================');
  },

  sponsorId: function(){
    var templateId = 'xxxf9991067-7b60-4d98-81ec-0338e44b3c0d';
    // this.get('controller').get.('store').find('template','xxxf9991067-7b60-4d98-81ec-0338e44b3c0d');
    this.Model.store.find('template', templateId);
    return this.get('sponsorX');
  },

  logFoo: function(){
    console.log('-------------------------------------------------');
    console.log('Advertisements service has been called and works.');
    console.log('sponsorX: '+this.get('sponsorX'));
    console.log('-------------------------------------------------');
  }
});
