import SponsorsBaseController from '../base';

export default SponsorsBaseController.extend({
  needs: 'sponsors.signup',

  actions: {
    cancel: function() {
      // This should be the current sponsor record sponsor.show
      this.transitionToRoute('sponsors.index');
      return false;
    }
  }

});
