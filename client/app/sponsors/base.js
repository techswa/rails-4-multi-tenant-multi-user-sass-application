import Ember from 'ember';

export default Ember.Controller.extend({

actions: {
  save: function() {
    //Add validation
    var _self = this;
    this.get('model').save().then(function(sponsor) {
      _self.transitionToRoute('sponsors.show', sponsor);
    });
  },
  cancel: function() {
    return true;
  }
}
});
