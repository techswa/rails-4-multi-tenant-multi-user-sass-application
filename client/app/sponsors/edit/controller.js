// sponsor/edit

import SponsorsBaseController from '../base';

export default SponsorsBaseController.extend({

  // needs: 'sponsors.signup',
  actions: {
    cancel: function() {
      this.transitionToRoute('sponsors.show', this.get('model'));
      return false;
    }
  }

});
