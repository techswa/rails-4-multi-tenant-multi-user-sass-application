import Ember from 'ember';

export default Ember.Controller.extend({

  // Include application controller
  needs: ['application'],
  // Create binding to the controller
  controllersApplication: Ember.computed.alias("controllers.application"),

  testVal: 'test value',

  // Set user and sponsor cookies
  ObsUpdateCookies: function(self) {
    console.log('ObsUpdateCookies');
    self.updateCookies();
  }.observes('currentPath'),

  // Bind properties for use
  // sponsorDetailsBinding: 'controllers.application.appSponsor'
  //computed alias can be overwritten later
  appData: Ember.computed.alias("controllers.application"),
  //Binding is static
  applicationModelBody: Ember.computed.alias("controllers.application.model.body"),
  applicationModel: Ember.computed.alias("controllers.application.model"),

  myCampaignId: Ember.computed.alias("controllers.application.model.campaignId"),


  actions: {
    setApplicationTitle: function() {
      console.log('setApplication Title action called');

    //  this.send('updateCookies');
      this.set('applicationModel.body','12345');
      this.send('testFunction');
    },
    zookies: function(){
      console.log("zookies:this.appData.model.set('sponsorId', 'it finally works')");
      //  this.controllerFsor('application').send('testFunction');
      // this.appData.model.set('sponsorId', 'it finally works');
      //this.controllersApplication.model.set('sponsorId','dddd');

      console.log("zookies:invitation.controller.updateCookies returned");
    }
  },
});
