import Ember from 'ember';

export default Ember.Route.extend({

  activate: function(){
    console.log('Entered invitations.invitation.route');

    // call function in the application route
    // probobly better to have in controller - call on load
    this.send('manageCookies','campaign','hog', 'set');
  },
  actions: {
    updateCookies: function(){
      console.log('invitation.route.updateCookies called');


      return true;
    },
  }
});
