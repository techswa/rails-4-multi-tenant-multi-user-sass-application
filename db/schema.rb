# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150217165609) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "accounting_payment_types", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "name"
    t.integer  "sort"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "accounting_payments", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.uuid     "sponsor_id"
    t.uuid     "accounting_payment_type_id"
    t.integer  "amount"
    t.uuid     "staff_id"
    t.text     "note"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "accounting_statuses", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.uuid     "sponsors_sponsor_id"
    t.boolean  "suspended"
    t.date     "grace"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "accounting_statuses", ["sponsors_sponsor_id"], name: "index_accounting_statuses_on_sponsors_sponsor_id", unique: true, using: :btree

  create_table "advertisements_advertisements", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.uuid     "sponsor_id"
    t.string   "title"
    t.text     "description"
    t.float    "revision"
    t.uuid     "advertisements_group_id"
    t.uuid     "advertisements_template_id"
    t.uuid     "images_imgfile_id"
    t.boolean  "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "advertisements_groups", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.uuid     "sponsors_type_id"
  end

  create_table "advertisements_mechanicals", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "display_width"
    t.integer  "display_height"
    t.string   "symbol_name"
    t.boolean  "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "advertisements_rotations", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.uuid     "sponsor_id"
    t.uuid     "advertisements_advertisement_id"
    t.boolean  "running"
    t.datetime "start_date"
    t.datetime "stop_date"
    t.text     "notes"
    t.boolean  "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.uuid     "advertisements_mechanical_id"
    t.uuid     "marketing_campaign_id"
    t.string   "title"
    t.boolean  "active"
  end

  create_table "advertisements_templates", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.uuid     "sponsor_id"
    t.text     "code"
    t.boolean  "archived",                     default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.uuid     "advertisements_mechanical_id"
    t.uuid     "sponsors_franchise_id"
  end

  add_index "advertisements_templates", ["sponsors_franchise_id", "title"], name: "sponsorsfranchise_title_idx", using: :btree

  create_table "images_imgfiles", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.uuid     "sponsorships_sponsor_id"
    t.uuid     "images_type_id"
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.boolean  "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "images_presets", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "symbol_name"
    t.integer  "preview_width"
    t.integer  "preview_height"
    t.integer  "display_width"
    t.integer  "display_height"
    t.boolean  "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "images_types", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "symbol_name"
    t.uuid     "images_preset_id"
    t.boolean  "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lookups_counties", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "full_name"
    t.string   "fips_county"
    t.string   "fips_state"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lookups_exchanges", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "full_name"
    t.string   "abbreviated_name"
    t.string   "city"
    t.uuid     "lookups_state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lookups_industries", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "naics_code"
    t.string   "title"
    t.text     "note"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lookups_states", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "fips_state"
    t.string   "full_name"
    t.string   "abbreviated_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "marketing_campaigns", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.uuid     "sponsor_id"
    t.uuid     "marketing_channel_id"
    t.string   "title"
    t.text     "description"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "sent"
    t.integer  "responses"
    t.integer  "signups"
    t.text     "notes"
    t.boolean  "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.uuid     "advertisements_rotation_id"
  end

  create_table "marketing_channels", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.uuid     "sponsor_id"
    t.string   "title"
    t.text     "description"
    t.boolean  "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sponsors", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "user_name",              default: "", null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.uuid     "marketing_campaign_id"
    t.uuid     "lookups_industry_id"
    t.uuid     "sponsors_type_id"
    t.string   "license_number"
    t.string   "license_email"
    t.string   "business_name"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "primary_phone"
    t.string   "address_1"
    t.string   "address_2"
    t.string   "city"
    t.uuid     "lookups_state_id"
    t.string   "zipcode"
    t.string   "website"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "authentication_token"
    t.uuid     "campaign_id"
    t.uuid     "sponsors_level_id"
  end

  add_index "sponsors", ["email"], name: "index_sponsors_on_email", unique: true, using: :btree
  add_index "sponsors", ["reset_password_token"], name: "index_sponsors_on_reset_password_token", unique: true, using: :btree
  add_index "sponsors", ["user_name"], name: "index_sponsors_on_user_name", unique: true, using: :btree

  create_table "sponsors_details", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.uuid     "sponsor_id"
    t.string   "company_name"
    t.string   "contact_last_name"
    t.string   "contact_first_name"
    t.string   "phone_1"
    t.string   "phone_2"
    t.string   "website"
    t.string   "email"
    t.string   "tag_line_1"
    t.string   "tag_line_2"
    t.text     "welcome_greeting"
    t.text     "return_greeting"
    t.string   "logo_filename"
    t.string   "headshot_filename"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.uuid     "sponsors_style_id"
  end

  create_table "sponsors_franchises", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sponsors_levels", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sponsors_portals", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.uuid     "sponsor_id"
    t.uuid     "portal_setting_id"
    t.boolean  "disabled"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sponsors_styles", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sponsors_types", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "symbol_name"
    t.boolean  "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sponsorships_businesses", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.uuid     "vendor_id"
    t.uuid     "sponsor_id"
    t.uuid     "marketing_campaign_id"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active"
  end

  create_table "sponsorships_consumers", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.uuid     "sponsors_sponsor_id"
    t.uuid     "user_id"
    t.uuid     "marketing_campaign_id"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sponsorships_vendors", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.uuid     "vendor_id"
    t.uuid     "sponsor_id"
    t.uuid     "marketing_campaign_id"
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "staffs", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "user_name",              default: "",    null: false
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "staff_role_id"
    t.text     "note"
    t.boolean  "disabled",               default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "staffs", ["email"], name: "index_staffs_on_email", unique: true, using: :btree
  add_index "staffs", ["reset_password_token"], name: "index_staffs_on_reset_password_token", unique: true, using: :btree
  add_index "staffs", ["user_name"], name: "index_staffs_on_user_name", unique: true, using: :btree

  create_table "statistics_devices", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.text     "note"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "statistics_visits", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.uuid     "user_id"
    t.uuid     "sponsors_sponsor_id"
    t.uuid     "marketing_campaign_id"
    t.text     "session_id"
    t.uuid     "statistics_device_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tests", force: true do |t|
    t.string   "name"
    t.string   "color"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", id: :uuid, default: "uuid_generate_v4()", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "last_name"
    t.string   "first_name"
    t.text     "role"
    t.uuid     "campaign_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
