class RenamePositionToMechanicalOnAdvertisementsTemplates < ActiveRecord::Migration
  def change
    rename_column :advertisements_templates, :advertisements_position_id, :advertisements_mechanical_id
  end
end
