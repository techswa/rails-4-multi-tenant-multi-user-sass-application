class RenameStyleIdToSponsorsStyleId < ActiveRecord::Migration
  def change
    rename_column :sponsors_details, :style_id, :sponsors_style_id
  end
end
