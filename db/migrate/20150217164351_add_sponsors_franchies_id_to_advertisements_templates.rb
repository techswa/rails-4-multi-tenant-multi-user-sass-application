class AddSponsorsFranchiesIdToAdvertisementsTemplates < ActiveRecord::Migration
  def change
    add_column :advertisements_templates, :sponsors_franchise_id, :uuid
  end
end
