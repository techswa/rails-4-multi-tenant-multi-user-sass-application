class AddLevelIdToSponsor < ActiveRecord::Migration
  def change
    add_column :sponsors, :sponsors_level_id, :uuid
  end
end
