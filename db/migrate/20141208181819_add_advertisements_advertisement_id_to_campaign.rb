class AddAdvertisementsAdvertisementIdToCampaign < ActiveRecord::Migration
  def change
    add_column :marketing_campaigns, :advertisements_advertisement_id, :uuid
  end
end
