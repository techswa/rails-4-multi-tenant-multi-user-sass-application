class CreateAdvertisementsGroups < ActiveRecord::Migration
  def change
    create_table :advertisements_groups do |t|
      t.string :title
      t.text :description
      t.boolean :archived

      t.timestamps
    end
  end
end
