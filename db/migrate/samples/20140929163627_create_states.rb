class CreateStates < ActiveRecord::Migration
  def change
    create_table :states, id: :uuid do |t|
      t.text :fips
      t.text :name
      t.text :abbreviation

      t.timestamps
    end
  end
end
