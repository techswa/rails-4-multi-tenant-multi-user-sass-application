class CreateCounties < ActiveRecord::Migration
  def change
    create_table :counties do |t|
      t.text :fips
      t.text :state_fips
      t.text :name

      t.timestamps
    end
  end
end
