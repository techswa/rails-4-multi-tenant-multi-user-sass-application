class AddSponsorsTypeIdToAdvertisementsGroup < ActiveRecord::Migration
  def change
    add_column :advertisements_groups, :sponsors_type_id, :uuid
  end
end
