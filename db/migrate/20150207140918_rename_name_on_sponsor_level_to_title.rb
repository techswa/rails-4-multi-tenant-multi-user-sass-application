class RenameNameOnSponsorLevelToTitle < ActiveRecord::Migration
  def change
    rename_column :sponsors_levels, :name, :title
  end
end
