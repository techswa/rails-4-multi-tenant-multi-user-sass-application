class CreateSponsorshipsVendors < ActiveRecord::Migration
  def change
    create_table :sponsorships_vendors, id: :uuid do |t|
      t.uuid :vendor_id
      t.uuid :sponsor_id
      t.uuid :marketing_campaign_id
      t.date :start_date
      t.date :end_date
      t.boolean :active

      t.timestamps
    end
  end
end
