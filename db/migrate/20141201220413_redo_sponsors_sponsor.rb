class RedoSponsorsSponsor < ActiveRecord::Migration
  def change
    drop_table :sponsors_sponsors

    create_table :sponsors_sponsors, id: :uuid do |t|
      t.uuid :campaign_id
      t.uuid :user_id
      t.uuid :lookups_industry_id
      t.string :business_name
      t.string :first_name
      t.string :last_name
      t.string :license
      t.string :primary_phone
      t.string :address_1
      t.string :address_2
      t.string :city
      t.uuid :lookups_state_id
      t.string :zipcode
      t.string :primary_email
      t.string :website
      t.uuid :sponsors_type_id
      t.text :notes

      t.timestamps
    end
  end
end
