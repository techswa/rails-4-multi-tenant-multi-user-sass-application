class AddAdvertisementsPositionIdToAdvertisementsRotations < ActiveRecord::Migration
  def change
    add_column :advertisements_rotations, :advertisements_position_id, :uuid
  end
end
