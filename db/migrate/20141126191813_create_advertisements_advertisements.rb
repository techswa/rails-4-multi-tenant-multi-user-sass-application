class CreateAdvertisementsAdvertisements < ActiveRecord::Migration
  def change
    create_table :advertisements_advertisements, id: :uuid do |t|
      t.uuid :sponsors_sponsor_id
      t.string :title
      t.text :description
      t.float :revision
      t.string :symbol_name
      t.uuid :advertisements_source_id
      t.uuid :advertisements_position_id
      t.uuid :advertisements_template_id
      t.uuid :images_imgfile_id
      t.boolean :archived

      t.timestamps
    end
  end
end
