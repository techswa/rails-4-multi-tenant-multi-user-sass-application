class RenameSponsorOnMarketingCampaigns < ActiveRecord::Migration
  def change
    rename_column :marketing_campaigns, :sponsors_sponsor_id, :sponsor_id
  end
end
