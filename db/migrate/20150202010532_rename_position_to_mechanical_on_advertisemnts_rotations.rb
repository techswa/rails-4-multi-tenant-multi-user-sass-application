class RenamePositionToMechanicalOnAdvertisemntsRotations < ActiveRecord::Migration
  def change
    rename_column :advertisements_rotations, :advertisements_position_id, :advertisements_mechanical_id
  end
end
