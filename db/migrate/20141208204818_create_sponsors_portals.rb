class CreateSponsorsPortals < ActiveRecord::Migration
  def change
    create_table :sponsors_portals, id: :uuid do |t|
      t.uuid :sponsor_id
      t.uuid :portal_setting_id
      t.boolean :disabled

      t.timestamps
    end
  end
end
