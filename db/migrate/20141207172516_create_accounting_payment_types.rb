class CreateAccountingPaymentTypes < ActiveRecord::Migration
  def change
    create_table :accounting_payment_types, id: :uuid do |t|
      t.string :name
      t.integer :sort

      t.timestamps
    end
  end
end
