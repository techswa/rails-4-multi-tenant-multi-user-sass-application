class AddAdvertisementsGroupsToAdvertisementsTemplates < ActiveRecord::Migration
  def change
    add_column :advertisements_templates, :advertisements_group_id, :uuid
  end
end
