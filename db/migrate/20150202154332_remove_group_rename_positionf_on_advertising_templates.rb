class RemoveGroupRenamePositionfOnAdvertisingTemplates < ActiveRecord::Migration
  def change
    remove_column :advertisements_templates, :advertisements_group_id
    rename_column :advertisements_templates, :advertisements_position_id, :advertisements_mechanical_id
  end
end
