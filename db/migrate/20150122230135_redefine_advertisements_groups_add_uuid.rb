class RedefineAdvertisementsGroupsAddUuid < ActiveRecord::Migration
    def change
      drop_table :advertisements_groups
      create_table :advertisements_groups, id: :uuid do |t|
        t.string :title
        t.text :description
        t.boolean :archived
        t.timestamps
      end
    end

end
