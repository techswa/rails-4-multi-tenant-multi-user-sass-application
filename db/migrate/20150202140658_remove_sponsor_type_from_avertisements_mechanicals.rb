class RemoveSponsorTypeFromAvertisementsMechanicals < ActiveRecord::Migration
  def change
    remove_column :advertisements_mechanicals, :sponsors_type_id
  end
end
