class AddMarketingCampaignIdToAdvertisementsRotations < ActiveRecord::Migration
  def change
    add_column :advertisements_rotations, :marketing_campaign_id, :uuid
  end
end
