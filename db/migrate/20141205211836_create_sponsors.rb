class CreateSponsors < ActiveRecord::Migration
  def change
    create_table :sponsors, id: :uuid do |t|
      t.uuid :campaign_id
      t.uuid :lookups_industry_id
      t.uuid :sponsors_type_id
      t.string :license_number
      t.string :license_email
      t.string :business_name
      t.string :first_name
      t.string :last_name
      t.string :primary_phone
      t.string :address_1
      t.string :address_2
      t.string :city
      t.uuid :lookups_state_id
      t.string :zipcode
      t.string :website
      t.string :notes

      t.timestamps
    end
  end
end
