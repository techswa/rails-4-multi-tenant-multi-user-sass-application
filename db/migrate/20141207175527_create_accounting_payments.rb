class CreateAccountingPayments < ActiveRecord::Migration
  def change
    create_table :accounting_payments, id: :uuid do |t|
      t.uuid :sponsor_id
      t.uuid :accounting_payment_type_id
      t.integer :amount
      t.uuid :staff_id
      t.text :note

      t.timestamps
    end
  end
end
