class RenameLicenseOnSponsorsSponsors < ActiveRecord::Migration
  def change
    rename_column :sponsors_sponsors, :license, :license_number
  end
end
