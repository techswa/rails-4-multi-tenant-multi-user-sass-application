class AddAuthenticationTokenToSponsor < ActiveRecord::Migration
  def change
    add_column :sponsors, :authentication_token, :string
  end
end
