class AddAdvertisementsPositionIdToAdvertismentsTemplates < ActiveRecord::Migration
  def change
    add_column :advertisements_templates, :advertisements_position_id, :uuid
  end
end
