class CreateStaffs < ActiveRecord::Migration
  def change
    create_table :staffs, id: :uuid do |t|
      t.string :first_name
      t.string :last_name
      t.integer :staff_role_id
      t.text :note
      t.boolean :disabled, default: false

      t.timestamps
    end
  end
end
