class AddActiveToAdvertisementsRotations < ActiveRecord::Migration
  def change
    add_column :advertisements_rotations, :active, :boolean
  end
end
