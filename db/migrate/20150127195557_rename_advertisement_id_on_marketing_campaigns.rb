class RenameAdvertisementIdOnMarketingCampaigns < ActiveRecord::Migration
  def change
    rename_column :marketing_campaigns, :advertisements_advertisement_id, :advertisements_rotation_id
  end
end
