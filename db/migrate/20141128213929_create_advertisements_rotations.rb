class CreateAdvertisementsRotations < ActiveRecord::Migration
  def change
    create_table :advertisements_rotations, id: :uuid do |t|
      t.uuid :sponsors_sponsor_id
      t.uuid :advertisements_advertisement_id
      t.boolean :running
      t.datetime :start_date
      t.datetime :stop_date
      t.text :notes
      t.boolean :archived

      t.timestamps
    end
  end
end
