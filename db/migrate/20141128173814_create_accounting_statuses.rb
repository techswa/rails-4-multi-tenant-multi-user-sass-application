class CreateAccountingStatuses < ActiveRecord::Migration
  def change
    create_table :accounting_statuses, id: :uuid do |t|
      t.uuid :sponsors_sponsor_id
      t.boolean :suspended
      t.date :grace

      t.timestamps
    end
  end
end
