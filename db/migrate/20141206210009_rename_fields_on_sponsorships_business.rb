class RenameFieldsOnSponsorshipsBusiness < ActiveRecord::Migration
  def change
    rename_column :sponsorships_businesses, :sponsors_sponsor_id, :sponsor_id
    rename_column :sponsorships_businesses, :sponsors_sponsored_id, :sponsored_id
  end
end
