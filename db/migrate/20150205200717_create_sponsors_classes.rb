class CreateSponsorsClasses < ActiveRecord::Migration
  def change
    create_table :sponsors_styles, id: :uuid do |t|
      t.string :name
      t.text :description
      t.boolean :archived

      t.timestamps
    end
  end
end
