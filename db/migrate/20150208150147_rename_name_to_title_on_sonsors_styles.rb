class RenameNameToTitleOnSonsorsStyles < ActiveRecord::Migration
  def change
    rename_column :sponsors_styles, :name, :title
  end
end
