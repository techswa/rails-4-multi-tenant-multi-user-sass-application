class AddActiveToSponsorshipsBusinesses < ActiveRecord::Migration
  def change
    add_column :sponsorships_businesses, :active, :boolean
  end
end
