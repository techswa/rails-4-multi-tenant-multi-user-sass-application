class AddIndexUniqueOnAccountingStatus < ActiveRecord::Migration
  def change
    add_index :accounting_statuses, :sponsors_sponsor_id, :unique => true
  end
end
