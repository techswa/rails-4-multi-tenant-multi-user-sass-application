class AddTitleToAdvertisementsRotations < ActiveRecord::Migration
  def change
    add_column :advertisements_rotations, :title, :string
  end
end
