class CreateSponsorsFranchises < ActiveRecord::Migration
  def change
    create_table :sponsors_franchises, id: :uuid do |t|
      t.string :title
      t.text :description
      t.boolean :archived

      t.timestamps
    end
  end
end
