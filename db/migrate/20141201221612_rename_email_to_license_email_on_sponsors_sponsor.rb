class RenameEmailToLicenseEmailOnSponsorsSponsor < ActiveRecord::Migration
  def change
    rename_column :sponsors_sponsors, :primary_email, :license_email
  end
end
