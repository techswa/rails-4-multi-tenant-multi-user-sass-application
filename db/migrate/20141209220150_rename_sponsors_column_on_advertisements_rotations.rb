class RenameSponsorsColumnOnAdvertisementsRotations < ActiveRecord::Migration
  def change
    rename_column :advertisements_rotations, :sponsors_sponsor_id, :sponsor_id
  end
end
