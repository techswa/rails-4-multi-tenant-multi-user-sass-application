class RenameCampaignOnSponsor < ActiveRecord::Migration
  def change
    rename_column :sponsors, :campaign_id, :marketing_campaign_id
  end
end
