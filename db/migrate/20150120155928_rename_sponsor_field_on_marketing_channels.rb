class RenameSponsorFieldOnMarketingChannels < ActiveRecord::Migration
  def change
    rename_column :marketing_channels, :sponsors_sponsor_id, :sponsor_id
  end
end
