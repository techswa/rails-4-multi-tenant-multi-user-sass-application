class RenameSponsorColumnsOnSponsorshipsBusiness < ActiveRecord::Migration
  def change
    rename_column :sponsorships_businesses, :sponsor_id, :vendor_id
    rename_column :sponsorships_businesses, :sponsored_id, :sponsor_id
  end
end
