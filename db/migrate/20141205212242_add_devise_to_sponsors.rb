class AddDeviseToSponsors < ActiveRecord::Migration
  def self.up
    drop_table :sponsors
    create_table(:sponsors, id: :uuid) do |t|
      ## Database authenticatable
      t.string :user_name,          null: false, default: ""
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.inet     :current_sign_in_ip
      t.inet     :last_sign_in_ip

      ## Confirmable
      # t.string   :confirmation_token
      # t.datetime :confirmed_at
      # t.datetime :confirmation_sent_at
      # t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      # t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at


      t.uuid :campaign_id
      t.uuid :lookups_industry_id
      t.uuid :sponsors_type_id
      t.string :license_number
      t.string :license_email
      t.string :business_name
      t.string :first_name
      t.string :last_name
      t.string :primary_phone
      t.string :address_1
      t.string :address_2
      t.string :city
      t.uuid :lookups_state_id
      t.string :zipcode
      t.string :website
      t.text :notes


      t.timestamps
    end
    add_index :sponsors, :user_name,   unique: true
    add_index :sponsors, :email,                unique: true
    add_index :sponsors, :reset_password_token, unique: true
    # add_index :sponsors, :confirmation_token,   unique: true
    # add_index :sponsors, :unlock_token,         unique: true
  end

  def self.down
    # By default, we don't want to make any assumption about how to roll back a migration when your
    # model already existed. Please edit below which fields you would like to remove in this migration.
    raise ActiveRecord::IrreversibleMigration
  end
end
