class UpdateAdvertisementsAdvertisements < ActiveRecord::Migration
  def change
    rename_column :advertisements_advertisements, :advertisements_source_id, :advertisements_group_id
    remove_column :advertisements_advertisements, :advertisements_position_id
  end
end
