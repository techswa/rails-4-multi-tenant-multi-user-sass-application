class RenameSponsorsColumnOnAdvertisementsAdvertisements < ActiveRecord::Migration
  def change
    rename_column :advertisements_advertisements, :sponsors_sponsor_id, :sponsor_id
  end
end
