class AddCampaignIdToSponsor < ActiveRecord::Migration
  def change
    add_column :sponsors, :campaign_id, :uuid
  end
end
