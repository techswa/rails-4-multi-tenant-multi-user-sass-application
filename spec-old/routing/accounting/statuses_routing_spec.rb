require "rails_helper"

RSpec.describe Accounting::StatusesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/accounting/statuses").to route_to("accounting/statuses#index")
    end

    it "routes to #new" do
      expect(:get => "/accounting/statuses/new").to route_to("accounting/statuses#new")
    end

    it "routes to #show" do
      expect(:get => "/accounting/statuses/1").to route_to("accounting/statuses#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/accounting/statuses/1/edit").to route_to("accounting/statuses#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/accounting/statuses").to route_to("accounting/statuses#create")
    end

    it "routes to #update" do
      expect(:put => "/accounting/statuses/1").to route_to("accounting/statuses#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/accounting/statuses/1").to route_to("accounting/statuses#destroy", :id => "1")
    end

  end
end
