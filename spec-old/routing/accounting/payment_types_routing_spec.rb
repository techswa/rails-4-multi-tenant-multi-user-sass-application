require "rails_helper"

RSpec.describe Accounting::PaymentTypesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/accounting/payment_types").to route_to("accounting/payment_types#index")
    end

    it "routes to #new" do
      expect(:get => "/accounting/payment_types/new").to route_to("accounting/payment_types#new")
    end

    it "routes to #show" do
      expect(:get => "/accounting/payment_types/1").to route_to("accounting/payment_types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/accounting/payment_types/1/edit").to route_to("accounting/payment_types#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/accounting/payment_types").to route_to("accounting/payment_types#create")
    end

    it "routes to #update" do
      expect(:put => "/accounting/payment_types/1").to route_to("accounting/payment_types#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/accounting/payment_types/1").to route_to("accounting/payment_types#destroy", :id => "1")
    end

  end
end
