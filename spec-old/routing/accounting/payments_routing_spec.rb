require "rails_helper"

RSpec.describe Accounting::PaymentsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/accounting/payments").to route_to("accounting/payments#index")
    end

    it "routes to #new" do
      expect(:get => "/accounting/payments/new").to route_to("accounting/payments#new")
    end

    it "routes to #show" do
      expect(:get => "/accounting/payments/1").to route_to("accounting/payments#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/accounting/payments/1/edit").to route_to("accounting/payments#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/accounting/payments").to route_to("accounting/payments#create")
    end

    it "routes to #update" do
      expect(:put => "/accounting/payments/1").to route_to("accounting/payments#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/accounting/payments/1").to route_to("accounting/payments#destroy", :id => "1")
    end

  end
end
