require "rails_helper"

RSpec.describe Advertisements::SourcesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/advertisements/sources").to route_to("advertisements/sources#index")
    end

    it "routes to #new" do
      expect(:get => "/advertisements/sources/new").to route_to("advertisements/sources#new")
    end

    it "routes to #show" do
      expect(:get => "/advertisements/sources/1").to route_to("advertisements/sources#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/advertisements/sources/1/edit").to route_to("advertisements/sources#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/advertisements/sources").to route_to("advertisements/sources#create")
    end

    it "routes to #update" do
      expect(:put => "/advertisements/sources/1").to route_to("advertisements/sources#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/advertisements/sources/1").to route_to("advertisements/sources#destroy", :id => "1")
    end

  end
end
