require "rails_helper"

RSpec.describe Advertisements::AdvertisementsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/advertisements/advertisements").to route_to("advertisements/advertisements#index")
    end

    it "routes to #new" do
      expect(:get => "/advertisements/advertisements/new").to route_to("advertisements/advertisements#new")
    end

    it "routes to #show" do
      expect(:get => "/advertisements/advertisements/1").to route_to("advertisements/advertisements#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/advertisements/advertisements/1/edit").to route_to("advertisements/advertisements#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/advertisements/advertisements").to route_to("advertisements/advertisements#create")
    end

    it "routes to #update" do
      expect(:put => "/advertisements/advertisements/1").to route_to("advertisements/advertisements#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/advertisements/advertisements/1").to route_to("advertisements/advertisements#destroy", :id => "1")
    end

  end
end
