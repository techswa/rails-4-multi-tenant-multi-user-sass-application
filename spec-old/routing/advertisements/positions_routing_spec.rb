require "rails_helper"

RSpec.describe Advertisements::PositionsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/advertisements/positions").to route_to("advertisements/positions#index")
    end

    it "routes to #new" do
      expect(:get => "/advertisements/positions/new").to route_to("advertisements/positions#new")
    end

    it "routes to #show" do
      expect(:get => "/advertisements/positions/1").to route_to("advertisements/positions#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/advertisements/positions/1/edit").to route_to("advertisements/positions#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/advertisements/positions").to route_to("advertisements/positions#create")
    end

    it "routes to #update" do
      expect(:put => "/advertisements/positions/1").to route_to("advertisements/positions#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/advertisements/positions/1").to route_to("advertisements/positions#destroy", :id => "1")
    end

  end
end
