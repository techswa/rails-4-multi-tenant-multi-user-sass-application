require "rails_helper"

RSpec.describe Advertisements::TemplatesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/advertisements/templates").to route_to("advertisements/templates#index")
    end

    it "routes to #new" do
      expect(:get => "/advertisements/templates/new").to route_to("advertisements/templates#new")
    end

    it "routes to #show" do
      expect(:get => "/advertisements/templates/1").to route_to("advertisements/templates#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/advertisements/templates/1/edit").to route_to("advertisements/templates#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/advertisements/templates").to route_to("advertisements/templates#create")
    end

    it "routes to #update" do
      expect(:put => "/advertisements/templates/1").to route_to("advertisements/templates#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/advertisements/templates/1").to route_to("advertisements/templates#destroy", :id => "1")
    end

  end
end
