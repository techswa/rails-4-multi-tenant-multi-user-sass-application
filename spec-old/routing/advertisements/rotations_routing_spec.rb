require "rails_helper"

RSpec.describe Advertisements::RotationsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/advertisements/rotations").to route_to("advertisements/rotations#index")
    end

    it "routes to #new" do
      expect(:get => "/advertisements/rotations/new").to route_to("advertisements/rotations#new")
    end

    it "routes to #show" do
      expect(:get => "/advertisements/rotations/1").to route_to("advertisements/rotations#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/advertisements/rotations/1/edit").to route_to("advertisements/rotations#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/advertisements/rotations").to route_to("advertisements/rotations#create")
    end

    it "routes to #update" do
      expect(:put => "/advertisements/rotations/1").to route_to("advertisements/rotations#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/advertisements/rotations/1").to route_to("advertisements/rotations#destroy", :id => "1")
    end

  end
end
