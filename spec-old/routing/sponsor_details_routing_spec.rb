require "rails_helper"

RSpec.describe SponsorDetailsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/sponsor_details").to route_to("sponsor_details#index")
    end

    it "routes to #new" do
      expect(:get => "/sponsor_details/new").to route_to("sponsor_details#new")
    end

    it "routes to #show" do
      expect(:get => "/sponsor_details/1").to route_to("sponsor_details#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/sponsor_details/1/edit").to route_to("sponsor_details#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/sponsor_details").to route_to("sponsor_details#create")
    end

    it "routes to #update" do
      expect(:put => "/sponsor_details/1").to route_to("sponsor_details#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/sponsor_details/1").to route_to("sponsor_details#destroy", :id => "1")
    end

  end
end
