require "rails_helper"

RSpec.describe Sponsors::TypesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/sponsors/types").to route_to("sponsors/types#index")
    end

    it "routes to #new" do
      expect(:get => "/sponsors/types/new").to route_to("sponsors/types#new")
    end

    it "routes to #show" do
      expect(:get => "/sponsors/types/1").to route_to("sponsors/types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/sponsors/types/1/edit").to route_to("sponsors/types#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/sponsors/types").to route_to("sponsors/types#create")
    end

    it "routes to #update" do
      expect(:put => "/sponsors/types/1").to route_to("sponsors/types#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/sponsors/types/1").to route_to("sponsors/types#destroy", :id => "1")
    end

  end
end
