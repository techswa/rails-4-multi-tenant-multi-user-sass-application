require "rails_helper"

RSpec.describe Sponsors::PortalsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/sponsors/portals").to route_to("sponsors/portals#index")
    end

    it "routes to #new" do
      expect(:get => "/sponsors/portals/new").to route_to("sponsors/portals#new")
    end

    it "routes to #show" do
      expect(:get => "/sponsors/portals/1").to route_to("sponsors/portals#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/sponsors/portals/1/edit").to route_to("sponsors/portals#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/sponsors/portals").to route_to("sponsors/portals#create")
    end

    it "routes to #update" do
      expect(:put => "/sponsors/portals/1").to route_to("sponsors/portals#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/sponsors/portals/1").to route_to("sponsors/portals#destroy", :id => "1")
    end

  end
end
