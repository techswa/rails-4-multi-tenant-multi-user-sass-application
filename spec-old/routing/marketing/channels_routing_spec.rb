require "rails_helper"

RSpec.describe Marketing::ChannelsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/marketing/channels").to route_to("marketing/channels#index")
    end

    it "routes to #new" do
      expect(:get => "/marketing/channels/new").to route_to("marketing/channels#new")
    end

    it "routes to #show" do
      expect(:get => "/marketing/channels/1").to route_to("marketing/channels#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/marketing/channels/1/edit").to route_to("marketing/channels#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/marketing/channels").to route_to("marketing/channels#create")
    end

    it "routes to #update" do
      expect(:put => "/marketing/channels/1").to route_to("marketing/channels#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/marketing/channels/1").to route_to("marketing/channels#destroy", :id => "1")
    end

  end
end
