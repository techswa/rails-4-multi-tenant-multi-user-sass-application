require "rails_helper"

RSpec.describe Marketing::CampaignsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/marketing/campaigns").to route_to("marketing/campaigns#index")
    end

    it "routes to #new" do
      expect(:get => "/marketing/campaigns/new").to route_to("marketing/campaigns#new")
    end

    it "routes to #show" do
      expect(:get => "/marketing/campaigns/1").to route_to("marketing/campaigns#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/marketing/campaigns/1/edit").to route_to("marketing/campaigns#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/marketing/campaigns").to route_to("marketing/campaigns#create")
    end

    it "routes to #update" do
      expect(:put => "/marketing/campaigns/1").to route_to("marketing/campaigns#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/marketing/campaigns/1").to route_to("marketing/campaigns#destroy", :id => "1")
    end

  end
end
