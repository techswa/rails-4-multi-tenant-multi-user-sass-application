require "rails_helper"

RSpec.describe Statistics::DevicesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/statistics/devices").to route_to("statistics/devices#index")
    end

    it "routes to #new" do
      expect(:get => "/statistics/devices/new").to route_to("statistics/devices#new")
    end

    it "routes to #show" do
      expect(:get => "/statistics/devices/1").to route_to("statistics/devices#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/statistics/devices/1/edit").to route_to("statistics/devices#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/statistics/devices").to route_to("statistics/devices#create")
    end

    it "routes to #update" do
      expect(:put => "/statistics/devices/1").to route_to("statistics/devices#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/statistics/devices/1").to route_to("statistics/devices#destroy", :id => "1")
    end

  end
end
