require "rails_helper"

RSpec.describe Statistics::VisitsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/statistics/visits").to route_to("statistics/visits#index")
    end

    it "routes to #new" do
      expect(:get => "/statistics/visits/new").to route_to("statistics/visits#new")
    end

    it "routes to #show" do
      expect(:get => "/statistics/visits/1").to route_to("statistics/visits#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/statistics/visits/1/edit").to route_to("statistics/visits#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/statistics/visits").to route_to("statistics/visits#create")
    end

    it "routes to #update" do
      expect(:put => "/statistics/visits/1").to route_to("statistics/visits#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/statistics/visits/1").to route_to("statistics/visits#destroy", :id => "1")
    end

  end
end
