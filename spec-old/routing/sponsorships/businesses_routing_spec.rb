require "rails_helper"

RSpec.describe Sponsorships::BusinessesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/sponsorships/businesses").to route_to("sponsorships/businesses#index")
    end

    it "routes to #new" do
      expect(:get => "/sponsorships/businesses/new").to route_to("sponsorships/businesses#new")
    end

    it "routes to #show" do
      expect(:get => "/sponsorships/businesses/1").to route_to("sponsorships/businesses#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/sponsorships/businesses/1/edit").to route_to("sponsorships/businesses#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/sponsorships/businesses").to route_to("sponsorships/businesses#create")
    end

    it "routes to #update" do
      expect(:put => "/sponsorships/businesses/1").to route_to("sponsorships/businesses#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/sponsorships/businesses/1").to route_to("sponsorships/businesses#destroy", :id => "1")
    end

  end
end
