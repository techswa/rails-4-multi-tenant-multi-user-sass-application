require "rails_helper"

RSpec.describe Sponsorships::ConsumersController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/sponsorships/consumers").to route_to("sponsorships/consumers#index")
    end

    it "routes to #new" do
      expect(:get => "/sponsorships/consumers/new").to route_to("sponsorships/consumers#new")
    end

    it "routes to #show" do
      expect(:get => "/sponsorships/consumers/1").to route_to("sponsorships/consumers#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/sponsorships/consumers/1/edit").to route_to("sponsorships/consumers#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/sponsorships/consumers").to route_to("sponsorships/consumers#create")
    end

    it "routes to #update" do
      expect(:put => "/sponsorships/consumers/1").to route_to("sponsorships/consumers#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/sponsorships/consumers/1").to route_to("sponsorships/consumers#destroy", :id => "1")
    end

  end
end
