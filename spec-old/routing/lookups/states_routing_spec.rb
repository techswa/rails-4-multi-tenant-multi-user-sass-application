require "rails_helper"

RSpec.describe Lookups::StatesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/lookups/states").to route_to("lookups/states#index")
    end

    it "routes to #new" do
      expect(:get => "/lookups/states/new").to route_to("lookups/states#new")
    end

    it "routes to #show" do
      expect(:get => "/lookups/states/1").to route_to("lookups/states#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/lookups/states/1/edit").to route_to("lookups/states#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/lookups/states").to route_to("lookups/states#create")
    end

    it "routes to #update" do
      expect(:put => "/lookups/states/1").to route_to("lookups/states#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/lookups/states/1").to route_to("lookups/states#destroy", :id => "1")
    end

  end
end
