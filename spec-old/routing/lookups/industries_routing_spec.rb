require "rails_helper"

RSpec.describe Lookups::IndustriesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/lookups/industries").to route_to("lookups/industries#index")
    end

    it "routes to #new" do
      expect(:get => "/lookups/industries/new").to route_to("lookups/industries#new")
    end

    it "routes to #show" do
      expect(:get => "/lookups/industries/1").to route_to("lookups/industries#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/lookups/industries/1/edit").to route_to("lookups/industries#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/lookups/industries").to route_to("lookups/industries#create")
    end

    it "routes to #update" do
      expect(:put => "/lookups/industries/1").to route_to("lookups/industries#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/lookups/industries/1").to route_to("lookups/industries#destroy", :id => "1")
    end

  end
end
