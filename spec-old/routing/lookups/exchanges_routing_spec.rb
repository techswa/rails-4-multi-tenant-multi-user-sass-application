require "rails_helper"

RSpec.describe Lookups::ExchangesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/lookups/exchanges").to route_to("lookups/exchanges#index")
    end

    it "routes to #new" do
      expect(:get => "/lookups/exchanges/new").to route_to("lookups/exchanges#new")
    end

    it "routes to #show" do
      expect(:get => "/lookups/exchanges/1").to route_to("lookups/exchanges#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/lookups/exchanges/1/edit").to route_to("lookups/exchanges#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/lookups/exchanges").to route_to("lookups/exchanges#create")
    end

    it "routes to #update" do
      expect(:put => "/lookups/exchanges/1").to route_to("lookups/exchanges#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/lookups/exchanges/1").to route_to("lookups/exchanges#destroy", :id => "1")
    end

  end
end
