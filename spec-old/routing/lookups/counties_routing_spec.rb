require "rails_helper"

RSpec.describe Lookups::CountiesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/lookups/counties").to route_to("lookups/counties#index")
    end

    it "routes to #new" do
      expect(:get => "/lookups/counties/new").to route_to("lookups/counties#new")
    end

    it "routes to #show" do
      expect(:get => "/lookups/counties/1").to route_to("lookups/counties#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/lookups/counties/1/edit").to route_to("lookups/counties#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/lookups/counties").to route_to("lookups/counties#create")
    end

    it "routes to #update" do
      expect(:put => "/lookups/counties/1").to route_to("lookups/counties#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/lookups/counties/1").to route_to("lookups/counties#destroy", :id => "1")
    end

  end
end
