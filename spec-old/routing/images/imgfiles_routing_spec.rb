require "rails_helper"

RSpec.describe Images::ImgfilesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/images/imgfiles").to route_to("images/imgfiles#index")
    end

    it "routes to #new" do
      expect(:get => "/images/imgfiles/new").to route_to("images/imgfiles#new")
    end

    it "routes to #show" do
      expect(:get => "/images/imgfiles/1").to route_to("images/imgfiles#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/images/imgfiles/1/edit").to route_to("images/imgfiles#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/images/imgfiles").to route_to("images/imgfiles#create")
    end

    it "routes to #update" do
      expect(:put => "/images/imgfiles/1").to route_to("images/imgfiles#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/images/imgfiles/1").to route_to("images/imgfiles#destroy", :id => "1")
    end

  end
end
