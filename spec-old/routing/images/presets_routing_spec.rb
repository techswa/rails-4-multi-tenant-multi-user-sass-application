require "rails_helper"

RSpec.describe Images::PresetsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/images/presets").to route_to("images/presets#index")
    end

    it "routes to #new" do
      expect(:get => "/images/presets/new").to route_to("images/presets#new")
    end

    it "routes to #show" do
      expect(:get => "/images/presets/1").to route_to("images/presets#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/images/presets/1/edit").to route_to("images/presets#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/images/presets").to route_to("images/presets#create")
    end

    it "routes to #update" do
      expect(:put => "/images/presets/1").to route_to("images/presets#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/images/presets/1").to route_to("images/presets#destroy", :id => "1")
    end

  end
end
