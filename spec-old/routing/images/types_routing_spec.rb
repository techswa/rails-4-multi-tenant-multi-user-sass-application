require "rails_helper"

RSpec.describe Images::TypesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/images/types").to route_to("images/types#index")
    end

    it "routes to #new" do
      expect(:get => "/images/types/new").to route_to("images/types#new")
    end

    it "routes to #show" do
      expect(:get => "/images/types/1").to route_to("images/types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/images/types/1/edit").to route_to("images/types#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/images/types").to route_to("images/types#create")
    end

    it "routes to #update" do
      expect(:put => "/images/types/1").to route_to("images/types#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/images/types/1").to route_to("images/types#destroy", :id => "1")
    end

  end
end
