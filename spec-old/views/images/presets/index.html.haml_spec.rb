require 'rails_helper'

RSpec.describe "images/presets/index", :type => :view do
  before(:each) do
    assign(:images_presets, [
      Images::Preset.create!(
        :title => "Title",
        :description => "MyText",
        :symbol_name => "Symbol Name",
        :preview_width => 1,
        :preview_height => 2,
        :display_width => 3,
        :display_height => 4,
        :archived => false
      ),
      Images::Preset.create!(
        :title => "Title",
        :description => "MyText",
        :symbol_name => "Symbol Name",
        :preview_width => 1,
        :preview_height => 2,
        :display_width => 3,
        :display_height => 4,
        :archived => false
      )
    ])
  end

  it "renders a list of images/presets" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Symbol Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
