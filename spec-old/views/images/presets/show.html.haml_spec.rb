require 'rails_helper'

RSpec.describe "images/presets/show", :type => :view do
  before(:each) do
    @images_preset = assign(:images_preset, Images::Preset.create!(
      :title => "Title",
      :description => "MyText",
      :symbol_name => "Symbol Name",
      :preview_width => 1,
      :preview_height => 2,
      :display_width => 3,
      :display_height => 4,
      :archived => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Symbol Name/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/false/)
  end
end
