require 'rails_helper'

RSpec.describe "images/presets/new", :type => :view do
  before(:each) do
    assign(:images_preset, Images::Preset.new(
      :title => "MyString",
      :description => "MyText",
      :symbol_name => "MyString",
      :preview_width => 1,
      :preview_height => 1,
      :display_width => 1,
      :display_height => 1,
      :archived => false
    ))
  end

  it "renders new images_preset form" do
    render

    assert_select "form[action=?][method=?]", images_presets_path, "post" do

      assert_select "input#images_preset_title[name=?]", "images_preset[title]"

      assert_select "textarea#images_preset_description[name=?]", "images_preset[description]"

      assert_select "input#images_preset_symbol_name[name=?]", "images_preset[symbol_name]"

      assert_select "input#images_preset_preview_width[name=?]", "images_preset[preview_width]"

      assert_select "input#images_preset_preview_height[name=?]", "images_preset[preview_height]"

      assert_select "input#images_preset_display_width[name=?]", "images_preset[display_width]"

      assert_select "input#images_preset_display_height[name=?]", "images_preset[display_height]"

      assert_select "input#images_preset_archived[name=?]", "images_preset[archived]"
    end
  end
end
