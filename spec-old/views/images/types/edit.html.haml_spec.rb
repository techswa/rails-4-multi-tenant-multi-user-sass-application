require 'rails_helper'

RSpec.describe "images/types/edit", :type => :view do
  before(:each) do
    @images_type = assign(:images_type, Images::Type.create!(
      :title => "MyString",
      :description => "MyText",
      :symbol_name => "MyString",
      :images_preset_id => "",
      :archived => false
    ))
  end

  it "renders the edit images_type form" do
    render

    assert_select "form[action=?][method=?]", images_type_path(@images_type), "post" do

      assert_select "input#images_type_title[name=?]", "images_type[title]"

      assert_select "textarea#images_type_description[name=?]", "images_type[description]"

      assert_select "input#images_type_symbol_name[name=?]", "images_type[symbol_name]"

      assert_select "input#images_type_images_preset_id[name=?]", "images_type[images_preset_id]"

      assert_select "input#images_type_archived[name=?]", "images_type[archived]"
    end
  end
end
