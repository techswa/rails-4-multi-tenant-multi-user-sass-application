require 'rails_helper'

RSpec.describe "images/types/show", :type => :view do
  before(:each) do
    @images_type = assign(:images_type, Images::Type.create!(
      :title => "Title",
      :description => "MyText",
      :symbol_name => "Symbol Name",
      :images_preset_id => "",
      :archived => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Symbol Name/)
    expect(rendered).to match(//)
    expect(rendered).to match(/false/)
  end
end
