require 'rails_helper'

RSpec.describe "images/types/index", :type => :view do
  before(:each) do
    assign(:images_types, [
      Images::Type.create!(
        :title => "Title",
        :description => "MyText",
        :symbol_name => "Symbol Name",
        :images_preset_id => "",
        :archived => false
      ),
      Images::Type.create!(
        :title => "Title",
        :description => "MyText",
        :symbol_name => "Symbol Name",
        :images_preset_id => "",
        :archived => false
      )
    ])
  end

  it "renders a list of images/types" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Symbol Name".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
