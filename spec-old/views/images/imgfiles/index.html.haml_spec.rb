require 'rails_helper'

RSpec.describe "images/imgfiles/index", :type => :view do
  before(:each) do
    assign(:images_imgfiles, [
      Images::Imgfile.create!(
        :title => "Title",
        :description => "MyText",
        :sponsorships_sponsor_id => "",
        :images_type_id => "",
        :file_file_name => "File File Name",
        :file_content_type => "File Content Type",
        :file_file_size => 1,
        :archived => false
      ),
      Images::Imgfile.create!(
        :title => "Title",
        :description => "MyText",
        :sponsorships_sponsor_id => "",
        :images_type_id => "",
        :file_file_name => "File File Name",
        :file_content_type => "File Content Type",
        :file_file_size => 1,
        :archived => false
      )
    ])
  end

  it "renders a list of images/imgfiles" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "File File Name".to_s, :count => 2
    assert_select "tr>td", :text => "File Content Type".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
