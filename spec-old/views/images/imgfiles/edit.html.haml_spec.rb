require 'rails_helper'

RSpec.describe "images/imgfiles/edit", :type => :view do
  before(:each) do
    @images_imgfile = assign(:images_imgfile, Images::Imgfile.create!(
      :title => "MyString",
      :description => "MyText",
      :sponsorships_sponsor_id => "",
      :images_type_id => "",
      :file_file_name => "MyString",
      :file_content_type => "MyString",
      :file_file_size => 1,
      :archived => false
    ))
  end

  it "renders the edit images_imgfile form" do
    render

    assert_select "form[action=?][method=?]", images_imgfile_path(@images_imgfile), "post" do

      assert_select "input#images_imgfile_title[name=?]", "images_imgfile[title]"

      assert_select "textarea#images_imgfile_description[name=?]", "images_imgfile[description]"

      assert_select "input#images_imgfile_sponsorships_sponsor_id[name=?]", "images_imgfile[sponsorships_sponsor_id]"

      assert_select "input#images_imgfile_images_type_id[name=?]", "images_imgfile[images_type_id]"

      assert_select "input#images_imgfile_file_file_name[name=?]", "images_imgfile[file_file_name]"

      assert_select "input#images_imgfile_file_content_type[name=?]", "images_imgfile[file_content_type]"

      assert_select "input#images_imgfile_file_file_size[name=?]", "images_imgfile[file_file_size]"

      assert_select "input#images_imgfile_archived[name=?]", "images_imgfile[archived]"
    end
  end
end
