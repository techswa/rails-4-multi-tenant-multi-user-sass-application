require 'rails_helper'

RSpec.describe "images/imgfiles/show", :type => :view do
  before(:each) do
    @images_imgfile = assign(:images_imgfile, Images::Imgfile.create!(
      :title => "Title",
      :description => "MyText",
      :sponsorships_sponsor_id => "",
      :images_type_id => "",
      :file_file_name => "File File Name",
      :file_content_type => "File Content Type",
      :file_file_size => 1,
      :archived => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/File File Name/)
    expect(rendered).to match(/File Content Type/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/false/)
  end
end
