require 'rails_helper'

RSpec.describe "tests/new", :type => :view do
  before(:each) do
    assign(:tests, Test.new(
      :name => "MyString",
      :color => "MyString"
    ))
  end

  it "renders new test form" do
    render

    assert_select "form[action=?][method=?]", tests_path, "post" do

      assert_select "input#test_name[name=?]", "test[name]"

      assert_select "input#test_color[name=?]", "test[color]"
    end
  end
end
