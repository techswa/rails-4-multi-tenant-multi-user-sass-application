require 'rails_helper'

RSpec.describe "marketing/channels/show", :type => :view do
  before(:each) do
    @marketing_channel = assign(:marketing_channel, Marketing::Channel.create!(
      :sponsors_sponsor_id => "",
      :title => "Title",
      :description => "MyText",
      :archived => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
  end
end
