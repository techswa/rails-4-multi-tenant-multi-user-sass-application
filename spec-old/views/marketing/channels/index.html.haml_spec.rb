require 'rails_helper'

RSpec.describe "marketing/channels/index", :type => :view do
  before(:each) do
    assign(:marketing_channels, [
      Marketing::Channel.create!(
        :sponsors_sponsor_id => "",
        :title => "Title",
        :description => "MyText",
        :archived => false
      ),
      Marketing::Channel.create!(
        :sponsors_sponsor_id => "",
        :title => "Title",
        :description => "MyText",
        :archived => false
      )
    ])
  end

  it "renders a list of marketing/channels" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
