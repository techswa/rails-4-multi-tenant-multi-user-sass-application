require 'rails_helper'

RSpec.describe "marketing/channels/edit", :type => :view do
  before(:each) do
    @marketing_channel = assign(:marketing_channel, Marketing::Channel.create!(
      :sponsors_sponsor_id => "",
      :title => "MyString",
      :description => "MyText",
      :archived => false
    ))
  end

  it "renders the edit marketing_channel form" do
    render

    assert_select "form[action=?][method=?]", marketing_channel_path(@marketing_channel), "post" do

      assert_select "input#marketing_channel_sponsors_sponsor_id[name=?]", "marketing_channel[sponsors_sponsor_id]"

      assert_select "input#marketing_channel_title[name=?]", "marketing_channel[title]"

      assert_select "textarea#marketing_channel_description[name=?]", "marketing_channel[description]"

      assert_select "input#marketing_channel_archived[name=?]", "marketing_channel[archived]"
    end
  end
end
