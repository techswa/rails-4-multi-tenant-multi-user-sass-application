require 'rails_helper'

RSpec.describe "marketing/campaigns/show", :type => :view do
  before(:each) do
    @marketing_campaign = assign(:marketing_campaign, Marketing::Campaign.create!(
      :sponsor_id => "",
      :title => "Title",
      :description => "MyText",
      :sent => 1,
      :responses => 2,
      :signups => 3,
      :notes => "MyText",
      :archived => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
  end
end
