require 'rails_helper'

RSpec.describe "marketing/campaigns/edit", :type => :view do
  before(:each) do
    @marketing_campaign = assign(:marketing_campaign, Marketing::Campaign.create!(
      :sponsor_id => "",
      :title => "MyString",
      :description => "MyText",
      :sent => 1,
      :responses => 1,
      :signups => 1,
      :notes => "MyText",
      :archived => false
    ))
  end

  it "renders the edit marketing_campaign form" do
    render

    assert_select "form[action=?][method=?]", marketing_campaign_path(@marketing_campaign), "post" do

      assert_select "input#marketing_campaign_sponsor_id[name=?]", "marketing_campaign[sponsor_id]"

      assert_select "input#marketing_campaign_title[name=?]", "marketing_campaign[title]"

      assert_select "textarea#marketing_campaign_description[name=?]", "marketing_campaign[description]"

      assert_select "input#marketing_campaign_sent[name=?]", "marketing_campaign[sent]"

      assert_select "input#marketing_campaign_responses[name=?]", "marketing_campaign[responses]"

      assert_select "input#marketing_campaign_signups[name=?]", "marketing_campaign[signups]"

      assert_select "textarea#marketing_campaign_notes[name=?]", "marketing_campaign[notes]"

      assert_select "input#marketing_campaign_archived[name=?]", "marketing_campaign[archived]"
    end
  end
end
