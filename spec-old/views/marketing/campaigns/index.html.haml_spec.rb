require 'rails_helper'

RSpec.describe "marketing/campaigns/index", :type => :view do
  before(:each) do
    assign(:marketing_campaigns, [
      Marketing::Campaign.create!(
        :sponsor_id => "",
        :title => "Title",
        :description => "MyText",
        :sent => 1,
        :responses => 2,
        :signups => 3,
        :notes => "MyText",
        :archived => false
      ),
      Marketing::Campaign.create!(
        :sponsor_id => "",
        :title => "Title",
        :description => "MyText",
        :sent => 1,
        :responses => 2,
        :signups => 3,
        :notes => "MyText",
        :archived => false
      )
    ])
  end

  it "renders a list of marketing/campaigns" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
