require 'rails_helper'

RSpec.describe "sponsors/portals/new", :type => :view do
  before(:each) do
    assign(:sponsors_portal, Sponsors::Portal.new(
      :sponsor_id => "",
      :portal_setting_id => "",
      :disabled => false
    ))
  end

  it "renders new sponsors_portal form" do
    render

    assert_select "form[action=?][method=?]", sponsors_portals_path, "post" do

      assert_select "input#sponsors_portal_sponsor_id[name=?]", "sponsors_portal[sponsor_id]"

      assert_select "input#sponsors_portal_portal_setting_id[name=?]", "sponsors_portal[portal_setting_id]"

      assert_select "input#sponsors_portal_disabled[name=?]", "sponsors_portal[disabled]"
    end
  end
end
