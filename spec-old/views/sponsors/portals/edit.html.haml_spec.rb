require 'rails_helper'

RSpec.describe "sponsors/portals/edit", :type => :view do
  before(:each) do
    @sponsors_portal = assign(:sponsors_portal, Sponsors::Portal.create!(
      :sponsor_id => "",
      :portal_setting_id => "",
      :disabled => false
    ))
  end

  it "renders the edit sponsors_portal form" do
    render

    assert_select "form[action=?][method=?]", sponsors_portal_path(@sponsors_portal), "post" do

      assert_select "input#sponsors_portal_sponsor_id[name=?]", "sponsors_portal[sponsor_id]"

      assert_select "input#sponsors_portal_portal_setting_id[name=?]", "sponsors_portal[portal_setting_id]"

      assert_select "input#sponsors_portal_disabled[name=?]", "sponsors_portal[disabled]"
    end
  end
end
