require 'rails_helper'

RSpec.describe "sponsors/portals/show", :type => :view do
  before(:each) do
    @sponsors_portal = assign(:sponsors_portal, Sponsors::Portal.create!(
      :sponsor_id => "",
      :portal_setting_id => "",
      :disabled => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/false/)
  end
end
