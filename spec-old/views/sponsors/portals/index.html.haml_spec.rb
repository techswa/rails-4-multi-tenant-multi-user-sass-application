require 'rails_helper'

RSpec.describe "sponsors/portals/index", :type => :view do
  before(:each) do
    assign(:sponsors_portals, [
      Sponsors::Portal.create!(
        :sponsor_id => "",
        :portal_setting_id => "",
        :disabled => false
      ),
      Sponsors::Portal.create!(
        :sponsor_id => "",
        :portal_setting_id => "",
        :disabled => false
      )
    ])
  end

  it "renders a list of sponsors/portals" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
