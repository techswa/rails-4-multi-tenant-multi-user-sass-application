require 'rails_helper'

RSpec.describe "sponsors/show", :type => :view do
  before(:each) do
    @sponsor = assign(:sponsor, Sponsor.create!(
      :campaign_id => "",
      :lookups_industry_id => "",
      :sponsors_type_id => "",
      :license_number => "License Number",
      :license_email => "License Email",
      :business_name => "Business Name",
      :first_name => "First Name",
      :last_name => "Last Name",
      :primary_phone => "Primary Phone",
      :address_1 => "Address 1",
      :address_2 => "Address 2",
      :city => "City",
      :lookups_state_id => "",
      :zipcode => "Zipcode",
      :website => "Website",
      :notes => "Notes"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/License Number/)
    expect(rendered).to match(/License Email/)
    expect(rendered).to match(/Business Name/)
    expect(rendered).to match(/First Name/)
    expect(rendered).to match(/Last Name/)
    expect(rendered).to match(/Primary Phone/)
    expect(rendered).to match(/Address 1/)
    expect(rendered).to match(/Address 2/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(//)
    expect(rendered).to match(/Zipcode/)
    expect(rendered).to match(/Website/)
    expect(rendered).to match(/Notes/)
  end
end
