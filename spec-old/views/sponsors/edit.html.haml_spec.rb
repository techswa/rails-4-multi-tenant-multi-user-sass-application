require 'rails_helper'

RSpec.describe "sponsors/edit", :type => :view do
  before(:each) do
    @sponsor = assign(:sponsor, Sponsor.create!(
      :campaign_id => "",
      :lookups_industry_id => "",
      :sponsors_type_id => "",
      :license_number => "MyString",
      :license_email => "MyString",
      :business_name => "MyString",
      :first_name => "MyString",
      :last_name => "MyString",
      :primary_phone => "MyString",
      :address_1 => "MyString",
      :address_2 => "MyString",
      :city => "MyString",
      :lookups_state_id => "",
      :zipcode => "MyString",
      :website => "MyString",
      :notes => "MyString"
    ))
  end

  it "renders the edit sponsor form" do
    render

    assert_select "form[action=?][method=?]", sponsor_path(@sponsor), "post" do

      assert_select "input#sponsor_campaign_id[name=?]", "sponsor[campaign_id]"

      assert_select "input#sponsor_lookups_industry_id[name=?]", "sponsor[lookups_industry_id]"

      assert_select "input#sponsor_sponsors_type_id[name=?]", "sponsor[sponsors_type_id]"

      assert_select "input#sponsor_license_number[name=?]", "sponsor[license_number]"

      assert_select "input#sponsor_license_email[name=?]", "sponsor[license_email]"

      assert_select "input#sponsor_business_name[name=?]", "sponsor[business_name]"

      assert_select "input#sponsor_first_name[name=?]", "sponsor[first_name]"

      assert_select "input#sponsor_last_name[name=?]", "sponsor[last_name]"

      assert_select "input#sponsor_primary_phone[name=?]", "sponsor[primary_phone]"

      assert_select "input#sponsor_address_1[name=?]", "sponsor[address_1]"

      assert_select "input#sponsor_address_2[name=?]", "sponsor[address_2]"

      assert_select "input#sponsor_city[name=?]", "sponsor[city]"

      assert_select "input#sponsor_lookups_state_id[name=?]", "sponsor[lookups_state_id]"

      assert_select "input#sponsor_zipcode[name=?]", "sponsor[zipcode]"

      assert_select "input#sponsor_website[name=?]", "sponsor[website]"

      assert_select "input#sponsor_notes[name=?]", "sponsor[notes]"
    end
  end
end
