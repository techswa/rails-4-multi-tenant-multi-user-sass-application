require 'rails_helper'

RSpec.describe "sponsors/index", :type => :view do
  before(:each) do
    assign(:sponsors, [
      Sponsor.create!(
        :campaign_id => "",
        :lookups_industry_id => "",
        :sponsors_type_id => "",
        :license_number => "License Number",
        :license_email => "License Email",
        :business_name => "Business Name",
        :first_name => "First Name",
        :last_name => "Last Name",
        :primary_phone => "Primary Phone",
        :address_1 => "Address 1",
        :address_2 => "Address 2",
        :city => "City",
        :lookups_state_id => "",
        :zipcode => "Zipcode",
        :website => "Website",
        :notes => "Notes"
      ),
      Sponsor.create!(
        :campaign_id => "",
        :lookups_industry_id => "",
        :sponsors_type_id => "",
        :license_number => "License Number",
        :license_email => "License Email",
        :business_name => "Business Name",
        :first_name => "First Name",
        :last_name => "Last Name",
        :primary_phone => "Primary Phone",
        :address_1 => "Address 1",
        :address_2 => "Address 2",
        :city => "City",
        :lookups_state_id => "",
        :zipcode => "Zipcode",
        :website => "Website",
        :notes => "Notes"
      )
    ])
  end

  it "renders a list of sponsors" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "License Number".to_s, :count => 2
    assert_select "tr>td", :text => "License Email".to_s, :count => 2
    assert_select "tr>td", :text => "Business Name".to_s, :count => 2
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "Primary Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Address 1".to_s, :count => 2
    assert_select "tr>td", :text => "Address 2".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Zipcode".to_s, :count => 2
    assert_select "tr>td", :text => "Website".to_s, :count => 2
    assert_select "tr>td", :text => "Notes".to_s, :count => 2
  end
end
