require 'rails_helper'

RSpec.describe "sponsors/types/index", :type => :view do
  before(:each) do
    assign(:sponsors_types, [
      Sponsors::Type.create!(
        :title => "Title",
        :description => "MyText",
        :symbol_name => "Symbol Name",
        :archived => false
      ),
      Sponsors::Type.create!(
        :title => "Title",
        :description => "MyText",
        :symbol_name => "Symbol Name",
        :archived => false
      )
    ])
  end

  it "renders a list of sponsors/types" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Symbol Name".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
