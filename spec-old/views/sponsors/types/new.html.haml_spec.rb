require 'rails_helper'

RSpec.describe "sponsors/types/new", :type => :view do
  before(:each) do
    assign(:sponsors_type, Sponsors::Type.new(
      :title => "MyString",
      :description => "MyText",
      :symbol_name => "MyString",
      :archived => false
    ))
  end

  it "renders new sponsors_type form" do
    render

    assert_select "form[action=?][method=?]", sponsors_types_path, "post" do

      assert_select "input#sponsors_type_title[name=?]", "sponsors_type[title]"

      assert_select "textarea#sponsors_type_description[name=?]", "sponsors_type[description]"

      assert_select "input#sponsors_type_symbol_name[name=?]", "sponsors_type[symbol_name]"

      assert_select "input#sponsors_type_archived[name=?]", "sponsors_type[archived]"
    end
  end
end
