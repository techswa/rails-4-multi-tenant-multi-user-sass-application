require 'rails_helper'

RSpec.describe "accounting/payment_types/edit", :type => :view do
  before(:each) do
    @accounting_payment_type = assign(:accounting_payment_type, Accounting::PaymentType.create!(
      :name => "MyString",
      :sort => 1
    ))
  end

  it "renders the edit accounting_payment_type form" do
    render

    assert_select "form[action=?][method=?]", accounting_payment_type_path(@accounting_payment_type), "post" do

      assert_select "input#accounting_payment_type_name[name=?]", "accounting_payment_type[name]"

      assert_select "input#accounting_payment_type_sort[name=?]", "accounting_payment_type[sort]"
    end
  end
end
