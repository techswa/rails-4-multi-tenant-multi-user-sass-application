require 'rails_helper'

RSpec.describe "accounting/payment_types/new", :type => :view do
  before(:each) do
    assign(:accounting_payment_type, Accounting::PaymentType.new(
      :name => "MyString",
      :sort => 1
    ))
  end

  it "renders new accounting_payment_type form" do
    render

    assert_select "form[action=?][method=?]", accounting_payment_types_path, "post" do

      assert_select "input#accounting_payment_type_name[name=?]", "accounting_payment_type[name]"

      assert_select "input#accounting_payment_type_sort[name=?]", "accounting_payment_type[sort]"
    end
  end
end
