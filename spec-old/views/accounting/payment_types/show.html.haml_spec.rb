require 'rails_helper'

RSpec.describe "accounting/payment_types/show", :type => :view do
  before(:each) do
    @accounting_payment_type = assign(:accounting_payment_type, Accounting::PaymentType.create!(
      :name => "Name",
      :sort => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/1/)
  end
end
