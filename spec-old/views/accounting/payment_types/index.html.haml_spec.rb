require 'rails_helper'

RSpec.describe "accounting/payment_types/index", :type => :view do
  before(:each) do
    assign(:accounting_payment_types, [
      Accounting::PaymentType.create!(
        :name => "Name",
        :sort => 1
      ),
      Accounting::PaymentType.create!(
        :name => "Name",
        :sort => 1
      )
    ])
  end

  it "renders a list of accounting/payment_types" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
