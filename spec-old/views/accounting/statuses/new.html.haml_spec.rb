require 'rails_helper'

RSpec.describe "accounting/statuses/new", :type => :view do
  before(:each) do
    assign(:accounting_status, Accounting::Status.new(
      :sponsors_sponsor_id => "",
      :suspended => false
    ))
  end

  it "renders new accounting_status form" do
    render

    assert_select "form[action=?][method=?]", accounting_statuses_path, "post" do

      assert_select "input#accounting_status_sponsors_sponsor_id[name=?]", "accounting_status[sponsors_sponsor_id]"

      assert_select "input#accounting_status_suspended[name=?]", "accounting_status[suspended]"
    end
  end
end
