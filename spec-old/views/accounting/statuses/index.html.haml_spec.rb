require 'rails_helper'

RSpec.describe "accounting/statuses/index", :type => :view do
  before(:each) do
    assign(:accounting_statuses, [
      Accounting::Status.create!(
        :sponsors_sponsor_id => "",
        :suspended => false
      ),
      Accounting::Status.create!(
        :sponsors_sponsor_id => "",
        :suspended => false
      )
    ])
  end

  it "renders a list of accounting/statuses" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
