require 'rails_helper'

RSpec.describe "accounting/statuses/show", :type => :view do
  before(:each) do
    @accounting_status = assign(:accounting_status, Accounting::Status.create!(
      :sponsors_sponsor_id => "",
      :suspended => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/false/)
  end
end
