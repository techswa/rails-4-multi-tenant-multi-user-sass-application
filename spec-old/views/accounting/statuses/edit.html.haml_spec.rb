require 'rails_helper'

RSpec.describe "accounting/statuses/edit", :type => :view do
  before(:each) do
    @accounting_status = assign(:accounting_status, Accounting::Status.create!(
      :sponsors_sponsor_id => "",
      :suspended => false
    ))
  end

  it "renders the edit accounting_status form" do
    render

    assert_select "form[action=?][method=?]", accounting_status_path(@accounting_status), "post" do

      assert_select "input#accounting_status_sponsors_sponsor_id[name=?]", "accounting_status[sponsors_sponsor_id]"

      assert_select "input#accounting_status_suspended[name=?]", "accounting_status[suspended]"
    end
  end
end
