require 'rails_helper'

RSpec.describe "accounting/payments/index", :type => :view do
  before(:each) do
    assign(:accounting_payments, [
      Accounting::Payment.create!(
        :sponsor_id => "",
        :accounting_payment_type_id => "",
        :amount => 1,
        :staff_id => "",
        :note => "MyText"
      ),
      Accounting::Payment.create!(
        :sponsor_id => "",
        :accounting_payment_type_id => "",
        :amount => 1,
        :staff_id => "",
        :note => "MyText"
      )
    ])
  end

  it "renders a list of accounting/payments" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
