require 'rails_helper'

RSpec.describe "accounting/payments/show", :type => :view do
  before(:each) do
    @accounting_payment = assign(:accounting_payment, Accounting::Payment.create!(
      :sponsor_id => "",
      :accounting_payment_type_id => "",
      :amount => 1,
      :staff_id => "",
      :note => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/1/)
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
  end
end
