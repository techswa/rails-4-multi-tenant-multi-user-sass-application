require 'rails_helper'

RSpec.describe "accounting/payments/edit", :type => :view do
  before(:each) do
    @accounting_payment = assign(:accounting_payment, Accounting::Payment.create!(
      :sponsor_id => "",
      :accounting_payment_type_id => "",
      :amount => 1,
      :staff_id => "",
      :note => "MyText"
    ))
  end

  it "renders the edit accounting_payment form" do
    render

    assert_select "form[action=?][method=?]", accounting_payment_path(@accounting_payment), "post" do

      assert_select "input#accounting_payment_sponsor_id[name=?]", "accounting_payment[sponsor_id]"

      assert_select "input#accounting_payment_accounting_payment_type_id[name=?]", "accounting_payment[accounting_payment_type_id]"

      assert_select "input#accounting_payment_amount[name=?]", "accounting_payment[amount]"

      assert_select "input#accounting_payment_staff_id[name=?]", "accounting_payment[staff_id]"

      assert_select "textarea#accounting_payment_note[name=?]", "accounting_payment[note]"
    end
  end
end
