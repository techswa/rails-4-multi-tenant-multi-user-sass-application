require 'rails_helper'

RSpec.describe "lookups/counties/new", :type => :view do
  before(:each) do
    assign(:lookups_county, Lookups::County.new(
      :full_name => "MyString",
      :fips_county => "MyString",
      :fips_state => "MyString"
    ))
  end

  it "renders new lookups_county form" do
    render

    assert_select "form[action=?][method=?]", lookups_counties_path, "post" do

      assert_select "input#lookups_county_full_name[name=?]", "lookups_county[full_name]"

      assert_select "input#lookups_county_fips_county[name=?]", "lookups_county[fips_county]"

      assert_select "input#lookups_county_fips_state[name=?]", "lookups_county[fips_state]"
    end
  end
end
