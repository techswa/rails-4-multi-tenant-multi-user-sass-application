require 'rails_helper'

RSpec.describe "lookups/counties/edit", :type => :view do
  before(:each) do
    @lookups_county = assign(:lookups_county, Lookups::County.create!(
      :full_name => "MyString",
      :fips_county => "MyString",
      :fips_state => "MyString"
    ))
  end

  it "renders the edit lookups_county form" do
    render

    assert_select "form[action=?][method=?]", lookups_county_path(@lookups_county), "post" do

      assert_select "input#lookups_county_full_name[name=?]", "lookups_county[full_name]"

      assert_select "input#lookups_county_fips_county[name=?]", "lookups_county[fips_county]"

      assert_select "input#lookups_county_fips_state[name=?]", "lookups_county[fips_state]"
    end
  end
end
