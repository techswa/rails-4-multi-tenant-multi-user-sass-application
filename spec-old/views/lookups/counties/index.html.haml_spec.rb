require 'rails_helper'

RSpec.describe "lookups/counties/index", :type => :view do
  before(:each) do
    assign(:lookups_counties, [
      Lookups::County.create!(
        :full_name => "Full Name",
        :fips_county => "Fips County",
        :fips_state => "Fips State"
      ),
      Lookups::County.create!(
        :full_name => "Full Name",
        :fips_county => "Fips County",
        :fips_state => "Fips State"
      )
    ])
  end

  it "renders a list of lookups/counties" do
    render
    assert_select "tr>td", :text => "Full Name".to_s, :count => 2
    assert_select "tr>td", :text => "Fips County".to_s, :count => 2
    assert_select "tr>td", :text => "Fips State".to_s, :count => 2
  end
end
