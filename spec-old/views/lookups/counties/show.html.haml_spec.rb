require 'rails_helper'

RSpec.describe "lookups/counties/show", :type => :view do
  before(:each) do
    @lookups_county = assign(:lookups_county, Lookups::County.create!(
      :full_name => "Full Name",
      :fips_county => "Fips County",
      :fips_state => "Fips State"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Full Name/)
    expect(rendered).to match(/Fips County/)
    expect(rendered).to match(/Fips State/)
  end
end
