require 'rails_helper'

RSpec.describe "lookups/industries/new", :type => :view do
  before(:each) do
    assign(:lookups_industry, Lookups::Industry.new(
      :naics_code => "MyString",
      :title => "MyString",
      :note => "MyText"
    ))
  end

  it "renders new lookups_industry form" do
    render

    assert_select "form[action=?][method=?]", lookups_industries_path, "post" do

      assert_select "input#lookups_industry_naics_code[name=?]", "lookups_industry[naics_code]"

      assert_select "input#lookups_industry_title[name=?]", "lookups_industry[title]"

      assert_select "textarea#lookups_industry_note[name=?]", "lookups_industry[note]"
    end
  end
end
