require 'rails_helper'

RSpec.describe "lookups/industries/index", :type => :view do
  before(:each) do
    assign(:lookups_industries, [
      Lookups::Industry.create!(
        :naics_code => "Naics Code",
        :title => "Title",
        :note => "MyText"
      ),
      Lookups::Industry.create!(
        :naics_code => "Naics Code",
        :title => "Title",
        :note => "MyText"
      )
    ])
  end

  it "renders a list of lookups/industries" do
    render
    assert_select "tr>td", :text => "Naics Code".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
