require 'rails_helper'

RSpec.describe "lookups/industries/show", :type => :view do
  before(:each) do
    @lookups_industry = assign(:lookups_industry, Lookups::Industry.create!(
      :naics_code => "Naics Code",
      :title => "Title",
      :note => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Naics Code/)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
  end
end
