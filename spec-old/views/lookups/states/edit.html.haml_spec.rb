require 'rails_helper'

RSpec.describe "lookups/states/edit", :type => :view do
  before(:each) do
    @lookups_state = assign(:lookups_state, Lookups::State.create!(
      :fips_state => "MyString",
      :full_name => "MyString",
      :abbreviated_name => "MyString"
    ))
  end

  it "renders the edit lookups_state form" do
    render

    assert_select "form[action=?][method=?]", lookups_state_path(@lookups_state), "post" do

      assert_select "input#lookups_state_fips_state[name=?]", "lookups_state[fips_state]"

      assert_select "input#lookups_state_full_name[name=?]", "lookups_state[full_name]"

      assert_select "input#lookups_state_abbreviated_name[name=?]", "lookups_state[abbreviated_name]"
    end
  end
end
