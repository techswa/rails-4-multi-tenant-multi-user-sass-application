require 'rails_helper'

RSpec.describe "lookups/states/show", :type => :view do
  before(:each) do
    @lookups_state = assign(:lookups_state, Lookups::State.create!(
      :fips_state => "Fips State",
      :full_name => "Full Name",
      :abbreviated_name => "Abbreviated Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Fips State/)
    expect(rendered).to match(/Full Name/)
    expect(rendered).to match(/Abbreviated Name/)
  end
end
