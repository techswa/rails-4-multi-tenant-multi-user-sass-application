require 'rails_helper'

RSpec.describe "lookups/states/index", :type => :view do
  before(:each) do
    assign(:lookups_states, [
      Lookups::State.create!(
        :fips_state => "Fips State",
        :full_name => "Full Name",
        :abbreviated_name => "Abbreviated Name"
      ),
      Lookups::State.create!(
        :fips_state => "Fips State",
        :full_name => "Full Name",
        :abbreviated_name => "Abbreviated Name"
      )
    ])
  end

  it "renders a list of lookups/states" do
    render
    assert_select "tr>td", :text => "Fips State".to_s, :count => 2
    assert_select "tr>td", :text => "Full Name".to_s, :count => 2
    assert_select "tr>td", :text => "Abbreviated Name".to_s, :count => 2
  end
end
