require 'rails_helper'

RSpec.describe "lookups/states/new", :type => :view do
  before(:each) do
    assign(:lookups_state, Lookups::State.new(
      :fips_state => "MyString",
      :full_name => "MyString",
      :abbreviated_name => "MyString"
    ))
  end

  it "renders new lookups_state form" do
    render

    assert_select "form[action=?][method=?]", lookups_states_path, "post" do

      assert_select "input#lookups_state_fips_state[name=?]", "lookups_state[fips_state]"

      assert_select "input#lookups_state_full_name[name=?]", "lookups_state[full_name]"

      assert_select "input#lookups_state_abbreviated_name[name=?]", "lookups_state[abbreviated_name]"
    end
  end
end
