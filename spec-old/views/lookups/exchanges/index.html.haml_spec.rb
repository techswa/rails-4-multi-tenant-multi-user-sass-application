require 'rails_helper'

RSpec.describe "lookups/exchanges/index", :type => :view do
  before(:each) do
    assign(:lookups_exchanges, [
      Lookups::Exchange.create!(
        :exchange_name => "Exchange Name",
        :exchange_abbreviation => "Exchange Abbreviation",
        :city => "City",
        :lookups_state_id => ""
      ),
      Lookups::Exchange.create!(
        :exchange_name => "Exchange Name",
        :exchange_abbreviation => "Exchange Abbreviation",
        :city => "City",
        :lookups_state_id => ""
      )
    ])
  end

  it "renders a list of lookups/exchanges" do
    render
    assert_select "tr>td", :text => "Exchange Name".to_s, :count => 2
    assert_select "tr>td", :text => "Exchange Abbreviation".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
