require 'rails_helper'

RSpec.describe "lookups/exchanges/show", :type => :view do
  before(:each) do
    @lookups_exchange = assign(:lookups_exchange, Lookups::Exchange.create!(
      :exchange_name => "Exchange Name",
      :exchange_abbreviation => "Exchange Abbreviation",
      :city => "City",
      :lookups_state_id => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Exchange Name/)
    expect(rendered).to match(/Exchange Abbreviation/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(//)
  end
end
