require 'rails_helper'

RSpec.describe "lookups/exchanges/new", :type => :view do
  before(:each) do
    assign(:lookups_exchange, Lookups::Exchange.new(
      :exchange_name => "MyString",
      :exchange_abbreviation => "MyString",
      :city => "MyString",
      :lookups_state_id => ""
    ))
  end

  it "renders new lookups_exchange form" do
    render

    assert_select "form[action=?][method=?]", lookups_exchanges_path, "post" do

      assert_select "input#lookups_exchange_exchange_name[name=?]", "lookups_exchange[exchange_name]"

      assert_select "input#lookups_exchange_exchange_abbreviation[name=?]", "lookups_exchange[exchange_abbreviation]"

      assert_select "input#lookups_exchange_city[name=?]", "lookups_exchange[city]"

      assert_select "input#lookups_exchange_lookups_state_id[name=?]", "lookups_exchange[lookups_state_id]"
    end
  end
end
