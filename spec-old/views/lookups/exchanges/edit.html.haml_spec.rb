require 'rails_helper'

RSpec.describe "lookups/exchanges/edit", :type => :view do
  before(:each) do
    @lookups_exchange = assign(:lookups_exchange, Lookups::Exchange.create!(
      :exchange_name => "MyString",
      :exchange_abbreviation => "MyString",
      :city => "MyString",
      :lookups_state_id => ""
    ))
  end

  it "renders the edit lookups_exchange form" do
    render

    assert_select "form[action=?][method=?]", lookups_exchange_path(@lookups_exchange), "post" do

      assert_select "input#lookups_exchange_exchange_name[name=?]", "lookups_exchange[exchange_name]"

      assert_select "input#lookups_exchange_exchange_abbreviation[name=?]", "lookups_exchange[exchange_abbreviation]"

      assert_select "input#lookups_exchange_city[name=?]", "lookups_exchange[city]"

      assert_select "input#lookups_exchange_lookups_state_id[name=?]", "lookups_exchange[lookups_state_id]"
    end
  end
end
