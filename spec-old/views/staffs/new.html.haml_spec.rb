require 'rails_helper'

RSpec.describe "staffs/new", :type => :view do
  before(:each) do
    assign(:staff, Staff.new(
      :first_name => "MyString",
      :last_name => "MyString",
      :staff_role_id => 1,
      :note => "MyText",
      :disabled => false
    ))
  end

  it "renders new staff form" do
    render

    assert_select "form[action=?][method=?]", staffs_path, "post" do

      assert_select "input#staff_first_name[name=?]", "staff[first_name]"

      assert_select "input#staff_last_name[name=?]", "staff[last_name]"

      assert_select "input#staff_staff_role_id[name=?]", "staff[staff_role_id]"

      assert_select "textarea#staff_note[name=?]", "staff[note]"

      assert_select "input#staff_disabled[name=?]", "staff[disabled]"
    end
  end
end
