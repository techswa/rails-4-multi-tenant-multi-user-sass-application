require 'rails_helper'

RSpec.describe "staffs/index", :type => :view do
  before(:each) do
    assign(:staffs, [
      Staff.create!(
        :first_name => "First Name",
        :last_name => "Last Name",
        :staff_role_id => 1,
        :note => "MyText",
        :disabled => false
      ),
      Staff.create!(
        :first_name => "First Name",
        :last_name => "Last Name",
        :staff_role_id => 1,
        :note => "MyText",
        :disabled => false
      )
    ])
  end

  it "renders a list of staffs" do
    render
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
