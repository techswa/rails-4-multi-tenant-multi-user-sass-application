require 'rails_helper'

RSpec.describe "statistics/visits/show", :type => :view do
  before(:each) do
    @statistics_visit = assign(:statistics_visit, Statistics::Visit.create!(
      :user_id => "",
      :sponsors_sponsor_id => "",
      :marketing_campaign_id => "",
      :session_id => "MyText",
      :statistics_device_id => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
  end
end
