require 'rails_helper'

RSpec.describe "statistics/visits/new", :type => :view do
  before(:each) do
    assign(:statistics_visit, Statistics::Visit.new(
      :user_id => "",
      :sponsors_sponsor_id => "",
      :marketing_campaign_id => "",
      :session_id => "MyText",
      :statistics_device_id => ""
    ))
  end

  it "renders new statistics_visit form" do
    render

    assert_select "form[action=?][method=?]", statistics_visits_path, "post" do

      assert_select "input#statistics_visit_user_id[name=?]", "statistics_visit[user_id]"

      assert_select "input#statistics_visit_sponsors_sponsor_id[name=?]", "statistics_visit[sponsors_sponsor_id]"

      assert_select "input#statistics_visit_marketing_campaign_id[name=?]", "statistics_visit[marketing_campaign_id]"

      assert_select "textarea#statistics_visit_session_id[name=?]", "statistics_visit[session_id]"

      assert_select "input#statistics_visit_statistics_device_id[name=?]", "statistics_visit[statistics_device_id]"
    end
  end
end
