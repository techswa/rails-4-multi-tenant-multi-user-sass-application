require 'rails_helper'

RSpec.describe "statistics/visits/index", :type => :view do
  before(:each) do
    assign(:statistics_visits, [
      Statistics::Visit.create!(
        :user_id => "",
        :sponsors_sponsor_id => "",
        :marketing_campaign_id => "",
        :session_id => "MyText",
        :statistics_device_id => ""
      ),
      Statistics::Visit.create!(
        :user_id => "",
        :sponsors_sponsor_id => "",
        :marketing_campaign_id => "",
        :session_id => "MyText",
        :statistics_device_id => ""
      )
    ])
  end

  it "renders a list of statistics/visits" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
