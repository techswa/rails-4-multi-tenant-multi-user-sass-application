require 'rails_helper'

RSpec.describe "statistics/devices/index", :type => :view do
  before(:each) do
    assign(:statistics_devices, [
      Statistics::Device.create!(
        :note => "MyText"
      ),
      Statistics::Device.create!(
        :note => "MyText"
      )
    ])
  end

  it "renders a list of statistics/devices" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
