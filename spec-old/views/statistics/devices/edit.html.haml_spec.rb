require 'rails_helper'

RSpec.describe "statistics/devices/edit", :type => :view do
  before(:each) do
    @statistics_device = assign(:statistics_device, Statistics::Device.create!(
      :note => "MyText"
    ))
  end

  it "renders the edit statistics_device form" do
    render

    assert_select "form[action=?][method=?]", statistics_device_path(@statistics_device), "post" do

      assert_select "textarea#statistics_device_note[name=?]", "statistics_device[note]"
    end
  end
end
