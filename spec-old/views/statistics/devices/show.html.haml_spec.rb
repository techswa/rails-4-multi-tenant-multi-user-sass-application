require 'rails_helper'

RSpec.describe "statistics/devices/show", :type => :view do
  before(:each) do
    @statistics_device = assign(:statistics_device, Statistics::Device.create!(
      :note => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
  end
end
