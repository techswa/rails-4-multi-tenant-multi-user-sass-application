require 'rails_helper'

RSpec.describe "statistics/devices/new", :type => :view do
  before(:each) do
    assign(:statistics_device, Statistics::Device.new(
      :note => "MyText"
    ))
  end

  it "renders new statistics_device form" do
    render

    assert_select "form[action=?][method=?]", statistics_devices_path, "post" do

      assert_select "textarea#statistics_device_note[name=?]", "statistics_device[note]"
    end
  end
end
