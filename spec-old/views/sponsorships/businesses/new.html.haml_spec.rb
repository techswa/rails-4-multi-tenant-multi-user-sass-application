require 'rails_helper'

RSpec.describe "sponsorships/businesses/new", :type => :view do
  before(:each) do
    assign(:sponsorships_business, Sponsorships::Business.new(
      :sponsors_sponsor_id => "",
      :sponsors_sponsored_id => "",
      :marketing_campaign_id => ""
    ))
  end

  it "renders new sponsorships_business form" do
    render

    assert_select "form[action=?][method=?]", sponsorships_businesses_path, "post" do

      assert_select "input#sponsorships_business_sponsors_sponsor_id[name=?]", "sponsorships_business[sponsors_sponsor_id]"

      assert_select "input#sponsorships_business_sponsors_sponsored_id[name=?]", "sponsorships_business[sponsors_sponsored_id]"

      assert_select "input#sponsorships_business_marketing_campaign_id[name=?]", "sponsorships_business[marketing_campaign_id]"
    end
  end
end
