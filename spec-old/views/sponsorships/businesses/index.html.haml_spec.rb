require 'rails_helper'

RSpec.describe "sponsorships/businesses/index", :type => :view do
  before(:each) do
    assign(:sponsorships_businesses, [
      Sponsorships::Business.create!(
        :sponsors_sponsor_id => "",
        :sponsors_sponsored_id => "",
        :marketing_campaign_id => ""
      ),
      Sponsorships::Business.create!(
        :sponsors_sponsor_id => "",
        :sponsors_sponsored_id => "",
        :marketing_campaign_id => ""
      )
    ])
  end

  it "renders a list of sponsorships/businesses" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
