require 'rails_helper'

RSpec.describe "sponsorships/businesses/edit", :type => :view do
  before(:each) do
    @sponsorships_business = assign(:sponsorships_business, Sponsorships::Business.create!(
      :sponsors_sponsor_id => "",
      :sponsors_sponsored_id => "",
      :marketing_campaign_id => ""
    ))
  end

  it "renders the edit sponsorships_business form" do
    render

    assert_select "form[action=?][method=?]", sponsorships_business_path(@sponsorships_business), "post" do

      assert_select "input#sponsorships_business_sponsors_sponsor_id[name=?]", "sponsorships_business[sponsors_sponsor_id]"

      assert_select "input#sponsorships_business_sponsors_sponsored_id[name=?]", "sponsorships_business[sponsors_sponsored_id]"

      assert_select "input#sponsorships_business_marketing_campaign_id[name=?]", "sponsorships_business[marketing_campaign_id]"
    end
  end
end
