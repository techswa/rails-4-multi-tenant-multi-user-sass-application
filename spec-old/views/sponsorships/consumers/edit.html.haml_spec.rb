require 'rails_helper'

RSpec.describe "sponsorships/consumers/edit", :type => :view do
  before(:each) do
    @sponsorships_consumer = assign(:sponsorships_consumer, Sponsorships::Consumer.create!(
      :sponsors_sponsor_id => "",
      :user_id => "",
      :marketing_campaign_id => ""
    ))
  end

  it "renders the edit sponsorships_consumer form" do
    render

    assert_select "form[action=?][method=?]", sponsorships_consumer_path(@sponsorships_consumer), "post" do

      assert_select "input#sponsorships_consumer_sponsors_sponsor_id[name=?]", "sponsorships_consumer[sponsors_sponsor_id]"

      assert_select "input#sponsorships_consumer_user_id[name=?]", "sponsorships_consumer[user_id]"

      assert_select "input#sponsorships_consumer_marketing_campaign_id[name=?]", "sponsorships_consumer[marketing_campaign_id]"
    end
  end
end
