require 'rails_helper'

RSpec.describe "sponsorships/consumers/index", :type => :view do
  before(:each) do
    assign(:sponsorships_consumers, [
      Sponsorships::Consumer.create!(
        :sponsors_sponsor_id => "",
        :user_id => "",
        :marketing_campaign_id => ""
      ),
      Sponsorships::Consumer.create!(
        :sponsors_sponsor_id => "",
        :user_id => "",
        :marketing_campaign_id => ""
      )
    ])
  end

  it "renders a list of sponsorships/consumers" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
