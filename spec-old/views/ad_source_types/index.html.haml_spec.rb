require 'rails_helper'

RSpec.describe "ad_source_types/index", :type => :view do
  before(:each) do
    assign(:source_types, [
      SourceType.create!(
        :title => "Title",
        :description => "MyText",
        :archived => false
      ),
      SourceType.create!(
        :title => "Title",
        :description => "MyText",
        :archived => false
      )
    ])
  end

  it "renders a list of ad_source_types" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
