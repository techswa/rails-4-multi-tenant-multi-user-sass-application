require 'rails_helper'

RSpec.describe "ad_source_types/edit", :type => :view do
  before(:each) do
    @ad_source_type = assign(:ad_source_type, SourceType.create!(
      :title => "MyString",
      :description => "MyText",
      :archived => false
    ))
  end

  it "renders the edit ad_source_type form" do
    render

    assert_select "form[action=?][method=?]", ad_source_type_path(@ad_source_type), "post" do

      assert_select "input#ad_source_type_title[name=?]", "ad_source_type[title]"

      assert_select "textarea#ad_source_type_description[name=?]", "ad_source_type[description]"

      assert_select "input#ad_source_type_archived[name=?]", "ad_source_type[archived]"
    end
  end
end
