require 'rails_helper'

RSpec.describe "ad_source_types/new", :type => :view do
  before(:each) do
    assign(:ad_source_type, SourceType.new(
      :title => "MyString",
      :description => "MyText",
      :archived => false
    ))
  end

  it "renders new ad_source_type form" do
    render

    assert_select "form[action=?][method=?]", ad_source_types_path, "post" do

      assert_select "input#ad_source_type_title[name=?]", "ad_source_type[title]"

      assert_select "textarea#ad_source_type_description[name=?]", "ad_source_type[description]"

      assert_select "input#ad_source_type_archived[name=?]", "ad_source_type[archived]"
    end
  end
end
