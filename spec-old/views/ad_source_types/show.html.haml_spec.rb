require 'rails_helper'

RSpec.describe "ad_source_types/show", :type => :view do
  before(:each) do
    @ad_source_type = assign(:ad_source_type, SourceType.create!(
      :title => "Title",
      :description => "MyText",
      :archived => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
  end
end
