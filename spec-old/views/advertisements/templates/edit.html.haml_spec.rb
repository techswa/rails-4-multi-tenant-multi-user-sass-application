require 'rails_helper'

RSpec.describe "advertisements/templates/edit", :type => :view do
  before(:each) do
    @advertisements_template = assign(:advertisements_template, Advertisements::Template.create!(
      :title => "MyString",
      :description => "MyText",
      :sponsor_id => "",
      :code => "MyText",
      :archived => false
    ))
  end

  it "renders the edit advertisements_template form" do
    render

    assert_select "form[action=?][method=?]", advertisements_template_path(@advertisements_template), "post" do

      assert_select "input#advertisements_template_title[name=?]", "advertisements_template[title]"

      assert_select "textarea#advertisements_template_description[name=?]", "advertisements_template[description]"

      assert_select "input#advertisements_template_sponsor_id[name=?]", "advertisements_template[sponsor_id]"

      assert_select "textarea#advertisements_template_code[name=?]", "advertisements_template[code]"

      assert_select "input#advertisements_template_archived[name=?]", "advertisements_template[archived]"
    end
  end
end
