require 'rails_helper'

RSpec.describe "advertisements/templates/show", :type => :view do
  before(:each) do
    @advertisements_template = assign(:advertisements_template, Advertisements::Template.create!(
      :title => "Title",
      :description => "MyText",
      :sponsor_id => "",
      :code => "MyText",
      :archived => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
  end
end
