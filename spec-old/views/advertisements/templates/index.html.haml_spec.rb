require 'rails_helper'

RSpec.describe "advertisements/templates/index", :type => :view do
  before(:each) do
    assign(:advertisements_templates, [
      Advertisements::Template.create!(
        :title => "Title",
        :description => "MyText",
        :sponsor_id => "",
        :code => "MyText",
        :archived => false
      ),
      Advertisements::Template.create!(
        :title => "Title",
        :description => "MyText",
        :sponsor_id => "",
        :code => "MyText",
        :archived => false
      )
    ])
  end

  it "renders a list of advertisements/templates" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
