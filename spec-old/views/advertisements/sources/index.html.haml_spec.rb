require 'rails_helper'

RSpec.describe "advertisements/sources/index", :type => :view do
  before(:each) do
    assign(:advertisements_sources, [
      Advertisements::Source.create!(
        :title => "Title",
        :description => "MyText",
        :symbol_name => "Symbol Name",
        :archived => false
      ),
      Advertisements::Source.create!(
        :title => "Title",
        :description => "MyText",
        :symbol_name => "Symbol Name",
        :archived => false
      )
    ])
  end

  it "renders a list of advertisements/sources" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Symbol Name".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
