require 'rails_helper'

RSpec.describe "advertisements/sources/show", :type => :view do
  before(:each) do
    @advertisements_source = assign(:advertisements_source, Advertisements::Source.create!(
      :title => "Title",
      :description => "MyText",
      :symbol_name => "Symbol Name",
      :archived => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Symbol Name/)
    expect(rendered).to match(/false/)
  end
end
