require 'rails_helper'

RSpec.describe "advertisements/sources/new", :type => :view do
  before(:each) do
    assign(:advertisements_source, Advertisements::Source.new(
      :title => "MyString",
      :description => "MyText",
      :symbol_name => "MyString",
      :archived => false
    ))
  end

  it "renders new advertisements_source form" do
    render

    assert_select "form[action=?][method=?]", advertisements_sources_path, "post" do

      assert_select "input#advertisements_source_title[name=?]", "advertisements_source[title]"

      assert_select "textarea#advertisements_source_description[name=?]", "advertisements_source[description]"

      assert_select "input#advertisements_source_symbol_name[name=?]", "advertisements_source[symbol_name]"

      assert_select "input#advertisements_source_archived[name=?]", "advertisements_source[archived]"
    end
  end
end
