require 'rails_helper'

RSpec.describe "advertisements/positions/show", :type => :view do
  before(:each) do
    @advertisements_position = assign(:advertisements_position, Advertisements::Position.create!(
      :title => "Title",
      :description => "MyText",
      :sponsors_type_id => "",
      :display_width => 1,
      :display_height => 2,
      :symbol_name => "Symbol Name",
      :archived => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Symbol Name/)
    expect(rendered).to match(/false/)
  end
end
