require 'rails_helper'

RSpec.describe "advertisements/positions/new", :type => :view do
  before(:each) do
    assign(:advertisements_position, Advertisements::Position.new(
      :title => "MyString",
      :description => "MyText",
      :sponsors_type_id => "",
      :display_width => 1,
      :display_height => 1,
      :symbol_name => "MyString",
      :archived => false
    ))
  end

  it "renders new advertisements_position form" do
    render

    assert_select "form[action=?][method=?]", advertisements_positions_path, "post" do

      assert_select "input#advertisements_position_title[name=?]", "advertisements_position[title]"

      assert_select "textarea#advertisements_position_description[name=?]", "advertisements_position[description]"

      assert_select "input#advertisements_position_sponsors_type_id[name=?]", "advertisements_position[sponsors_type_id]"

      assert_select "input#advertisements_position_display_width[name=?]", "advertisements_position[display_width]"

      assert_select "input#advertisements_position_display_height[name=?]", "advertisements_position[display_height]"

      assert_select "input#advertisements_position_symbol_name[name=?]", "advertisements_position[symbol_name]"

      assert_select "input#advertisements_position_archived[name=?]", "advertisements_position[archived]"
    end
  end
end
