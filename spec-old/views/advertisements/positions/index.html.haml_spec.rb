require 'rails_helper'

RSpec.describe "advertisements/positions/index", :type => :view do
  before(:each) do
    assign(:advertisements_positions, [
      Advertisements::Position.create!(
        :title => "Title",
        :description => "MyText",
        :sponsors_type_id => "",
        :display_width => 1,
        :display_height => 2,
        :symbol_name => "Symbol Name",
        :archived => false
      ),
      Advertisements::Position.create!(
        :title => "Title",
        :description => "MyText",
        :sponsors_type_id => "",
        :display_width => 1,
        :display_height => 2,
        :symbol_name => "Symbol Name",
        :archived => false
      )
    ])
  end

  it "renders a list of advertisements/positions" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Symbol Name".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
