require 'rails_helper'

RSpec.describe "advertisements/rotations/show", :type => :view do
  before(:each) do
    @advertisements_rotation = assign(:advertisements_rotation, Advertisements::Rotation.create!(
      :sponsors_sponsor_id => "",
      :advertisements_advertisement_id => "",
      :running => false,
      :notes => "MyText",
      :archived => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
  end
end
