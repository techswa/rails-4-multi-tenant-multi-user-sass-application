require 'rails_helper'

RSpec.describe "advertisements/rotations/new", :type => :view do
  before(:each) do
    assign(:advertisements_rotation, Advertisements::Rotation.new(
      :sponsors_sponsor_id => "",
      :advertisements_advertisement_id => "",
      :running => false,
      :notes => "MyText",
      :archived => false
    ))
  end

  it "renders new advertisements_rotation form" do
    render

    assert_select "form[action=?][method=?]", advertisements_rotations_path, "post" do

      assert_select "input#advertisements_rotation_sponsors_sponsor_id[name=?]", "advertisements_rotation[sponsors_sponsor_id]"

      assert_select "input#advertisements_rotation_advertisements_advertisement_id[name=?]", "advertisements_rotation[advertisements_advertisement_id]"

      assert_select "input#advertisements_rotation_running[name=?]", "advertisements_rotation[running]"

      assert_select "textarea#advertisements_rotation_notes[name=?]", "advertisements_rotation[notes]"

      assert_select "input#advertisements_rotation_archived[name=?]", "advertisements_rotation[archived]"
    end
  end
end
