require 'rails_helper'

RSpec.describe "advertisements/rotations/index", :type => :view do
  before(:each) do
    assign(:advertisements_rotations, [
      Advertisements::Rotation.create!(
        :sponsors_sponsor_id => "",
        :advertisements_advertisement_id => "",
        :running => false,
        :notes => "MyText",
        :archived => false
      ),
      Advertisements::Rotation.create!(
        :sponsors_sponsor_id => "",
        :advertisements_advertisement_id => "",
        :running => false,
        :notes => "MyText",
        :archived => false
      )
    ])
  end

  it "renders a list of advertisements/rotations" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
