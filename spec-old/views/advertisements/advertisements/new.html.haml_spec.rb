require 'rails_helper'

RSpec.describe "advertisements/advertisements/new", :type => :view do
  before(:each) do
    assign(:advertisements_advertisement, Advertisements::Advertisement.new(
      :sponsors_sponsor_id => "",
      :title => "MyString",
      :description => "MyText",
      :revision => 1.5,
      :symbol_name => "MyString",
      :advertisements_source_id => "",
      :advertisements_position_id => "",
      :advertisements_template_id => "",
      :images_imgfile_id => "",
      :archived => false
    ))
  end

  it "renders new advertisements_advertisement form" do
    render

    assert_select "form[action=?][method=?]", advertisements_advertisements_path, "post" do

      assert_select "input#advertisements_advertisement_sponsors_sponsor_id[name=?]", "advertisements_advertisement[sponsors_sponsor_id]"

      assert_select "input#advertisements_advertisement_title[name=?]", "advertisements_advertisement[title]"

      assert_select "textarea#advertisements_advertisement_description[name=?]", "advertisements_advertisement[description]"

      assert_select "input#advertisements_advertisement_revision[name=?]", "advertisements_advertisement[revision]"

      assert_select "input#advertisements_advertisement_symbol_name[name=?]", "advertisements_advertisement[symbol_name]"

      assert_select "input#advertisements_advertisement_advertisements_source_id[name=?]", "advertisements_advertisement[advertisements_source_id]"

      assert_select "input#advertisements_advertisement_advertisements_position_id[name=?]", "advertisements_advertisement[advertisements_position_id]"

      assert_select "input#advertisements_advertisement_advertisements_template_id[name=?]", "advertisements_advertisement[advertisements_template_id]"

      assert_select "input#advertisements_advertisement_images_imgfile_id[name=?]", "advertisements_advertisement[images_imgfile_id]"

      assert_select "input#advertisements_advertisement_archived[name=?]", "advertisements_advertisement[archived]"
    end
  end
end
