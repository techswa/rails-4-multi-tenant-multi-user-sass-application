require 'rails_helper'

RSpec.describe "advertisements/advertisements/show", :type => :view do
  before(:each) do
    @advertisements_advertisement = assign(:advertisements_advertisement, Advertisements::Advertisement.create!(
      :sponsors_sponsor_id => "",
      :title => "Title",
      :description => "MyText",
      :revision => 1.5,
      :symbol_name => "Symbol Name",
      :advertisements_source_id => "",
      :advertisements_position_id => "",
      :advertisements_template_id => "",
      :images_imgfile_id => "",
      :archived => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/1.5/)
    expect(rendered).to match(/Symbol Name/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/false/)
  end
end
