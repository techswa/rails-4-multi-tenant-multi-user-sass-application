require 'rails_helper'

RSpec.describe "advertisements/advertisements/index", :type => :view do
  before(:each) do
    assign(:advertisements_advertisements, [
      Advertisements::Advertisement.create!(
        :sponsors_sponsor_id => "",
        :title => "Title",
        :description => "MyText",
        :revision => 1.5,
        :symbol_name => "Symbol Name",
        :advertisements_source_id => "",
        :advertisements_position_id => "",
        :advertisements_template_id => "",
        :images_imgfile_id => "",
        :archived => false
      ),
      Advertisements::Advertisement.create!(
        :sponsors_sponsor_id => "",
        :title => "Title",
        :description => "MyText",
        :revision => 1.5,
        :symbol_name => "Symbol Name",
        :advertisements_source_id => "",
        :advertisements_position_id => "",
        :advertisements_template_id => "",
        :images_imgfile_id => "",
        :archived => false
      )
    ])
  end

  it "renders a list of advertisements/advertisements" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Symbol Name".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
