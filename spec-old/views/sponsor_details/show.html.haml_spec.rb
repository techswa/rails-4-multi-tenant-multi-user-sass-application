require 'rails_helper'

RSpec.describe "sponsor_details/show", :type => :view do
  before(:each) do
    @sponsor_detail = assign(:sponsor_detail, SponsorDetail.create!(
      :sponsor_id => "",
      :company_name => "Company Name",
      :contact_last_name => "Contact Last Name",
      :contact_first_name => "Contact First Name",
      :phone_1 => "Phone 1",
      :phone_2 => "Phone 2",
      :website => "Website",
      :email => "Email",
      :tag_line_1 => "Tag Line 1",
      :tag_line_2 => "Tag Line 2",
      :welcome_greeting => "MyText",
      :return_greeting => "MyText",
      :logo_file => "Logo File",
      :headshot_file => "Headshot File"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Company Name/)
    expect(rendered).to match(/Contact Last Name/)
    expect(rendered).to match(/Contact First Name/)
    expect(rendered).to match(/Phone 1/)
    expect(rendered).to match(/Phone 2/)
    expect(rendered).to match(/Website/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Tag Line 1/)
    expect(rendered).to match(/Tag Line 2/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Logo File/)
    expect(rendered).to match(/Headshot File/)
  end
end
