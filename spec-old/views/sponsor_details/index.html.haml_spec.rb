require 'rails_helper'

RSpec.describe "sponsor_details/index", :type => :view do
  before(:each) do
    assign(:sponsor_details, [
      SponsorDetail.create!(
        :sponsor_id => "",
        :company_name => "Company Name",
        :contact_last_name => "Contact Last Name",
        :contact_first_name => "Contact First Name",
        :phone_1 => "Phone 1",
        :phone_2 => "Phone 2",
        :website => "Website",
        :email => "Email",
        :tag_line_1 => "Tag Line 1",
        :tag_line_2 => "Tag Line 2",
        :welcome_greeting => "MyText",
        :return_greeting => "MyText",
        :logo_file => "Logo File",
        :headshot_file => "Headshot File"
      ),
      SponsorDetail.create!(
        :sponsor_id => "",
        :company_name => "Company Name",
        :contact_last_name => "Contact Last Name",
        :contact_first_name => "Contact First Name",
        :phone_1 => "Phone 1",
        :phone_2 => "Phone 2",
        :website => "Website",
        :email => "Email",
        :tag_line_1 => "Tag Line 1",
        :tag_line_2 => "Tag Line 2",
        :welcome_greeting => "MyText",
        :return_greeting => "MyText",
        :logo_file => "Logo File",
        :headshot_file => "Headshot File"
      )
    ])
  end

  it "renders a list of sponsor_details" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Company Name".to_s, :count => 2
    assert_select "tr>td", :text => "Contact Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "Contact First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Phone 1".to_s, :count => 2
    assert_select "tr>td", :text => "Phone 2".to_s, :count => 2
    assert_select "tr>td", :text => "Website".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Tag Line 1".to_s, :count => 2
    assert_select "tr>td", :text => "Tag Line 2".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Logo File".to_s, :count => 2
    assert_select "tr>td", :text => "Headshot File".to_s, :count => 2
  end
end
