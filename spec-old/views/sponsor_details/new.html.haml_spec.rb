require 'rails_helper'

RSpec.describe "sponsor_details/new", :type => :view do
  before(:each) do
    assign(:sponsor_detail, SponsorDetail.new(
      :sponsor_id => "",
      :company_name => "MyString",
      :contact_last_name => "MyString",
      :contact_first_name => "MyString",
      :phone_1 => "MyString",
      :phone_2 => "MyString",
      :website => "MyString",
      :email => "MyString",
      :tag_line_1 => "MyString",
      :tag_line_2 => "MyString",
      :welcome_greeting => "MyText",
      :return_greeting => "MyText",
      :logo_file => "MyString",
      :headshot_file => "MyString"
    ))
  end

  it "renders new sponsor_detail form" do
    render

    assert_select "form[action=?][method=?]", sponsor_details_path, "post" do

      assert_select "input#sponsor_detail_sponsor_id[name=?]", "sponsor_detail[sponsor_id]"

      assert_select "input#sponsor_detail_company_name[name=?]", "sponsor_detail[company_name]"

      assert_select "input#sponsor_detail_contact_last_name[name=?]", "sponsor_detail[contact_last_name]"

      assert_select "input#sponsor_detail_contact_first_name[name=?]", "sponsor_detail[contact_first_name]"

      assert_select "input#sponsor_detail_phone_1[name=?]", "sponsor_detail[phone_1]"

      assert_select "input#sponsor_detail_phone_2[name=?]", "sponsor_detail[phone_2]"

      assert_select "input#sponsor_detail_website[name=?]", "sponsor_detail[website]"

      assert_select "input#sponsor_detail_email[name=?]", "sponsor_detail[email]"

      assert_select "input#sponsor_detail_tag_line_1[name=?]", "sponsor_detail[tag_line_1]"

      assert_select "input#sponsor_detail_tag_line_2[name=?]", "sponsor_detail[tag_line_2]"

      assert_select "textarea#sponsor_detail_welcome_greeting[name=?]", "sponsor_detail[welcome_greeting]"

      assert_select "textarea#sponsor_detail_return_greeting[name=?]", "sponsor_detail[return_greeting]"

      assert_select "input#sponsor_detail_logo_file[name=?]", "sponsor_detail[logo_file]"

      assert_select "input#sponsor_detail_headshot_file[name=?]", "sponsor_detail[headshot_file]"
    end
  end
end
