require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

RSpec.describe SponsorDetailsController, :type => :controller do

  # This should return the minimal set of attributes required to create a valid
  # SponsorDetail. As you add validations to SponsorDetail, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    skip("Add a hash of attributes valid for your model")
  }

  let(:invalid_attributes) {
    skip("Add a hash of attributes invalid for your model")
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # SponsorDetailsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET index" do
    it "assigns all sponsor_details as @sponsor_details" do
      sponsor_detail = SponsorDetail.create! valid_attributes
      get :index, {}, valid_session
      expect(assigns(:sponsor_details)).to eq([sponsor_detail])
    end
  end

  describe "GET show" do
    it "assigns the requested sponsor_detail as @sponsor_detail" do
      sponsor_detail = SponsorDetail.create! valid_attributes
      get :show, {:id => sponsor_detail.to_param}, valid_session
      expect(assigns(:sponsor_detail)).to eq(sponsor_detail)
    end
  end

  describe "GET new" do
    it "assigns a new sponsor_detail as @sponsor_detail" do
      get :new, {}, valid_session
      expect(assigns(:sponsor_detail)).to be_a_new(SponsorDetail)
    end
  end

  describe "GET edit" do
    it "assigns the requested sponsor_detail as @sponsor_detail" do
      sponsor_detail = SponsorDetail.create! valid_attributes
      get :edit, {:id => sponsor_detail.to_param}, valid_session
      expect(assigns(:sponsor_detail)).to eq(sponsor_detail)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new SponsorDetail" do
        expect {
          post :create, {:sponsor_detail => valid_attributes}, valid_session
        }.to change(SponsorDetail, :count).by(1)
      end

      it "assigns a newly created sponsor_detail as @sponsor_detail" do
        post :create, {:sponsor_detail => valid_attributes}, valid_session
        expect(assigns(:sponsor_detail)).to be_a(SponsorDetail)
        expect(assigns(:sponsor_detail)).to be_persisted
      end

      it "redirects to the created sponsor_detail" do
        post :create, {:sponsor_detail => valid_attributes}, valid_session
        expect(response).to redirect_to(SponsorDetail.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved sponsor_detail as @sponsor_detail" do
        post :create, {:sponsor_detail => invalid_attributes}, valid_session
        expect(assigns(:sponsor_detail)).to be_a_new(SponsorDetail)
      end

      it "re-renders the 'new' template" do
        post :create, {:sponsor_detail => invalid_attributes}, valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested sponsor_detail" do
        sponsor_detail = SponsorDetail.create! valid_attributes
        put :update, {:id => sponsor_detail.to_param, :sponsor_detail => new_attributes}, valid_session
        sponsor_detail.reload
        skip("Add assertions for updated state")
      end

      it "assigns the requested sponsor_detail as @sponsor_detail" do
        sponsor_detail = SponsorDetail.create! valid_attributes
        put :update, {:id => sponsor_detail.to_param, :sponsor_detail => valid_attributes}, valid_session
        expect(assigns(:sponsor_detail)).to eq(sponsor_detail)
      end

      it "redirects to the sponsor_detail" do
        sponsor_detail = SponsorDetail.create! valid_attributes
        put :update, {:id => sponsor_detail.to_param, :sponsor_detail => valid_attributes}, valid_session
        expect(response).to redirect_to(sponsor_detail)
      end
    end

    describe "with invalid params" do
      it "assigns the sponsor_detail as @sponsor_detail" do
        sponsor_detail = SponsorDetail.create! valid_attributes
        put :update, {:id => sponsor_detail.to_param, :sponsor_detail => invalid_attributes}, valid_session
        expect(assigns(:sponsor_detail)).to eq(sponsor_detail)
      end

      it "re-renders the 'edit' template" do
        sponsor_detail = SponsorDetail.create! valid_attributes
        put :update, {:id => sponsor_detail.to_param, :sponsor_detail => invalid_attributes}, valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested sponsor_detail" do
      sponsor_detail = SponsorDetail.create! valid_attributes
      expect {
        delete :destroy, {:id => sponsor_detail.to_param}, valid_session
      }.to change(SponsorDetail, :count).by(-1)
    end

    it "redirects to the sponsor_details list" do
      sponsor_detail = SponsorDetail.create! valid_attributes
      delete :destroy, {:id => sponsor_detail.to_param}, valid_session
      expect(response).to redirect_to(sponsor_details_url)
    end
  end

end
