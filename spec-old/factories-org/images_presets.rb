FactoryGirl.define do
  factory :images_preset, :class => 'Images::Preset' do
    title "MyString"
description "MyText"
symbol_name "MyString"
preview_width 1
preview_height 1
display_width 1
display_height 1
archived false
  end

end
