FactoryGirl.define do
  factory :sponsor_detail do
    sponsor_id ""
company_name "MyString"
contact_last_name "MyString"
contact_first_name "MyString"
phone_1 "MyString"
phone_2 "MyString"
website "MyString"
email "MyString"
tag_line_1 "MyString"
tag_line_2 "MyString"
welcome_greeting "MyText"
return_greeting "MyText"
logo_file "MyString"
headshot_file "MyString"
  end

end
