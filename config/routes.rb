Rails.application.routes.draw do



  namespace :sponsors do
    resources :franchises
  end

  # get 'myportal/index'
  match 'myportal' => 'myportal#index', :via => :get, as: :myportal
  match 'myportal/details' => 'myportal/details#show', :via => :get, as: :myportal_details_show
  match 'myportal/advertisements' => 'myportal/advertisements#index', :via => :get, as: :myportal_advertisements
  match 'myportal/visitors' => 'myportal/visitors#index', :via => :get, as: :myportal_visitors
  # namespace :myportal do
  #   resources :advertisements
  #   # resources :details
  # end

  # get 'myportal/visitors'
  #
  # match 'myportal/advertisements' => 'myportal/advertisements #index', :via => :get, as: :myportal
  # # get 'myportal/advertisements'
  #
  # get 'myportal/details'

 #  namespace :api do
 #    namespace :v1 do
 #      resources :sponsors
 #
 #      namespace :advertisements do
 #        resources :templates
 #      end
 #
 # match 'invitations/:invitation_id' => 'invitations#invitation', :via => :get, :as => :invitation
 #
 #
 #
 #      namespace :lookups do
 #        resources :states
 #      end
 #
 #    end
 #  end
# ---------------------
# end of api
# ---------------------

  namespace :sponsorships do
    resources :vendors
  end

  namespace :advertisements do
    resources :groups
    resources :templates
  end

  devise_for :users, :controllers => { :registrations_users => 'registrations_users'}

  devise_for :sponsors, controllers: {registrations: 'sponsors/registrations', sessions: 'sponsors/sessions'}

  # resources :sponsors

  # devise_for :staffs
  # resources :staffs

  # namespace :sponsor do
  #   get 'portal/index'
  # end
  #
  namespace :sponsors do
    resources :levels
    resources :portals
    resources :details
    resources :types
    resources :styles
  #   get 'invitations/unfortunately'
  #   get 'invitations/show'
  #   get 'invitations/index'
  end

  # namespace :admin do
    # resources :sponsors_types
    # resources :sponsors_levels
    # resources :sponsors_styles
  # end

  # namespace :accounting do
  #   resources :payments
  #   resources :payment_types
  # end
  #
  # namespace :statistics do
  #   resources :visits
  #   resources :devices
  # end
  #
  # namespace :marketing do
  #   resources :channels
  #   resources :campaigns
  # end
  #
  # match 'welcome/new/:campaign_id' => 'welcome#new', :via => :get, as: :new_sponsors_welcome
  #


   namespace :advertisements do
     resources :advertisements

  #   resources :mechanicals
  #   resources :templates
  #   resources :sources
  #   resources :rotations

   end

  #
  # namespace :sponsorships do
  #   resources :consumers
  #   resources :businesses
  # end
  #
  # namespace :images do
  #   resources :imgfiles
  #   resources :types
  #   resources :presets
  # end
  #
  # namespace :lookups do
  #   resources :industries
  #   resources :counties
  #   resources :campaigns
  #   resources :exchanges
  # end
  #
  # # invitation/a2e34f2c-d23f-47d3-9980-fb9b4932d465
  # # Route invitation to the correct sponsor
  # match 'invitation/:campaign_id' => 'sponsors/invitations#invitation', :via => :get, :as => :invitation

  get 'welcome' => 'welcome#index', as: 'welcome'


  # **** SAMPLE ****
  # Added for processing the sponsor sub domain  see RailsCast #221
  # constraints subdomain: 'sponsor' do
  #   # get "/" => redirect { |params| "http://www.externalurl.com" }
  #   puts 'Entered on subdomain'
  #   # get 'welcome' => 'welcome#index', as: 'sponsor_welcome'
  # end




  # resources :sponsor_types



  # get 'support/index'
  #
  # get 'tutorials/welcome'
  #
  # get 'tutorials/index'
  #
  # get 'tutorial/welcome'
  #
  # get 'tutorials/new'
  #
  # get 'tutorials/create'
  #
  # get 'tutorials/update'
  #
  # get 'tutorials/show'
  #
  # get 'tutorials/destroy'
  #
  #resources :tutorials


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".


  match 'myportal' => 'sponsors/portals#myportal', :via => :get, as: :sponsors_portals_myportal



  # You can have the root of your site routed with "root"
  root 'welcome#index'

end
