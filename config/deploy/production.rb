# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

role :app, %w{deployer@comparepropertytaxes.com}
role :web, %w{deployer@comparepropertytaxes.com}
role :db,  %w{deployer@comparepropertytaxes.com}

set :stage, :production
set :rails_env, :production

# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

#server 'example.com', user: 'deploy', roles: %w{web app}, my_property: :my_value
server 'comparepropertytaxes.com', user: 'deployer', roles: %w{web app db}, primary: true

# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult[net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start).
#
# Global options
# --------------
# set :ssh_options, {
#    #keys: %w(/home/deployer/.ssh/id_rsa),
#    forward_agent: true,
#    #auth_methods: %w(password)
# }
#

set :ssh_options, {
    config:false,
    user: 'deployer', # overrides user setting above
    password: 'w#fgt9&ecQ',
    keys: %w(/home/deployer/.ssh/id_rsa),
    forward_agent: true,
    auth_methods: %w(password),
    verbose: :debug
}



# And/or per server (overrides global)
# ------------------------------------
# server 'example.com',
#   user: 'user_name',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'user_name', # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: 'please use keys'
#   }



####
# Configure NGINX
####

# Server name for nginx
# No default value
set :nginx_domains, 'comparepropertytaxes.com'

# nginx service script
# Defaults to using the 'service' convinience script.
# You might prefer using the init.d instead depending on sudo privilages.
# default value: "service nginx"
set :nginx_service_path, '/etc/init.d/nginx'

# Roles the deploy nginx site on,
# default value: :web
set :nginx_roles, :web

# Path, where nginx log file will be stored
# default value:  "#{shared_path}/log"
# set :nginx_log_path, "#{shared_path}/log"

# Path where nginx is installed
# default value: "/etc/nginx"
set :nginx_root_path, '/etc/nginx'

# Path where to look for static files
# default value: "public"
set :nginx_static_dir, 'public'

# Path where nginx available site are stored
# default value: "sites-available"
set :nginx_sites_available, 'sites-available'

# Path where nginx available site are stored
# default value: "sites-enabled"
set :nginx_sites_enabled, 'sites-enabled'

# Path to look for custom config template
# `:default` will use the bundled nginx template
# default value: :default
set :nginx_template, "#{stage_config_path}/#{fetch :stage}/nginx.conf.erb"

# Use SSL on port 443 to serve on https. Every request to por 80
# will be rewritten to 443
# default value: false
set :nginx_use_ssl, false

# Name of SSL certificate file
# default value: "#{application}.crt"
# set :nginx_ssl_certificate, 'my-domain.crt'

# SSL certificate file path
# default value: "/etc/ssl/certs"
# set :nginx_ssl_certificate_path, "#{shared_path}/ssl/certs"

# Name of SSL certificate private key
# default value: "#{application}.key"
# set :nginx_ssl_certificate_key, 'my-domain.key'

# SSL certificate private key path
# default value: "/etc/ssl/private"
# set :nginx_ssl_certificate_key_path, "#{shared_path}/ssl/private"

# Whether you want to server an application through a proxy pass
# default value: true
set :app_server, true

# Socket file that nginx will use as upstream to serve the application
# Note: Socket upstream has priority over host:port upstreams
# no default value
set :app_server_socket, "#{shared_path}/sockets/unicorn-#{fetch :application}.sock"

# The host that nginx will use as upstream to server the application
# default value: 127.0.0.1
set :app_server_host, '127.0.0.1'

# The port the application server is running on
# no default value
set :app_server_port, 80

