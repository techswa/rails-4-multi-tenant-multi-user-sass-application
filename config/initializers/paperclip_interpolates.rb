# Custom path elements

# Used for base path
Paperclip.interpolates('sponsoree_id') do |img, style|
  img.instance.sponsor_detail.id.parameterize
end

# Used for sub path
Paperclip.interpolates('image_category_id') do |img, style|
  img.instance.image_category.id.parameterize
end
