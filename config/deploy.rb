# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'compare'

set :scm, :git

set :repo_url, 'ssh://deployer@swapp.webfactional.com/home/swapp/webapps/gitmaster/repos/cpt.git'

# set :branch, 'master'
# set :rails_env, 'production'
#:/home/swapp/webapps/gitmaster/repos/cpt.git

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# For webfaction per - https://community.webfaction.com/questions/15144/capistrano-3-deploys-fatal-error-executing-tmpmyappgit-sshsh
#set :tmp_dir, "/home/swapp/webapps/compare/compare_tmp"

# Directories that wont be over written
#set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default deploy_to directory is /var/www/my_app
 set :deploy_to, '/var/www/cpt'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
 #set :format, :pretty

#set :bundle_cmd, "/home/swapp/webapps/compare/gems/bin/bundle"
#set :bundle_dir, "/home/swapp/webapps/compare/gems/bin"

#set :bundle_flags, '--deployment --quiet'

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
 set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: '$PWD/bin:$PATH', GEM_HOME: '../../gems', RUBYLIB: '../../bin/lib'  }

# Default value for keep_releases is 5
 set :keep_releases, 5

# namespace :deploy do
#
#   desc 'Restart application'
#   task :restart do
#     on roles(:app), in: :sequence, wait: 5 do
#       # Your restart mechanism here, for example:
#       # execute :touch, release_path.join('tmp/restart.txt')
#     end
#   end
#
#   after :publishing, :restart
#
#   after :restart, :clear_cache do
#     on roles(:web), in: :groups, limit: 3, wait: 10 do
#       # Here we can do anything such as:
#       # within release_path do
#       #   execute :rake, 'cache:clear'
#       # end
#     end
#   end

#end
