Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Ember
  # config.ember.variant = :development

  # Do not eager load code on boot.
  config.eager_load = false

  Paperclip.options[:command_path] = '/usr/local/Cellar/imagemagick/6.8.7-0'

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
   config.action_view.raise_on_missing_translations = true

  #Devise requirement
  config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }

  # Paperclip S3 configuration
  # config.paperclip_defaults = {
  #     :storage => :s3,
  #     :s3_credentials => {
  #         :bucket => ENV['S3_BUCKET_NAME'],
  #         :access_key_id => ENV['AWS_ACCESS_KEY_ID'],
  #         :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
  #     }
  # }
  config.paperclip_defaults = {
      :storage => :s3,
      :s3_host_name => 's3.amazonaws.com',
      :s3_credentials => {
          :bucket => 'cpt-test',
          :access_key_id => 'AKIAIRDEK6XFKCMXZUDA',
          :secret_access_key => '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'
      }
  }

  ActionMailer::Base.delivery_method = :smtp
  ActionMailer::Base.smtp_settings = {
      :address => "smtp.webfaction.com",
      :port => 587,
      :domain => "smarterwebapps.com",
      :user_name => "swap_britton",
      :password => "paperless@1347",
      :authentication => "plain",
      :enable_starttls_auto => true
  }

end
